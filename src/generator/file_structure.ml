open Factori_errors
open Factori_config
open Factori_utils
open Ocaml_dune

let ( // ) = Factori_options.concat

(* (\* Given a contract name and an original format, infer where the
 *    compiled contract is *\)
 * let get_contract_path ~contract_name ~format =
 *   let dir = get_dir () in
 *   match format with
 *   | Michelson ->
 *     dir // "src" // "contracts" // "michelson" // (contract_name ^ ".tz")
 *   | MichelsonJson | KT1 ->
 *     dir // "src" // "contracts" // "michelson" // (contract_name ^ ".json")
 *   | OCaml ->
 *     dir // "_build" // "default" // "src" // "contracts" // "ml"
 *     // (contract_name ^ ".json")
 *   | Mligo | Jsligo | Pascaligo ->
 *     dir // "src" // "contracts" // "ligo" // (contract_name ^ ".json")
 *   | Archetype ->
 *     dir // "src" // "contracts" // "archetype" // (contract_name ^ ".json") *)

(* let get_local_config_file_path ~dir = dir// "contracts.json" *)

let create_local_config_file ~verbose ~dir =
  let local_config_file = Factori_config.get_local_config_name ~dir in
  if Sys.file_exists local_config_file then
    output_if_verbose ~verbose "contracts.json already exists\n"
  else (
    output_if_verbose ~verbose
      (Format.sprintf "Writing contracts.json to %s\n%!" local_config_file) ;
    serialize_file ~filename:local_config_file
      ~enc:Factori_config.local_config_enc { contracts = [] }
  )

let create_version_file ~dir =
  let version_file = Factori_config.get_version_file_name ~dir in
  if Sys.file_exists version_file then begin
    let project =
      deserialize_file ~filename:version_file
        ~enc:Factori_config.version_config_enc in
    if Version.version <> project.version then begin
      Format.printf
        "The version of the project [%s] does not match with the version of \
         Factori [%s]@."
        project.version Version.version ;
      exit 1
    end
  end else
    serialize_file ~filename:version_file ~enc:Factori_config.version_config_enc
      { version = Version.version }

let get_typenaming_file ~dir ~contract_name =
  dir // "src" // "contracts" // "type_naming"
  // (get_sanitized contract_name ^ ".json")

let check_typescript_infrastructure_present ~dir () =
  let tsconfig = get_typescript_tsconfig_path ~dir () in
  let functolib = get_typescript_functolib_path ~dir () in
  let package = get_typescript_package_dot_json_path () ~dir in
  Sys.file_exists tsconfig && Sys.file_exists functolib
  && Sys.file_exists package

let check_ocaml_infrastructure_present ~dir () =
  let factori_types = get_ocaml_factori_types_path ~dir in
  let blockchain_ml = get_ocaml_blockchain_path ~dir in
  Sys.file_exists factori_types && Sys.file_exists blockchain_ml

(** Check initialized status of current directory *)
let is_initialized_dir () =
  let dir = get_dir () in
  if not (is_dir dir) then (
    make_dir dir ;
    false
  ) else if
      Sys.file_exists (get_local_config_name ~dir)
      && (if !Factori_options.typescript then
           check_typescript_infrastructure_present ~dir ()
         else
           true)
      &&
      if !Factori_options.ocaml then
        check_ocaml_infrastructure_present ~dir ()
      else
        true
    then
    true
  else if Sys.readdir dir <> [||] && not !Factori_options.overwrite then
    raise (DirectoryNotEmpty dir)
  else
    false

(** Create file structure for the new Factori project *)
let create ?(verbose = 0) ?(library = false) ?(crawlori = false)
    ?(dipdup = false) ~overwrite ~name ~db_name () =
  let dir = get_dir () in
  (* match dir with
   * |  None -> Format.eprintf "Please provide a directory for project as an argument to the init command\n%!"
   * | Some dir -> *)
  make_dir dir ;
  if not (is_dir dir) then
    raise (NotADirectory (dir, "create"))
  else if Sys.readdir dir <> [||] && not overwrite then
    raise (DirectoryNotEmpty dir)
  else
    let _ = create_version_file ~dir in
    let _ = create_local_config_file ~verbose ~dir in
    let name = Option.value ~default:"my_factori_project" name in
    let src_dir = dir // "src" in
    let contracts_dir = src_dir // "contracts" in
    let contract_interfaces_dir = get_ocaml_interface_dir ~dir () in
    let typescript_dir = get_typescript_interface_dir ~dir () in
    let naming_dir = contracts_dir // "type_naming" in
    let ocaml_scenario_dir = get_ocaml_scenarios_dir ~dir () in
    let typescript_src_dir = get_typescript_interface_dir_src ~dir () in
    let typescript_public_dir = get_typescript_public_dir ~dir () in
    make_dir src_dir ;
    make_dir contract_interfaces_dir ;
    make_dir typescript_dir ;
    make_dir naming_dir ;
    make_dir typescript_src_dir ;
    make_dir typescript_public_dir ;

    if !Factori_options.ocaml then begin
      let libraries_dir = get_ocaml_libraries_dir ~dir in
      make_dir libraries_dir ;
      write_file (get_ocaml_factori_types_path ~dir) Ocaml_types.types_ml ;
      write_file
        (get_ocaml_factori_abstract_types_path ~dir)
        Ocaml_abstract_types.abstract_types_ml ;
      write_file (get_ocaml_blockchain_path ~dir) Ocaml_blockchain.blockchain_ml ;
      write_file (libraries_dir // "utils.ml") Ocaml_utils.utils_ml ;
      write_dune_file ~with_readable_backup:true
        ~dune_file:Artifacts.libraries_dunefile ~path:(libraries_dir // "dune")
        ()
    end ;

    (* write_file (ocaml_scenario_dir // "libraries" // "dune") Artifacts.libraries_dunefile; *)
    if !Factori_options.ocaml && not library then begin
      make_dir ocaml_scenario_dir ;
      write_dune_file ~with_readable_backup:true
        ~dune_file:(Artifacts.scenarios_dunefile (get_contracts ~dir))
        ~path:(ocaml_scenario_dir // "dune")
        () ;
      let scenario_path = get_ocaml_scenarios_path ~dir () in
      if not (Sys.file_exists scenario_path) then
        write_file scenario_path Artifacts.scenario_ml
    end ;
    (* write_file (ocaml_scenario_dir // "dune") (Artifacts.scenarios_dunefile []); *)
    if !Factori_options.typescript && not library then begin
      write_file
        (get_typescript_functolib_path ~dir ())
        (Format.asprintf "%a" Functolib_ts.functolib_ts ()) ;
      write_file (get_typescript_tsconfig_path ~dir ()) Artifacts.ts_config ;
      write_file
        (get_typescript_package_dot_json_path ~dir ())
        Artifacts.package_json ;
      if !Factori_options.web_mode then begin
        make_dir (get_vue_components_dir ~dir ()) ;
        write_file
          (get_typescript_webpack_config_js_path ~dir ())
          Artifacts.webpack_config_json ;
        write_file (get_typescript_index_html_path ~dir ()) Artifacts.index_html ;
        (* write_file (get_typescript_index_ts_path ~dir) Artifacts.index_ts; *)
        (* write_file (get_typescrtpt_style_css_path ~dir) Artifacts.style_css; *)
        (*  *write_file (get_typescript_app_vue_path ~dir) Artifacts.app_vue; *)
        write_file
          (get_typescript_vue_shim_ts_path ~dir ())
          Artifacts.vue_shim_ts
      end
    end ;
    if not library then begin
      write_file (dir // "Makefile") (Artifacts.makefile ~db_name ~crawlori) ;
      write_file (dir // "README.md") (Artifacts.readme ~crawlori ~dipdup) ;
      write_file
        (dir
        // Format.sprintf "%s.opam"
             (Option.value ~default:"project" !Factori_options.project_name))
        (Artifacts.factori_project_opam_file ~crawlori) ;
      write_file (dir // "dune-project") "(lang dune 2.9)" ;
      write_file (dir // "Makefile.generated") Artifacts.generated_makefile
    end ;
    write_file (dir // ".ocamlformat") Artifacts.ocamlformat ;
    Format.eprintf "Project '%s' created.\n%!" name ;
    (*let command =
        Format.sprintf
          "tree -P '*.ml|dune|functolib.ts|imported*.ts|*_interface.ts' -I \
           '_build|_opam' --prune %s"
          dir in
      Format.eprintf "%s%!\n" command ;
        let _ = Sys.command command in *)
    ()

let create_global_config_file ~filedir =
  let open Factori_config in
  Format.eprintf "parent %s%!\n" filedir ;
  make_dir filedir ;
  let filename = filedir // "config.json" in
  let config : factori_config = () in
  serialize_file ~filename ~enc:factori_config_enc config

(* let create_ml_contract ~dir ~overwrite ~contract_name =
   let dir = Option.value ~default:current_dir_name dir in
   let src_dir = dir // "src" in
   let contracts_dir = src_dir // "contracts" in
   let ml_dir = contracts_dir // "ml" in
   make_dir ~p:true ml_dir ;
   (* let contract_name = Option.value ~default:"unnamed_contract" contract_name in *)
   let filename = ml_dir // add_suffix contract_name ".ml" in
   if exists filename && not overwrite then
     (* Refuse to overwrite *)
     raise (NotOverwritingFile filename)
   else (
     write_file filename Artifacts.default_ml_contract ;
     let dune_content =
       Artifacts.dune_chunk_new_ml_contract ~name:contract_name in
     add_stanza_to_dune_file ~path:(ml_dir // "dune") ~file_must_exist:false
       Artifacts.dune_env_remove_warnings ;
     (* append_to_file ~check_already_present:true ~filepath:(ml_dir // "dune") ~content:Artifacts.dune_env_remove_warnings (); *)
     add_to_dune_file ~path:(ml_dir // "dune") dune_content ;
     (* append_to_file ~check_already_present:true ~filepath:(ml_dir // "dune") ~content:dune_content (); *)
     Factori_config.add_contract ~original_kt1:None ~dir ~format:OCaml
       ~name:contract_name () ;
     let ocaml_interface_file =
       Factori_config.get_ocaml_interface_filename ~contract_name in
     let ts_interface_file = contract_name ^ "_interface.ts" in
     write_file (get_ocaml_interface_dir ~dir // ocaml_interface_file) "" ;
     write_file
       (Factori_config.get_typescript_interface_dir ~dir // ts_interface_file)
       "" ;
     add_to_dune_file
       ~path:(get_ocaml_interface_dir ~dir // "dune")
       (Artifacts.dune_chunk_new_ocaml_interface ~name:contract_name)
   ) ;
   (* Now we need to enable ocaml-interface-related stuff for this new contract *)
   let ocaml_scenario_dir = get_ocaml_scenarios_dir ~dir in
   add_library_to_stanza_in_dune_file
     ~path:(ocaml_scenario_dir // "dune")
     ~stanza_name:"scenario"
     (Factori_config.get_ocaml_interface_basename ~contract_name) *)
(* write_file (ocaml_scenario_dir // "dune") (Artifacts.scenarios_dunefile contracts) *)
