open Format
open Infer_entrypoints
open Pair_type
open Types
open Factori_utils
open Factori_errors

(* These two functions uniformize the names of pairs of types like:
   type toto = int
   type toto_abstract = int id_or_concrete

   across various functions
*)
let auxiliary_type_name ~type_name = type_name

let abstract_type_name ~type_name = sprintf "abstract_%s" type_name

(* Special str_of_unary for abstract types: list and option have aliases *)
let str_of_unary = function
  | List -> "list'"
  | Set -> "set"
  | Ticket -> "ticket"
  | Option -> "option'"

(* Get a tuple for pattern matching;
 *)
let rec pp_ep ?(prefix = None) ppf ep =
  let rec aux (ppf : formatter) = function
    | Base b ->
      fprintf ppf "%a"
        (pp_base_type ~language:Factori_config.OCaml ~abstract:true ~prefix)
        b
    | Pair al ->
      surround Parens
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf ",")
           (fun ppf x -> aux ppf x))
        ppf al
    | Record (PairP _ as p) ->
      pp_pairedtype_ocaml ~delimiters:Parens ~sep:","
        ~f_leaf:(fun name ppf _ -> fprintf ppf "%s" (show_sanitized_name name))
        ~f_pair:(fun ppf _d -> fprintf ppf "")
        ppf p
    | Unary (List, _ep) -> fprintf ppf "l"
    | Or _ -> fprintf ppf "ovar"
    | Annot (ep, annot) ->
      if is_record_leaf ep then
        fprintf ppf "%s" (show_sanitized_name (bind_sanitize_annot annot))
      else
        fprintf ppf "%a" (pp_ep ~prefix) ep
    | _ -> eprintf "ignoring type %a%!\n" pp_type_ ep in
  let res = aux ppf ep in
  res

(* Write down the name of the type in a concise way *)
let rec output_type_pretty ppf = function
  | Base b ->
    fprintf ppf "%a"
      (pp_base_type ~language:Factori_config.OCaml ~abstract:true ~prefix:None)
      b
  | Annot (ep, _name) -> fprintf ppf "%a" output_type_pretty ep
  | Pair al ->
    let n = List.length al in
    fprintf ppf "(%a) tuple%d"
      (pp_print_list ~pp_sep:(fun ppf _ -> fprintf ppf ",") output_type_pretty)
      al n
  | Record _ as t ->
    fprintf ppf "%s" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "%s" (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn -> fprintf ppf "abstract_%s" (show_sanitized_name (fst tn))
  | Unary (u, a) -> fprintf ppf "%a %s" output_type_pretty a (str_of_unary u)
  | Binary (Lambda, (x, y)) ->
    fprintf ppf "(%a,%a) lambda" output_type_pretty x output_type_pretty y
  | Contract _c -> fprintf ppf "contract" (* pp_micheline_ c *)
  | Binary (b, (k, v)) ->
    fprintf ppf "(%a,%a) %s" output_type_pretty k output_type_pretty v
      (str_of_binary b)

and pp_sumtype_ocaml f path ppf x =
  let rec aux ppf = function
    | true :: l ->
      fprintf ppf "(Mprim {prim = `Left;\nargs = [%a];annots=[]})\n" aux l
    | false :: l ->
      fprintf ppf " (Mprim {prim = `Right;\nargs = [%a];\nannots=[]})\n" aux l
    | [] -> fprintf ppf "%a" f x in
  aux ppf path

and constructor_from_path path =
  String.concat "_"
    (List.map
       (fun b ->
         if b then
           "Left"
         else
           "Right")
       path)

and output_value_of_sumtype ppf t_ =
  let pp_sumtype (type a) f l ppf (x : a) =
    let rec aux ppf = function
      | true :: a -> fprintf ppf "SumOrL(%a)\n" aux a
      | false :: b -> fprintf ppf "SumOrR(%a)\n" aux b
      | [] -> fprintf ppf "%a" f x in
    aux ppf l in

  let l = sumtype_to_list t_ in
  fprintf ppf "(match arg with%a)"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "")
       (fun ppf (t_, path) ->
         match t_ with
         | Annot (u_, name) when is_unit u_ ->
           let f ppf s =
             fprintf ppf
               "LeafOr(Factori_utils.sanitized_of_str \"%s\",VUnit ())" s in
           fprintf ppf "\n| %s -> Abstract_value.VSumtype(%a)"
             (show_sanitized_name (bind_sanitize_capitalize name))
             (pp_sumtype f path)
             (show_sanitized_name (bind_sanitize_capitalize name))
         | Annot (t_, name) ->
           let f ppf t_ =
             fprintf ppf "LeafOr(Factori_utils.sanitized_of_str \"%s\",%a x)"
               (show_sanitized_name (bind_sanitize_capitalize name))
               output_value_of_type t_ in
           fprintf ppf "\n| %s x -> Abstract_value.VSumtype(%a)"
             (show_sanitized_name (bind_sanitize_capitalize name))
             (pp_sumtype f path) t_
         | t_ ->
           let f ppf t_ =
             fprintf ppf "LeafOr(Factori_utils.sanitized_of_str \"%s\",%a x)"
               (constructor_from_path path)
               output_value_of_type t_ in
           fprintf ppf "\n| %s x -> Abstract_value.VSumtype(%a)"
             (constructor_from_path path)
             (pp_sumtype f path) t_))
    l

and output_lift_sumtype ppf t_ =
  let l = sumtype_to_list t_ in
  fprintf ppf "(match arg with%a)"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "")
       (fun ppf (t_, path) ->
         match t_ with
         | Annot (u_, name) when is_unit u_ ->
           fprintf ppf "\n| %s -> Concrete (%s)"
             (show_sanitized_name (bind_sanitize_capitalize name))
             (show_sanitized_name (bind_sanitize_capitalize name))
         | Annot (t_, name) ->
           fprintf ppf "\n| %s x -> Concrete (%s (%a x))"
             (show_sanitized_name (bind_sanitize_capitalize name))
             (show_sanitized_name (bind_sanitize_capitalize name))
             output_lift_type t_
         | t_ ->
           fprintf ppf "\n| %s x -> Concrete (%s (%a x))"
             (constructor_from_path path)
             (constructor_from_path path)
             output_lift_type t_))
    l

and output_value_of_type ppf = function
  | Base b ->
    fprintf ppf "value_of_%a"
      (pp_base_type ~language:Factori_config.OCaml ~abstract:true ~prefix:None)
      b
  | Annot (ep, _) -> fprintf ppf "%a" output_value_of_type ep
  | Pair al ->
    fprintf ppf "(value_of_tuple%d %a)" (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf "\n")
         output_value_of_type)
      al
  | Record _ as t ->
    fprintf ppf "value_of_%s"
      (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t -> output_value_of_sumtype ppf t
  | TVar tn -> fprintf ppf "value_of_abstract_%s" (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "(value_of_%s %a)" (str_of_unary u) output_value_of_type a
  | Contract c ->
    fprintf ppf
      "(value_of_contract (*EzEncoding.destruct \
       Tzfunc.Proto.micheline_enc.json {|%a|}*))"
      pp_micheline_ c
  | Binary (b, (k, v)) ->
    fprintf ppf "(value_of_%s %a %a)" (str_of_binary b) output_value_of_type k
      output_value_of_type v

and output_lift_type ppf = function
  | Base b ->
    fprintf ppf "lift_%a"
      (pp_base_type ~language:Factori_config.OCaml ~abstract:true ~prefix:None)
      b
  | Annot (ep, _) -> fprintf ppf "%a" output_lift_type ep
  | Pair al ->
    fprintf ppf "(lift_tuple%d %a)" (List.length al)
      (pp_print_list ~pp_sep:(fun ppf _ -> fprintf ppf "\n") output_lift_type)
      al
  | Record _ as t ->
    fprintf ppf "lift_%s" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t -> output_lift_sumtype ppf t
  | TVar tn -> fprintf ppf "lift_%s" (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "(lift_%s %a)" (str_of_unary u) output_lift_type a
  | Contract c ->
    fprintf ppf
      "(lift_contract (*EzEncoding.destruct Tzfunc.Proto.micheline_enc.json \
       {|%a|}*))"
      pp_micheline_ c
  | Binary (b, (k, v)) ->
    fprintf ppf "(lift_%s %a %a)" (str_of_binary b) output_lift_type k
      output_lift_type v

and output_value_of_type_pretty ppf = function
  | Base b ->
    fprintf ppf "value_of_%a"
      (pp_base_type ~language:Factori_config.OCaml ~abstract:true ~prefix:None)
      b
  | Annot (ep, _name) ->
    (* fprintf ppf "%s_encode" name *) fprintf ppf "%a" output_value_of_type ep
  | Pair al ->
    fprintf ppf "(value_of_tuple%d %a)" (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf "\n")
         output_value_of_type_pretty)
      al
  | Record _ as t ->
    fprintf ppf "value_of_%s"
      (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "value_of_%s"
      (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn -> fprintf ppf "value_of_abstract_%s" (show_sanitized_name (fst tn))
  | Contract c ->
    fprintf ppf
      "(value_of_contract (*EzEncoding.destruct \
       Tzfunc.Proto.micheline_enc.json {|%a|}*))"
      pp_micheline_ c
  | Unary (u, a) ->
    fprintf ppf "(value_of_%s %a)" (str_of_unary u) output_value_of_type a
  | Binary (b, (k, v)) ->
    fprintf ppf "(%s_encode %a %a)" (str_of_binary b) output_value_of_type k
      output_value_of_type v

and output_lift_type_pretty ppf = function
  | Base b ->
    fprintf ppf "lift_%a"
      (pp_base_type ~language:Factori_config.OCaml ~abstract:true ~prefix:None)
      b
  | Annot (ep, _name) ->
    (* fprintf ppf "%s_encode" name *) fprintf ppf "%a" output_lift_type ep
  | Pair al ->
    fprintf ppf "(lift_tuple%d %a)" (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf "\n")
         output_lift_type_pretty)
      al
  | Record _ as t ->
    fprintf ppf "lift_%s" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "lift_%s" (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn -> fprintf ppf "lift_%s" (show_sanitized_name (fst tn))
  | Contract c ->
    fprintf ppf
      "(lift_contract (*EzEncoding.destruct Tzfunc.Proto.micheline_enc.json \
       {|%a|}*))"
      pp_micheline_ c
  | Unary (u, a) ->
    fprintf ppf "(lift_%s %a)" (str_of_unary u) output_lift_type a
  | Binary (b, (k, v)) ->
    fprintf ppf "(%s_encode %a %a)" (str_of_binary b) output_lift_type k
      output_lift_type v

and output_sumtype_content ppf t =
  let lst = sumtype_to_list t in
  fprintf ppf "%a"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "|")
       (fun ppf x ->
         let constructor_name, value =
           match x with
           | Annot (u_, name), _path when is_unit u_ ->
             (bind_sanitize_capitalize name, "")
           | Annot (t, name), _path ->
             ( bind_sanitize_capitalize name,
               Format.asprintf " of %a" output_type_pretty t )
           | t, path ->
             ( sanitized_of_str @@ constructor_from_path path,
               Format.asprintf " of %a" output_type_pretty t
               (* hopefully this does not happen too much or we will need another strategy *)
             ) in
         fprintf ppf "%s%s" (show_sanitized_name constructor_name) value))
    lst

(* Is this a duplicate of output_type_pretty? No *)
and output_type_content ppf = function
  | Base _ as t -> output_type_pretty ppf t
  | Annot (_, name) as t ->
    fprintf ppf "(* michelson name %s*)%a" (show_sanitized_name name)
      output_type_pretty t
  | Pair al ->
    let n = List.length al in
    fprintf ppf "(%a) tuple%d"
      (pp_print_list ~pp_sep:(fun ppf _ -> fprintf ppf ",") output_type_content)
      al n
  | Record (LeafP (_n, x)) -> fprintf ppf "%a" output_type_pretty x
  | Record (PairP l) as m ->
    let d =
      if not @@ is_one_field_record m then
        Curly
      else
        Nothing in
    let l = flattenP [] l in
    fprintf ppf "%s%a%s" (opening d)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ";")
         (fun ppf (n, x) ->
           fprintf ppf "%s : %a" (show_sanitized_name n) output_type_pretty x))
      l (closing d)
  | Or _ as t -> output_sumtype_content ppf t
  | TVar tn -> fprintf ppf "abstract_%s" (show_sanitized_name (fst tn))
  | Unary (u, a) -> fprintf ppf "(%a %s)" output_type_content a (str_of_unary u)
  | Binary (Lambda, (x, y)) ->
    fprintf ppf "(%a,%a) lambda" output_type_pretty x output_type_pretty y
  | Binary (b, (k, v)) ->
    fprintf ppf "(%a,%a) %s" output_type_pretty k output_type_pretty v
      (str_of_binary b)
  | Contract _c -> fprintf ppf "contract"

let rec extract_tuple argname ppf t =
  (* eprintf "entering extract_tuple with %s = %a%!\n" argname pp_type_ t; *)
  match t with
  | Annot (_, name) ->
    fprintf ppf "%s.%s" argname (show_sanitized_name (bind_sanitize_annot name))
  | Pair al ->
    fprintf ppf "(%a)"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (extract_tuple argname))
      al
  | t ->
    eprintf "In extract_tuple (ocaml):\n\t\t\t\tXXXX\n%a%!\n" pp_type_ t ;
    raise (ElementShouldHaveName (show_type_ t))

and value_of_record ppf ep =
  let rec aux ppf = function
    | LeafP (name, t) ->
      fprintf ppf
        "Abstract_value.LeafP(Factori_utils.sanitized_of_str \"%s\",%a arg.%s)"
        (show_sanitized_name name) output_value_of_type t
        (show_sanitized_name name)
    | PairP [] -> failwith "[value_of_record] never happens: PairP of []"
    | PairP [x1; x2] -> fprintf ppf "Abstract_value.PairP(%a,%a)" aux x1 aux x2
    | PairP (x :: l) ->
      fprintf ppf "Abstract_value.PairP(%a,%a)" aux x aux (PairP l)
    (* pp_pairedtype_ocaml_build
       *   (fun s ppf t ->
       *     fprintf ppf "(%a arg.%s)" output_value_of_type_pretty t
       *       (show_sanitized_name s))
       *   ppf p *) in
  aux ppf ep

and lift_record ppf ep =
  let rec aux ppf = function
    | LeafP (name, t) ->
      fprintf ppf "%s = %a arg.%s" (show_sanitized_name name) output_lift_type t
        (show_sanitized_name name)
    | PairP [] -> failwith "[lift_record] never happens: PairP of []"
    | PairP (x :: l) ->
      fprintf ppf "%a" aux x ;
      List.iter (fun item -> fprintf ppf ";\n%a" aux item) l ;
      fprintf ppf ""
    (* pp_pairedtype_ocaml_build
       *   (fun s ppf t ->
       *     fprintf ppf "(%a arg.%s)" output_lift_type_pretty t
       *       (show_sanitized_name s))
       *   ppf p *) in
  aux ppf ep

and declare_sumtype ppf t =
  let lst = sumtype_to_list t in
  fprintf ppf "%a"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "|")
       (fun ppf x ->
         let constructor_name, value =
           match x with
           | Annot (u_, name), _path when is_unit u_ ->
             (bind_sanitize_capitalize name, "")
           | Annot (t, name), _path ->
             ( bind_sanitize_capitalize name,
               Format.asprintf " of %a" output_type_pretty t )
           | t, path ->
             ( sanitized_of_str @@ constructor_from_path path,
               Format.asprintf " of %a" output_type_pretty t
               (* hopefully this does not happen too much or we will need another strategy *)
             ) in
         fprintf ppf "%s%s" (show_sanitized_name constructor_name) value))
    lst

and declare_type_ (m : type_) ppf name =
  let name = bind_sanitize_uncapitalize name in
  let san_name = show_sanitized_name name in
  let auxiliary_type_name = auxiliary_type_name ~type_name:san_name in
  let type_name = abstract_type_name ~type_name:auxiliary_type_name in
  (* Two groups here: those whose abstract types are id_or_concrete of
     the auxiliary, and those for which it is the same as the pretype*)
  match m with
  | Annot (ep, _) -> declare_type_ ep ppf name
  | TVar (vname, _) ->
    fprintf ppf "\ntype %s = %s\ntype %s = abstract_%s" auxiliary_type_name
      (show_sanitized_name vname)
      type_name
      (show_sanitized_name vname)
  | _ -> begin
    let abstract_is_same_as_auxiliary =
      match m with
      | Or _ ->
        let sumtype_content = Format.asprintf "%a" declare_sumtype m in
        fprintf ppf "\ntype %s = %s\n" auxiliary_type_name sumtype_content ;
        false
      | Base b ->
        fprintf ppf "type %s = %a" auxiliary_type_name
          (pp_base_type ~language:Factori_config.OCaml ~abstract:true
             ~prefix:None)
          b ;
        true
      | TVar (_vname, _) ->
        failwith "already considered above"
        (* fprintf ppf "type %s = %s" auxiliary_type_name (show_sanitized_name vname); true *)
      | Binary (Lambda, (x, y)) ->
        fprintf ppf "type %s = (%a,%a) lambda" auxiliary_type_name
          output_type_pretty x output_type_pretty y ;
        true
      | Binary (b, (k, v)) ->
        fprintf ppf "type %s = (%a,%a) %s" auxiliary_type_name
          output_type_pretty k output_type_pretty v (str_of_binary b) ;
        true
      | Unary (u, t) ->
        fprintf ppf "type %s = %a %s" auxiliary_type_name output_type_pretty t
          (str_of_unary u) ;
        true
      | Contract _t ->
        fprintf ppf "type %s = contract" auxiliary_type_name ;
        true
      | Annot (_ep, _) -> failwith "cannot happen"
      | Pair _ ->
        fprintf ppf "type %s = %a" auxiliary_type_name output_type_content m ;
        true
      | Record _ ->
        (* Case of a record type *)
        let record_content =
          Format.asprintf "%a"
            (fun ppf type_ -> output_type_content ppf type_)
            m in
        (* let auxiliary_type_name = auxiliary_type_name ~type_name:(show_sanitized_name name) in
         * let type_name = abstract_type_name ~type_name:auxiliary_type_name in *)
        fprintf ppf "\ntype %s = %s\n" auxiliary_type_name record_content ;
        false in
    if abstract_is_same_as_auxiliary then
      fprintf ppf "\ntype %s = %s\n" type_name auxiliary_type_name
    else
      fprintf ppf
        "\ntype %s = %s Factori_abstract_types.Abstract.id_or_concrete"
        type_name auxiliary_type_name
  end

and value_of_abstract_type ppf name =
  let name = bind_sanitize_uncapitalize name in
  let name = show_sanitized_name name in
  let auxiliary_name = auxiliary_type_name ~type_name:name in
  let abstract_name = abstract_type_name ~type_name:name in
  fprintf ppf
    "\n\
     let value_of_%s (x : %s) = match x with\n\
     | Id id -> Abstract_value.VAbstract (ScenId id)\n\
     | Concrete c -> (value_of_%s c)\n"
    abstract_name abstract_name auxiliary_name

and value_of_type_decl (m : type_) ppf name =
  let name = bind_sanitize_uncapitalize name in
  let auxiliary_san_name =
    auxiliary_type_name ~type_name:(show_sanitized_name name) in
  let abstract_san_name =
    abstract_type_name ~type_name:(show_sanitized_name name) in
  let prelude =
    asprintf "let value_of_%s : %s -> 'a = " auxiliary_san_name
      auxiliary_san_name in
  let prelude_with_arg =
    asprintf "let value_of_%s (arg : %s) : 'a = " auxiliary_san_name
      auxiliary_san_name in
  let postlude_sum_and_record = asprintf "%a" value_of_abstract_type name in
  let postlude_other_types =
    sprintf "\nlet value_of_%s = value_of_%s" abstract_san_name
      auxiliary_san_name in
  match m with
  | Base b ->
    fprintf ppf "%svalue_of_%a" prelude
      (pp_base_type ~language:Factori_config.OCaml ~abstract:true ~prefix:None)
      b ;
    fprintf ppf "%s" postlude_other_types
  | Annot (ep, _) ->
    fprintf ppf "%a" (value_of_type_decl ep) name ;
    fprintf ppf "%s" postlude_other_types
  | TVar tn ->
    fprintf ppf "\n%svalue_of_%s" prelude (show_sanitized_name (fst tn)) ;
    fprintf ppf "\nlet value_of_%s = value_of_abstract_%s" abstract_san_name
      (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "%s(value_of_%s (%a)) arg" prelude_with_arg (str_of_unary u)
      output_value_of_type_pretty a ;
    fprintf ppf "%s" postlude_other_types
  | Contract c ->
    fprintf ppf
      "%s(value_of_contract (*EzEncoding.destruct \
       Tzfunc.Proto.micheline_enc.json {|%a|}*) arg)"
      prelude_with_arg pp_micheline_ c ;
    fprintf ppf "%s" postlude_other_types
  | Binary (b, (k, v)) ->
    fprintf ppf "%s(value_of_%s (%a) (%a))" prelude (str_of_binary b)
      output_value_of_type k output_value_of_type v ;
    fprintf ppf "%s" postlude_other_types
  | Or _ as t ->
    fprintf ppf "let value_of_%s (arg : %s) = %a" auxiliary_san_name
      auxiliary_san_name output_value_of_sumtype t ;
    fprintf ppf "%s" postlude_sum_and_record
  | Pair _ as t ->
    fprintf ppf "let value_of_%s = %a" auxiliary_san_name output_value_of_type t ;
    fprintf ppf "%s" postlude_other_types
  | Record r ->
    fprintf ppf "let value_of_%s (arg : %s) = Abstract_value.VRecord (%a)"
      auxiliary_san_name auxiliary_san_name value_of_record r ;
    fprintf ppf "%s" postlude_sum_and_record

and lift_type_decl (m : type_) ppf name =
  let name = bind_sanitize_uncapitalize name in
  let auxiliary_san_name =
    auxiliary_type_name ~type_name:(show_sanitized_name name) in
  let abstract_san_name =
    abstract_type_name ~type_name:(show_sanitized_name name) in
  let prelude =
    asprintf "let lift_%s : 'a -> %s = " auxiliary_san_name abstract_san_name
  in
  let prelude_with_arg =
    asprintf "let lift_%s (arg : Interface.%s) : %s = " auxiliary_san_name
      auxiliary_san_name auxiliary_san_name in
  match m with
  | Base b ->
    fprintf ppf "%slift_%a" prelude
      (pp_base_type ~language:Factori_config.OCaml ~abstract:true ~prefix:None)
      b
  | Annot (ep, _) -> fprintf ppf "%a" (lift_type_decl ep) name
  | TVar tn -> fprintf ppf "\n%slift_%s" prelude (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "%s(lift_%s (%a)) arg" prelude_with_arg (str_of_unary u)
      output_lift_type_pretty a
  | Contract c ->
    fprintf ppf
      "%s(lift_contract (*EzEncoding.destruct Tzfunc.Proto.micheline_enc.json \
       {|%a|}*) arg)"
      prelude_with_arg pp_micheline_ c
  | Binary (b, (k, v)) ->
    fprintf ppf "%s(lift_%s (%a) (%a))" prelude (str_of_binary b)
      output_lift_type k output_lift_type v
  | Or _ as t ->
    fprintf ppf "let lift_%s (arg : Interface.%s) : %s = %a" auxiliary_san_name
      auxiliary_san_name abstract_san_name output_lift_sumtype t
  | Pair _ as t ->
    fprintf ppf "let lift_%s = %a" auxiliary_san_name output_lift_type t
  | Record r ->
    fprintf ppf "let lift_%s (arg : Interface.%s) : %s = Concrete {%a}"
      auxiliary_san_name auxiliary_san_name abstract_san_name lift_record r

let mk_deploy ~storage_name ~contract_name ppf () =
  let storage_name = show_sanitized_name storage_name in
  fprintf ppf
    "let mk_deploy ~scenario ~from ~amount ~network ~storage =\n\
    \  let open Scenario_dsl.AstInterface in\n\
    \  let storage_value =\n\
    \    make_value scenario (value_of_abstract_%s storage)\n\
    \  in\n\n\
    \   mk_deploy ~scenario ~from ~amount ~network ~storage:storage_value \
     ~contract_name:\"%s\""
    storage_name contract_name

let print_original_blockchain_storage ~storage_name ~final_storage_type_ ~kt1
    ~network ppf () =
  let open Tzfunc.Rp in
  let open Tzfunc.Proto in
  match
    Lwt_main.run
      (let>? storage =
         Factori_config.get_storage ~network
           ~debug:(!Factori_options.verbosity > 0)
           kt1 in
       (* add_modules_to_ocaml_file ~file_must_exist:false
        *   ~path:scenario_example_file
        *   [Factori_config.get_ocaml_interface_basename ~contract_name] ; *)
       try
         let initial_blockchain_storage_type =
           abstract_type_name ~type_name:(show_sanitized_name storage_name)
         in
         fprintf ppf
           "\n\
            let abstract_initial_blockchain_storage : %s = lift_%s \
            Interface.initial_blockchain_storage\n\n"
           initial_blockchain_storage_type
           (show_sanitized_name storage_name) ;
         let initial_blockchain_storage_type =
           auxiliary_type_name ~type_name:(show_sanitized_name storage_name)
         in
         fprintf ppf
           "\n\
            let initial_blockchain_storage : %s =\n\
            match abstract_initial_blockchain_storage with\n\
            | Concrete x -> %s\n\
            | _ -> failwith \"[initial_blockchain_storage] initial blockchain \
            storage should not be an Id\"\n\n"
           initial_blockchain_storage_type
           (match final_storage_type_ with
           | TVar (_, Pair _) | Pair _ -> "Concrete x"
           | Or _ | TVar (_, Or _) -> "x"
           | TVar (_, Record _) | Record _ -> "x"
           | _ -> "Concrete x") ;
         Lwt.return_ok ()
       with e ->
         handle_exception e ;
         fprintf ppf
           "\n\
            let initial_blockchain_storage = failwith \"Could not download \
            initial storage, this is a bug\"\n\
            (*%s*)\n"
           (EzEncoding.construct micheline_enc.json storage) ;
         Lwt.return_ok ())
  with
  | Ok () -> ()
  | Error e -> print_error e

let call_entrypoint ~contract_name ppf name =
  let name =
    {
      name with
      sanitized = String.uncapitalize_ascii (show_sanitized_name name);
    } in
  (* let real_name =
   *   try Infer_entrypoints.Naming.get_real_name name with _ -> name in *)
  let name = show_sanitized_name name in
  fprintf ppf
    "let mk_call_%s ~scenario ~from ~amount ~kt1 ~network ~param =\n\
    \  let open Scenario_dsl.AstInterface in\n\
    \  let param_value = make_value scenario (value_of_abstract_%s (Concrete \
     param) ) in\n\
    \  mk_call ~scenario ~from ~amount ~entrypoint:\"%s\" ~kt1 ~network \
     ~contract_name:\"%s\" ~param:param_value\n\n"
    name name name contract_name ;
  fprintf ppf
    (* let assert_failwith_str_update_operators ~network
     *      ~amount ~scenario ~from ~kt1 ~expected ~msg param =
     *   let open Scenario_dsl.AstInterface in
     *   let param_value =
     *     make_value scenario (value_of_abstract_update_operators (Concrete param))
     *   in
     *   mk_failed_call
     *     ~expected
     *     ~msg
     *     ~network
     *     ~amount
     *     ~from
     *     ~kt1
     *     param_value *)
    "let assert_failwith_str_%s ~scenario ~network ~amount ~from ~kt1 \
     ~expected ~msg param =\n\
     let open Scenario_dsl.AstInterface in\n\
     let param_value = make_value scenario (value_of_abstract_%s (Concrete \
     param) ) in\n\
     (mk_failed_call ~scenario ~entrypoint:\"%s\" ~expected ~msg ~network \
     ~amount ~from ~kt1 ~contract_name:\"%s\" param_value)\n\n"
    name name name contract_name
(* ;
   * fprintf ppf
   *   "let assert_failwith_generic_%s ?(node = Blockchain.default_node) \
   *    ?(debug=false) ?(amount=0L) ~scenario ~enc ~from ~kt1 ~expected ~msg param =\n\
   *    let open Scenario_dsl.AstInterface in\n\
   *    let param_value = make_value scenario (value_of_abstract_%s (Concrete param) ) in\n\
   *   (Interface.assert_failwith_str_%s ~enc ~expected ~msg ~debug ~node ~amount ~from ~kt1 param_value)\n\n"
   *   name name name *)

let rec pp_interface_instruction_ocaml ~contract_name ~storage_name ~network ppf
    ii =
  match ii with
  | Define_type (name, type_) ->
    fprintf ppf "%a\n%a\n%a\n" (declare_type_ type_) name
      (value_of_type_decl type_) name (lift_type_decl type_) name
  | Deploy ->
    fprintf ppf "(** Deploy*)\n%a" (mk_deploy ~storage_name ~contract_name) ()
  | Seq ii -> (pp_interface_ocaml ~contract_name ~storage_name ~network) ppf ii
  | Entrypoint (name, _) -> call_entrypoint ~contract_name ppf name

and pp_interface_ocaml ~network ~contract_name ~storage_name (ppf : formatter)
    (i : interface) =
  output_verbose ~level:3
    (asprintf "Entering pp_interface_ocaml with interface %a" pp_interface i) ;
  pp_print_list
    ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
    (pp_interface_instruction_ocaml ~contract_name ~storage_name ~network)
    ppf i

let process_entrypoints ~contract_name ~storage_name ~final_storage_type_
    ~network ?(kt1 = None) ppf interface =
  let contract_name = show_sanitized_name contract_name in
  output_verbose ~level:2
    (asprintf
       "Entering process_entrypoints with final storage: %a\n and interface %a"
       pp_type_ final_storage_type_ pp_interface interface) ;
  let interface =
    try_overflow ~msg:"extract interface ocaml" (fun () -> interface) in
  let _ = check_interface_for_doubles interface in
  (* eprintf "%a\n" pp_interface (extract_interface ~storage eps); *)
  (* eprintf "%a\n" pp_interface_ocaml (extract_interface ~storage eps); *)
  let original_blockchain_storage =
    match kt1 with
    | None -> ""
    | Some kt1 ->
      Format.asprintf "\n%a"
        (print_original_blockchain_storage ~storage_name ~final_storage_type_
           ~kt1 ~network)
        () in
  fprintf ppf
    "open Factori_abstract_types.Abstract\n\
     module Interface = %s_ocaml_interface\n\n\
     %a%s"
    (String.capitalize_ascii contract_name)
    (pp_interface_ocaml ~contract_name ~storage_name ~network)
    interface original_blockchain_storage
