open Pair_type
open Factori_utils

type value =
  | VUnit of unit
  | VInt of Z.t
  | VString of string
  | VBytes of Hex.hex
  | VTimestamp of string
  | VBool of bool
  | VAddress of string
  | VTez of Z.t
  | VKey of string
  | VKeyHash of string
  | VSignature of string
  | VOperation of string
  | VTuple of value list
  | VBigMap of Z.t
  | VMap of (value * value) list
  | VSet of value list
  | VList of value list
  | VOption of value option
  | VLambda of Tzfunc.Proto.script_expr
  | VRecord of value paired_type
  | VSumtype of string * value
  | VChain_id of string
  | VTicket of string
  | VSaplingState of string
(* [@@deriving encoding { recursive }] *)
