(* open Tzfunc.Proto *)
open Format
open Infer_entrypoints
open Pair_type
open Or_type
open Value
open Types
open Factori_utils

let rec pp_value_to_typescript ppf = function
  | VChain_id s -> fprintf ppf "\"%s\"" s
  | VUnit _ -> fprintf ppf "null"
  | VKeyHash kh -> fprintf ppf "\"%s\"" kh
  | VKey k -> fprintf ppf "\"%s\"" k
  | VSignature s -> fprintf ppf "\"%s\"" s
  | VOperation o -> fprintf ppf "\"%s\"" o
  | VOption None -> fprintf ppf "null"
  | VOption (Some x) -> fprintf ppf "%a" pp_value_to_typescript x
  | VInt z -> print_z_int_typescript ppf z
  | VTez z -> print_z_int_typescript ppf z
  | VString s -> fprintf ppf "\"%s\"" s
  | VBytes s -> fprintf ppf "\"%s\"" (s :> string)
  | VTimestamp s -> fprintf ppf "(\"%s\")" s
  | VBool b -> fprintf ppf "%b" b
  | VAddress addr -> fprintf ppf "\"%s\"" addr
  | VSumtype (constructor, v) -> (
    (* TODO: understand how to
       reconstruct the proper constructor
       names *)
    match v with
    | VUnit () ->
      fprintf ppf "/* TODO: fix constructors */{ kind : \"%s_constructor\" }"
        constructor
    | _ ->
      fprintf ppf
        "/* TODO: fix constructors */{ kind : \"%s_constructor\", %s_element : \
         (%a)}"
        constructor constructor pp_value_to_typescript v
      (* fprintf ppf "%s (%a)" constructor pp_value_to_typescript v *))
  | VMap m ->
    fprintf ppf "[%a]"
      (pp_print_list ~pp_sep:(tag ",") (fun ppf (x, y) ->
           fprintf ppf "[%a,%a]" pp_value_to_typescript x pp_value_to_typescript
             y))
      m
  | VSet m ->
    fprintf ppf "[%a]"
      (pp_print_list ~pp_sep:(tag ",") pp_value_to_typescript)
      m
  | VList m ->
    fprintf ppf "[%a]"
      (pp_print_list ~pp_sep:(tag ",") pp_value_to_typescript)
      m
  | VBigMap i ->
    fprintf ppf "{ kind : 'abstract', value : %a}" print_z_int_typescript i
  | VTuple vl ->
    fprintf ppf "[%a]"
      (pp_print_list ~pp_sep:(tag ",") pp_value_to_typescript)
      vl
  | VLambda l ->
    fprintf ppf
      "{from : {int : BigInt(0).toString()}, to_ : {int : \
       BigInt(0).toString()}, body : new Parser().parseJSON(%s)}"
      (EzEncoding.construct Tzfunc.Proto.script_expr_enc.json l)
  | VRecord (LeafP (field_name, v)) ->
    fprintf ppf "{%s : %a}"
      (show_sanitized_name field_name)
      pp_value_to_typescript v
  | VRecord (PairP _ as p) ->
    fprintf ppf "{%a}"
      (pp_pairedtype_typescript ~pairparens:false
         ~f_leaf:(fun name ppf x ->
           fprintf ppf "%s : %a" (show_sanitized_name name)
             pp_value_to_typescript x)
         ~f_pair:(fun ppf _d -> fprintf ppf "")
         ~delimiters:Nothing ~sep:",\n")
      p
  | VTicket s -> fprintf ppf "\"%s\"" s
  | VSaplingState s -> fprintf ppf "\"%s\"" s

(* Some day, on some contract, this will get more complicated *)
let rec get_constructor ~prefix b ppf type_ =
  Format.fprintf ppf "%s of (%a)"
    (if b then
      "Left"
    else
      "Right")
    (output_type_content ~prefix ~initial:false)
    type_

(* the 'initial' flag is used to build a 'initial_t' for each type
   't', which is used by Taquito for the deployment of contracts. *)
and output_type_pretty ?(initial = false) ~prefix ppf =
  let output_type_pretty = output_type_pretty ~initial in
  function
  | Base String -> fprintf ppf "string"
  | Base b ->
    fprintf ppf "%a"
      (pp_base_type ~language:Factori_config.Typescript ~abstract:false
         ~prefix:(Some prefix))
      b
  | Annot (ep, _name) -> fprintf ppf "%a" (output_type_pretty ~prefix) ep
  | Pair al ->
    fprintf ppf "[%a]"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_pretty ~prefix))
      al
  | Record _ as t ->
    fprintf ppf "%s" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "%s" (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn -> fprintf ppf "%s" (show_sanitized_name (fst tn))
  | Unary (List, a) | Unary (Set, a) ->
    fprintf ppf "%a[]" (output_type_pretty ~prefix) a
  | Unary (Ticket, t) ->
    fprintf ppf "%s.ticket<%a>" prefix (output_type_pretty ~prefix) t
  | Contract _c ->
    fprintf ppf "%s.contract" prefix (* (output_type_pretty ~prefix) c *)
  | Binary (BigMap, (k, v)) ->
    if initial then
      fprintf ppf "MichelsonMap<any,any>"
    else
      fprintf ppf "%s.big_map<%a,%a> " prefix
        (output_type_pretty ~prefix)
        k
        (output_type_pretty ~prefix)
        v
  | Binary (Lambda, (k, v)) ->
    fprintf ppf "%s.lambda<%a,%a> " prefix
      (output_type_pretty ~prefix)
      k
      (output_type_pretty ~prefix)
      v
  | Binary (Map, (k, v)) ->
    if initial then
      fprintf ppf "MichelsonMap<any,any>"
    else
      fprintf ppf "%s.map<%a,%a> " prefix
        (output_type_pretty ~prefix)
        k
        (output_type_pretty ~prefix)
        v
  | Unary (Option, a) ->
    fprintf ppf "%s.option<%a>" prefix (output_type_pretty ~prefix) a

and output_type_generator ~prefix ppf t =
  let output_type_generator = output_type_generator ~prefix in
  match t with
  | Base Int -> fprintf ppf "%s.big_int_generator" prefix
  | Base b ->
    fprintf ppf "%a_generator"
      (pp_base_type ~language:Factori_config.Typescript ~abstract:false
         ~prefix:(Some prefix))
      b
  | Annot (ep, _) -> fprintf ppf "%a" output_type_generator ep
  | Pair al ->
    fprintf ppf "(%s.tuple%d_generator(%a))" prefix (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",\n")
         output_type_generator)
      al
  | Record _ as t ->
    fprintf ppf "%s_generator"
      (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t -> output_sumtype_generator ~prefix ppf t
  | TVar tn -> fprintf ppf "%s_generator" (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "(%s.%s_generator(%a))" prefix (str_of_unary u)
      output_type_generator a
  | Contract _c -> fprintf ppf "%s.contract_generator" prefix
  | Binary (b, (k, v)) ->
    fprintf ppf "(%s.%s_generator(%a,%a))" prefix (str_of_binary b)
      output_type_generator k output_type_generator v

and output_type_encode ~prefix ppf = function
  | Base b ->
    fprintf ppf "%a_encode"
      (pp_base_type ~language:Factori_config.Typescript ~abstract:false
         ~prefix:(Some prefix))
      b
  | Annot (ep, _) -> fprintf ppf "%a" (output_type_encode ~prefix) ep
  | Pair al ->
    fprintf ppf "%s.tuple%d_encode(%a)" prefix (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_encode ~prefix))
      al
  | Record _ as t ->
    fprintf ppf "%s_encode" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "%s_encode" (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn -> fprintf ppf "%s_encode" (show_sanitized_name (fst tn))
  | Unary (List, a) ->
    fprintf ppf "%s.list_encode(%a)" prefix (output_type_encode ~prefix) a
  | Unary (Set, a) ->
    fprintf ppf "%s.set_encode(%a)" prefix (output_type_encode ~prefix) a
  | Unary (Ticket, t) ->
    fprintf ppf "%s.ticket_encode(%a)" prefix (output_type_encode ~prefix) t
  | Contract c ->
    fprintf ppf "%s.contract_encode(new Parser().parseJSON(%a))" prefix
      pp_micheline_ c (* (output_type_encode ~prefix) c *)
  | Binary (BigMap, (k, v)) ->
    fprintf ppf "%s.big_map_encode(%a,%a)" prefix
      (output_type_encode ~prefix)
      k
      (output_type_encode ~prefix)
      v
  | Binary (Lambda, (k, v)) ->
    fprintf ppf "%s.lambda_encode(%a,%a)" prefix
      (output_type_encode ~prefix)
      k
      (output_type_encode ~prefix)
      v
  | Binary (Map, (k, v)) ->
    fprintf ppf "%s.map_encode(%a,%a)" prefix
      (output_type_encode ~prefix)
      k
      (output_type_encode ~prefix)
      v
  | Unary (Option, a) ->
    fprintf ppf "%s.option_encode(%a)" prefix (output_type_encode ~prefix) a

and output_type_decode ~prefix ppf = function
  | Base b ->
    fprintf ppf "%s.%a_decode" prefix
      (pp_base_type ~language:Factori_config.Typescript ~abstract:false
         ~prefix:None)
      b
  | Annot (ep, _) -> fprintf ppf "%a" (output_type_decode ~prefix) ep
  | Pair al ->
    fprintf ppf "%s.tuple%d_decode(%a)" prefix (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_decode ~prefix))
      al
  | Record _ as t ->
    fprintf ppf "%s_decode" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "%s_decode" (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn -> fprintf ppf "%s_decode" (show_sanitized_name (fst tn))
  | Unary (List, a) ->
    fprintf ppf "%s.list_decode(%a)" prefix (output_type_decode ~prefix) a
  | Unary (Set, a) ->
    fprintf ppf "%s.set_decode(%a)" prefix (output_type_decode ~prefix) a
  | Unary (Ticket, t) ->
    fprintf ppf "%s.ticket_decode(%a)" prefix (output_type_decode ~prefix) t
  | Contract c ->
    fprintf ppf "%s.contract_decode(new Parser().parseJSON(%a))" prefix
      pp_micheline_ c
  | Binary (BigMap, (k, v)) ->
    fprintf ppf "%s.big_map_decode(%a,%a)" prefix
      (output_type_decode ~prefix)
      k
      (output_type_decode ~prefix)
      v
  | Binary (Lambda, (k, v)) ->
    fprintf ppf "%s.lambda_decode(%a,%a)" prefix
      (output_type_decode ~prefix)
      k
      (output_type_decode ~prefix)
      v
  | Binary (Map, (k, v)) ->
    fprintf ppf "%s.map_decode(%a,%a)" prefix
      (output_type_decode ~prefix)
      k
      (output_type_decode ~prefix)
      v
  | Unary (Option, a) ->
    fprintf ppf "%s.option_decode(%a)" prefix (output_type_decode ~prefix) a

and output_sumtype_generator ~prefix ppf t_ =
  let lst = sumtype_to_list t_ in
  fprintf ppf "\n%s.chooseFrom\n([%a])\n" prefix
    (pp_print_list ~pp_sep:(tag ",") (fun ppf (t_, path) ->
         fprintf ppf "(%a)"
           (fun ppf at ->
             match at with
             | Annot (u_, name) when is_unit u_ ->
               fprintf ppf "{ kind : \"%s_constructor\"}"
                 (show_sanitized_name (bind_sanitize_capitalize name))
             | Annot (t_, name) ->
               fprintf ppf "{ kind : \"%s_constructor\", value:  (%a ())}"
                 (show_sanitized_name (bind_sanitize_capitalize name))
                 (output_type_generator ~prefix)
                 t_
             | t ->
               fprintf ppf "{ kind : \"%s\", value : (%a ())}"
                 (sprintf "%s_constructor" (constructor_from_path path))
                 (output_type_generator ~prefix)
                 t)
           t_))
    lst

and output_sumtype_micheline ~prefix ppf or_t =
  pp_ortype_typescript
    (fun ppf t_ ->
      match t_ with
      | Annot (u_, _annot) when is_unit u_ ->
        fprintf ppf "%s.unit_micheline" prefix
      | t_ -> fprintf ppf "%a" (output_type_micheline ~prefix) t_)
    ppf or_t

and output_type_micheline ~prefix ppf t =
  let output_type_micheline = output_type_micheline ~prefix in
  match t with
  | Base b ->
    fprintf ppf "%a_micheline"
      (pp_base_type ~language:Factori_config.Typescript ~abstract:false
         ~prefix:(Some prefix))
      b
  | Annot (ep, _annot) -> fprintf ppf "(%a)" output_type_micheline ep
  | Pair al ->
    fprintf ppf "%s.tuple%d_micheline(%a)" prefix (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",\n")
         output_type_micheline)
      al
  | Record _ as t ->
    fprintf ppf "%s_micheline"
      (show_sanitized_name (Naming.get_or_create_name t))
  | Or (_ as t) -> output_sumtype_micheline ~prefix ppf t
  | TVar tn -> fprintf ppf "%s_micheline" (show_sanitized_name (fst tn))
  | Unary (List, a) ->
    fprintf ppf "(%s.list_micheline (%a))" prefix output_type_micheline a
  | Unary (Set, a) ->
    fprintf ppf "(%s.set_micheline (%a))" prefix output_type_micheline a
  | Unary (Ticket, t) ->
    fprintf ppf "(%s.ticket_micheline (%a))" prefix output_type_micheline t
  | Contract c ->
    fprintf ppf "(%s.contract_micheline (new Parser().parseJSON(%a)))" prefix
      pp_micheline_ c
  | Binary (BigMap, (k, v)) ->
    fprintf ppf "%s.big_map_micheline(%a,%a)" prefix output_type_micheline k
      output_type_micheline v
  | Binary (Lambda, (x, y)) ->
    fprintf ppf "(%s.lambda_micheline(%a,%a))" prefix output_type_micheline x
      output_type_micheline y
  | Binary (Map, (k, v)) ->
    fprintf ppf "(%s.map_micheline (%a,%a))" prefix output_type_micheline k
      output_type_micheline v
  | Unary (Option, a) ->
    fprintf ppf "(%s.option_micheline(%a))" prefix output_type_micheline a

(* Is this a duplicate of output_type_pretty? *)
and output_type_content ?(initial = false) ~prefix ppf =
  let output_type_pretty = output_type_pretty ~initial in
  let output_type_content = output_type_content ~initial in
  function
  | Base _ as t -> (output_type_pretty ~prefix) ppf t
  | Annot (_, _) as t -> (output_type_pretty ~prefix) ppf t
  | Pair al ->
    fprintf ppf "[%a]"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_content ~prefix))
      al
  | Record (LeafP (_n, x)) -> fprintf ppf "%a" (output_type_pretty ~prefix) x
  | Record (PairP l) ->
    let l = flattenP [] l in
    fprintf ppf "%a"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (fun ppf (n, x) ->
           fprintf ppf "%s : %a" (show_sanitized_name n)
             (output_type_pretty ~prefix)
             x))
      l
  (* | Record _ -> fprintf ppf "<TODO: record type content>" *)
  | Or (OrAbs (a1, a2)) ->
    fprintf ppf "| %a | %a"
      (get_constructor ~prefix true)
      (Or a1)
      (get_constructor ~prefix false)
      (Or a2)
  | Or (LeafOr _) -> fprintf ppf "LeafOr TODO"
  | TVar tn -> fprintf ppf "%s" (show_sanitized_name (fst tn))
  | Unary (List, a) ->
    fprintf ppf "%s.list<%a>" prefix (output_type_content ~prefix) a
  | Unary (Set, a) -> fprintf ppf "%a[]" (output_type_content ~prefix) a
  | Contract _c -> fprintf ppf "%s.contract" prefix (* pp_micheline_ c *)
  | Binary (BigMap, (k, v)) ->
    if initial then
      fprintf ppf "MichelsonMap<any,any>"
    else
      fprintf ppf "%s.big_map<%a,%a>" prefix
        (output_type_content ~prefix)
        k
        (output_type_content ~prefix)
        v
  | Binary (Lambda, (k, v)) ->
    fprintf ppf "%s.lambda<%a,%a>" prefix
      (output_type_content ~prefix)
      k
      (output_type_content ~prefix)
      v
  | Binary (Map, (k, v)) ->
    if initial then
      fprintf ppf "MichelsonMap<any,any>"
    else
      fprintf ppf "%s.map<%a,%a>" prefix
        (output_type_content ~prefix)
        k
        (output_type_content ~prefix)
        v
  | Unary (Option, a) ->
    fprintf ppf "%s.option<%a>" prefix (output_type_content ~prefix) a
  | Unary (Ticket, t) ->
    fprintf ppf "%s.ticket<%a>" prefix (output_type_content ~prefix) t

let declare_record ~prefix name ppf ep =
  let output_type_pretty = output_type_pretty ~initial:false in
  let rec aux ?(first_call = false) ?(name = sanitized_of_str "NoName") =
    function
    | Base _ as t ->
      if first_call then
        fprintf ppf "%a" (output_type_pretty ~prefix) t
      else
        fprintf ppf "%s : %a" (show_sanitized_name name)
          (output_type_pretty ~prefix)
          t
    | Annot (ep, name) -> aux ~name:(bind_sanitize_annot name) ep
    | Pair al ->
      let al =
        let rec flatten res = function
          | [] -> res
          | Pair bl :: xs ->
            let res1 = flatten res bl in
            flatten res1 xs
          | x :: xs -> flatten (x :: res) xs in
        List.rev (flatten [] al) in
      List.iter
        (fun a ->
          let _ = aux a ~name in
          fprintf ppf ",")
        al
    | t ->
      fprintf ppf "%s : %a" (show_sanitized_name name)
        (output_type_pretty ~prefix)
        t in
  aux ~first_call:true ~name ep

let rec extract_tuple argname ppf = function
  | Annot (_, name) ->
    fprintf ppf "%s.%s" argname (show_sanitized_name (bind_sanitize_annot name))
  | Pair al ->
    fprintf ppf "[%a]"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (extract_tuple argname))
      al
  | _t ->
    (* eprintf "In extract_tuple (typescript):\n\t\t\t\tXXXX\n%a%!\n" pp_type_ t; *)
    (* raise (ElementShouldHaveName (show_type_ t)) *)
    fprintf ppf "<TODO Extract tuple %s>" argname

and encode_record ~prefix ppf ep =
  let aux = function
    | LeafP (_n, t) -> fprintf ppf "%a" (output_type_encode ~prefix) t
    | PairP _ as p ->
      pp_pairedtype_typescript_build
        (fun s ppf t ->
          fprintf ppf "(%a(arg.%s))"
            (output_type_encode ~prefix)
            t (show_sanitized_name s))
        ppf p in
  aux ep

and generator_record ~prefix ppf ep =
  let aux = function
    | LeafP (_n, x) -> fprintf ppf "%a ()" (output_type_generator ~prefix) x
    | PairP l ->
      let l = flattenP [] l in
      fprintf ppf "{%a}"
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf ",")
           (fun ppf (n, x) ->
             fprintf ppf "%s : %a ()" (show_sanitized_name n)
               (output_type_generator ~prefix)
               x))
        l
    (* | (LeafP(_n,t)) -> fprintf ppf "%a" output_type_generator t
     *   | (PairP _ as p) -> *) in
  aux ep

and decode_record ~prefix ppf ep =
  let output_type_decode = output_type_decode ~prefix in
  let aux = function
    | LeafP (_, t) -> fprintf ppf "%a" output_type_decode t
    | PairP _ as p ->
      pp_pairedtype_typescript ~pairparens:true
        ~f_leaf:(fun _name ppf x -> output_type_decode ppf x)
        ~f_pair:(fun ppf d -> fprintf ppf "%s.tuple%d_decode " prefix d)
        ~delimiters:Parens ~sep:"," ppf p in
  aux ep

and micheline_record ~prefix ppf ep =
  let aux = function
    | LeafP (_n, t) -> fprintf ppf "%a" (output_type_micheline ~prefix) t
    | PairP _ as p ->
      pp_pairedtype_typescript_build
        (fun _s ppf t -> fprintf ppf "(%a)" (output_type_micheline ~prefix) t)
        ppf p in
  aux ep

let record_to_list ep =
  (* path is how we get to the element in the tuple *)
  let rec aux ~path = function
    | LeafP (field_name, t) -> [(field_name, (path, t))]
    | PairP l -> List.concat (List.mapi (fun i x -> aux ~path:(path @ [i]) x) l)
    (* | Base _ as t-> [name,(path,t)]
     * | Annot(ep,name) -> aux ~path ~name:(sanitize_annot name) ep
     * | Pair al->
     *    (\* let n = List.length al in *\)
     *    List.concat (List.mapi (fun i x -> aux ~path:(path@[i]) x) al)
     * | t  -> [name,(path,t)] *) in
  aux ~path:[] (* ~name:name *) ep

let sumtype_var_index = ref 0

let get_fresh_index () =
  let res = !sumtype_var_index in
  incr sumtype_var_index ;
  res
(* let sumtype_names = Hashtbl.create 100
 * let add_name (t : type_) name = Hashtbl.add sumtype_names t name *)
(* let get_name t =
 *   match Hashtbl.find_opt sumtype_names t with
 *   | None -> let i = get_fresh_index () in
 *             let res = "sumtype"^(string_of_int i) in
 *             add_name t res; res
 *   | Some name -> name *)

let define_sumtype_subtypes ~typename ~prefix ~initial ppf t =
  (* if initial is set to true, then we have already define these
     'subtypes', and there is nothing to print *)
  if initial then
    fprintf ppf ""
  else
    let lst = sumtype_to_list t in
    let rec aux res = function
      | [] -> res
      | (t_, path) :: xs ->
        let new_res =
          let cur_elem =
            match t_ with
            | Annot (u_, name) when is_unit u_ ->
              let name = show_sanitized_name @@ bind_sanitize_capitalize name in
              Format.asprintf
                "type %s_%s_constructor_subtype = { kind : \"%s_constructor\"}"
                (show_sanitized_name typename)
                name name
            | Annot (t, name) ->
              let name = show_sanitized_name @@ bind_sanitize_capitalize name in
              Format.asprintf
                "type %s_%s_constructor_subtype = { kind : \"%s_constructor\"; \
                 %s_element : %a}"
                (show_sanitized_name typename)
                name name name
                (output_type_pretty ~prefix ~initial:false)
                t
            | t ->
              let name = constructor_from_path path in
              Format.asprintf
                "type %s_%s_subtype = { kind : \"%s_constructor\"; %s_element \
                 : %a}"
                (show_sanitized_name typename)
                name name name
                (output_type_pretty ~prefix ~initial:false)
                t in
          cur_elem :: res in
        aux new_res xs in
    fprintf ppf "\n%s\n" (String.concat "\n" (aux [] lst))

let pp_sumtype f l ppf x =
  let rec aux ppf = function
    | true :: a -> fprintf ppf "{prim : 'Left',\nargs: [%a]}\n" aux a
    | false :: b -> fprintf ppf "{prim : 'Right',\nargs : [%a]}\n" aux b
    | [] -> fprintf ppf "%a" f x in
  aux ppf l

let output_sumtype_encode ~prefix argname ppf t =
  (* TODO for now, we assume the layout is "comb"; eventually we
     need to support "tree" as well *)
  let lst = sumtype_to_list t in
  fprintf ppf "\nswitch(%s.kind){\n%a\n}" argname
    (pp_print_list ~pp_sep:(tag "\n") (fun ppf (k, path) ->
         match k with
         | Annot (t, name) ->
           let name = show_sanitized_name @@ bind_sanitize_capitalize name in
           fprintf ppf "case(\"%s_constructor\"):\nreturn %a" name
             (pp_sumtype
                (fun ppf at ->
                  match t with
                  | u_ when is_unit u_ ->
                    fprintf ppf "functolib.unit_encode(null)"
                  | _ ->
                    fprintf ppf "%a(%s.%s_element)"
                      (output_type_encode ~prefix)
                      at argname name)
                path)
             t
         | t_local ->
           let name = constructor_from_path path in
           fprintf ppf "case(\"%s_constructor\"):\nreturn %a" name
             (pp_sumtype
                (fun ppf _at ->
                  match t with
                  | u_ when is_unit u_ ->
                    fprintf ppf "functolib.unit_encode(null)"
                  | _ ->
                    fprintf ppf "%a(%s.%s_element)"
                      (output_type_encode ~prefix)
                      t_local argname name)
                path)
             t))
    lst

and output_sumtype_decode ~prefix argname ppf t_ =
  let l = sumtype_to_list t_ in
  fprintf ppf
    "let p = %s.retrieve_path_from_sumtype_typescript(%s);\n\
    \               \n\
     %a\n\
     throw \"unknown primitive in output_sumtype_decode\"" prefix argname
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n")
       (fun ppf (t_, path) ->
         match t_ with
         | Annot (u_, name) when is_unit u_ ->
           let name = show_sanitized_name @@ bind_sanitize_capitalize name in
           fprintf ppf
             "\n\
              if(%s.arrayEquals(p[0],[%a])){\n\
              return {kind : \"%s_constructor\"}}" prefix
             (pp_print_list ~pp_sep:(tag ",") (fun ppf b -> fprintf ppf "%b" b))
             path name
         | Annot (t_, name) ->
           let name = show_sanitized_name @@ bind_sanitize_capitalize name in
           fprintf ppf
             "\n\
              if(%s.arrayEquals(p[0],[%a])){\n\
              return {kind : \"%s_constructor\", %s_element : (%a(p[1]))}}"
             prefix
             (pp_print_list ~pp_sep:(tag ",") (fun ppf b -> fprintf ppf "%b" b))
             path name name
             (output_type_decode ~prefix)
             t_
         | t_ ->
           let name = constructor_from_path path in
           fprintf ppf
             "\n\
              if(%s.arrayEquals(p[0],[%a])){\n\
              return {kind : \"%s_constructor\",%s_element: (%a(p[1]))}}" prefix
             (pp_print_list ~pp_sep:(tag ",") (fun ppf b -> fprintf ppf "%b" b))
             path name name
             (output_type_decode ~prefix)
             t_))
    l

let declare_sumtype ~(* ~prefix *) typename ppf t =
  let lst = sumtype_to_list t in
  let rec aux res = function
    | [] -> res
    | (Annot (_t, name), _) :: xs ->
      let name = show_sanitized_name @@ bind_sanitize_capitalize name in
      let new_res =
        let cur_elem =
          Format.asprintf "%s_%s_constructor_subtype"
            (show_sanitized_name typename)
            name in
        cur_elem :: res in
      aux new_res xs
    | (_t, path) :: xs ->
      let name = constructor_from_path path in
      let new_res =
        let cur_elem =
          Format.asprintf "%s_%s_subtype" (show_sanitized_name typename) name
        in
        cur_elem :: res in
      aux new_res xs in
  fprintf ppf "\n%s\n" (String.concat "|\n" (aux [] lst))

let rec declare_type_ ~prefix ~initial (m : type_) ppf (name : sanitized_name) =
  output_verbose ~level:1
  @@ sprintf "[typescript][declare_type_] Entering with name %s"
       (show_sanitized_name name) ;
  (* TODO: remove ~initial argument for all functions *)
  let mk_init (name : sanitized_name) = name in
  let output_type_pretty = output_type_pretty ~prefix ~initial in
  let output_type_content = output_type_content ~initial in
  match m with
  | Or _ ->
    let name = bind_sanitize_uncapitalize name in
    let sumtype_content =
      Format.asprintf "%a" (declare_sumtype ~typename:name) m in
    let d =
      if not (is_one_field_record m) then
        Curly
      else
        Nothing in
    fprintf ppf "%aexport type %s = %s%s%s"
      (define_sumtype_subtypes ~prefix ~initial ~typename:name)
      m
      (show_sanitized_name @@ mk_init name)
      (opening d) sumtype_content (closing d)
  | Base String ->
    fprintf ppf "export type %s = string" (show_sanitized_name @@ mk_init name)
  | Base b ->
    fprintf ppf "export type %s = %a"
      (show_sanitized_name @@ mk_init name)
      (pp_base_type ~language:Factori_config.Typescript ~abstract:false
         ~prefix:(Some prefix))
      b
  | TVar (vname, _) ->
    fprintf ppf "export type %s = %s"
      (show_sanitized_name @@ mk_init name)
      (show_sanitized_name vname)
  | Binary (BigMap, (k, v)) ->
    fprintf ppf "export type %s = %s.big_map<%a,%a>"
      (show_sanitized_name @@ mk_init name)
      prefix output_type_pretty k output_type_pretty v
  | Binary (Lambda, (k, v)) ->
    fprintf ppf "export type %s = %s.lambda<%a,%a>"
      (show_sanitized_name @@ mk_init name)
      prefix output_type_pretty k output_type_pretty v
  | Binary (Map, (k, v)) ->
    fprintf ppf "export type %s = %s.map<%a,%a>"
      (show_sanitized_name @@ mk_init name)
      prefix output_type_pretty k output_type_pretty v
  | Unary (List, t) ->
    fprintf ppf "export type %s = %a[]"
      (show_sanitized_name @@ mk_init name)
      output_type_pretty t
  | Unary (Set, t) ->
    fprintf ppf "export type %s = %a[]"
      (show_sanitized_name @@ mk_init name)
      output_type_pretty t
  | Unary (Ticket, t) ->
    fprintf ppf "export type %s = %s.ticket<%a>"
      (show_sanitized_name @@ mk_init name)
      prefix output_type_pretty t
  | Unary (Option, t) ->
    fprintf ppf "export type %s = %s.option<%a>"
      (show_sanitized_name @@ mk_init name)
      prefix output_type_pretty t
  | Contract _t ->
    fprintf ppf "export type %s = %s.contract"
      (show_sanitized_name @@ mk_init name)
      prefix
    (* output_type_pretty t *)
  | Annot (ep, _) -> declare_type_ ~prefix ~initial ep ppf (mk_init name)
  | Pair _ ->
    fprintf ppf "export type %s = %a"
      (show_sanitized_name @@ mk_init name)
      (output_type_content ~prefix)
      m
  | Record _ ->
    let name = bind_sanitize_uncapitalize name in
    let record_content =
      Format.asprintf "%a"
        (fun ppf type_ -> output_type_content ~prefix ppf type_)
        m in
    let d =
      if not (is_one_field_record m) then
        Curly
      else
        Nothing in
    fprintf ppf "export type %s = %s%s%s"
      (show_sanitized_name @@ mk_init name)
      (opening d) record_content (closing d)

and generator_type_decl ~prefix (m : type_) ppf name =
  let name = bind_sanitize_uncapitalize name in
  let prelude =
    asprintf "export let %s_generator = function() : %s{ return "
      (show_sanitized_name name) (show_sanitized_name name) in
  let closing = sprintf "}" in
  match m with
  | Base Int ->
    fprintf ppf "%sBigInt(%s.big_int_generator ())%s" prelude prefix closing
  | Base b ->
    fprintf ppf "%s%a_generator ()%s" prelude
      (pp_base_type ~language:Factori_config.Typescript ~abstract:false
         ~prefix:(Some prefix))
      b closing
  | Annot (ep, _) -> fprintf ppf "%a" (generator_type_decl ~prefix ep) name
  | TVar tn ->
    fprintf ppf "\n%s%s_generator ()%s" prelude
      (show_sanitized_name (fst tn))
      closing
  | Unary (u, a) ->
    fprintf ppf "%s(%s.%s_generator (%a)) ()%s" prelude prefix (str_of_unary u)
      (output_type_generator ~prefix)
      a closing
  | Contract _c ->
    fprintf ppf "%s%s.contract_generator ()%s" prelude prefix closing
  | Binary (b, (k, v)) ->
    fprintf ppf "%s(%s.%s_generator (%a,%a)) ()%s" prelude prefix
      (str_of_binary b)
      (output_type_generator ~prefix)
      k
      (output_type_generator ~prefix)
      v closing
  | Or _ as t ->
    fprintf ppf "export function %s_generator () : %s {return %a}"
      (show_sanitized_name name) (show_sanitized_name name)
      (output_sumtype_generator ~prefix)
      t
  | Pair _ as t ->
    fprintf ppf "export function %s_generator() : %s {return %a()}"
      (show_sanitized_name name) (show_sanitized_name name)
      (output_type_generator ~prefix)
      t
  | Record r ->
    fprintf ppf "export function %s_generator() : %s {return (%a)}"
      (show_sanitized_name name) (show_sanitized_name name)
      (generator_record ~prefix) r

and encode_type_decl ~prefix (m : type_) ppf (name : sanitized_name) =
  let output_type_encode = output_type_encode ~prefix in
  let prelude =
    asprintf "export function %s_encode (arg : %s) : MichelsonV1Expression"
      (show_sanitized_name name) (show_sanitized_name name) in
  match m with
  | Base String ->
    (* this is a special case *)
    fprintf ppf "%s{return %s.string_encode(arg)}" prelude prefix
  | Base b ->
    fprintf ppf "%s{return %a_encode(arg)}" prelude
      (pp_base_type ~language:Factori_config.Typescript ~abstract:false
         ~prefix:(Some prefix))
      b
  | Annot (ep, _) -> fprintf ppf "%a" (encode_type_decl ~prefix ep) name
  | TVar tn ->
    fprintf ppf "\n%s{return %s_encode(arg)}" prelude
      (show_sanitized_name (fst tn))
  | Unary (List, a) ->
    fprintf ppf "%s{return %s.list_encode(%a)(arg)}" prelude prefix
      output_type_encode a
  | Unary (Set, a) ->
    fprintf ppf "%s{return %s.list_encode(%a)(arg)}" prelude prefix
      output_type_encode a
  | Unary (Ticket, t) ->
    fprintf ppf "%s{return %s.ticket_encode(%a)(arg)}" prelude prefix
      output_type_encode t
  | Contract c ->
    fprintf ppf "%s{return %s.contract_encode(new Parser().parseJSON(%a))(arg)}"
      prelude prefix (* output_type_encode *) pp_micheline_ c
  | Binary (BigMap, (k, v)) ->
    fprintf ppf "%s{return %s.big_map_encode(%a,%a)(arg)}" prelude prefix
      output_type_encode k output_type_encode v
  | Binary (Lambda, (k, v)) ->
    fprintf ppf "%s{return %s.lambda_encode(%a,%a)(arg)}" prelude prefix
      output_type_encode k output_type_encode v
  | Binary (Map, (k, v)) ->
    fprintf ppf "%s{return %s.map_encode(%a,%a)(arg)}" prelude prefix
      output_type_encode k output_type_encode v
  | Unary (Option, a) ->
    fprintf ppf "%s{return functolib.option_encode(%a)(arg)}" prelude
      output_type_encode a
  | Or _ as t ->
    fprintf ppf
      "export function %s_encode (arg : %s) : MichelsonV1Expression {\n%a}"
      (show_sanitized_name name) (show_sanitized_name name)
      (output_sumtype_encode ~prefix "arg")
      t
  | Pair _ as t ->
    fprintf ppf "let %s_encode = %a\n export {%s_encode}"
      (show_sanitized_name name) output_type_encode t (show_sanitized_name name)
  | Record r ->
    fprintf ppf
      "export function %s_encode(arg : %s) : MichelsonV1Expression {\n\
       return %a}" (show_sanitized_name name) (show_sanitized_name name)
      (encode_record ~prefix) r

(* let name = (String.uncapitalize_ascii name) in
 * let record_encode = Format.asprintf "%a" (fun ppf type_ -> encode_record name ~prefix ppf type_) m in
 * fprintf ppf "export function %s_encode (arg : %s) : MichelsonV1Expression {\nreturn(%s)}"
 *   name name record_encode *)

let rec iterate_over_fields ppf (lst : (sanitized_name * _) list)
    (f : int -> sanitized_name -> 'a -> string) (d : delimiter) (sep : string) =
  let rec aux res count = function
    | [] -> List.rev res
    | (k, v) :: xs ->
      let new_res =
        let cur_elem = f count k v in
        cur_elem :: res in
      aux new_res (count + 1) xs in
  fprintf ppf "%s%s%s\n" (opening d)
    (String.concat sep (aux [] 0 lst))
    (closing d)

and record_extract_decoded_values ?(is_one_field_record = false) m ppf
    record_name =
  let sep =
    if is_one_field_record then
      Nothing
    else
      Curly in
  let f _ (k : sanitized_name) (path, _) =
    if is_one_field_record then
      Format.asprintf "(%s%a)" record_name
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf "")
           (fun ppf x -> fprintf ppf "[%d]" x))
        path
    else
      Format.asprintf "%s : (%s%a)" (show_sanitized_name k) record_name
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf "")
           (fun ppf x -> fprintf ppf "[%d]" x))
        path in
  fprintf ppf "return %a" (fun ppf -> iterate_over_fields ppf m f sep) ",\n"

and decode_type_decl ~prefix (m : type_) ppf (name : sanitized_name) =
  let prelude =
    asprintf "export function %s_decode(arg : MichelsonV1Expression) : %s"
      (show_sanitized_name name) (show_sanitized_name name) in
  let output_type_decode = output_type_decode ~prefix in
  match m with
  | Or _ as t ->
    fprintf ppf
      "export function %s_decode(arg : MichelsonV1Expression) : %s {\n%a}"
      (show_sanitized_name name) (show_sanitized_name name)
      (output_sumtype_decode ~prefix "arg")
      t (* fprintf ppf "%s%a" prelude output_type_decode t *)
  | Annot (ep, _) -> fprintf ppf "%a" (decode_type_decl ~prefix ep) name
  | TVar tn ->
    fprintf ppf "\n%s {return %s_decode(arg)}" prelude
      (show_sanitized_name (fst tn))
  | Unary (List, a) ->
    fprintf ppf "%s{return (%s.list_decode(%a))(arg)}" prelude prefix
      output_type_decode a
  | Unary (Set, a) ->
    fprintf ppf "%s{return (%s.list_decode(%a))(arg)}" prelude prefix
      output_type_decode a
  | Unary (Ticket, t) ->
    fprintf ppf "%s{return (%s.ticket_decode(%a))(arg)}" prelude prefix
      output_type_decode t
  | Contract c ->
    fprintf ppf
      "%s{return (%s.contract_decode(new Parser().parseJSON(%a))(arg))}" prelude
      prefix pp_micheline_ c
  | Binary (BigMap, (k, v)) ->
    fprintf ppf "%s{ return (%s.big_map_decode(%a,%a)(arg))}" prelude prefix
      output_type_decode k output_type_decode v
  | Binary (Lambda, (k, v)) ->
    fprintf ppf "%s{ return (%s.lambda_decode(%a,%a)(arg))}" prelude prefix
      output_type_decode k output_type_decode v
  | Binary (Map, (k, v)) ->
    fprintf ppf "%s{return ((%s.map_decode(%a,%a)(arg)))}" prelude prefix
      output_type_decode k output_type_decode v
  | Unary (Option, a) ->
    fprintf ppf "%s{return ((%s.option_decode(%a)(arg)))}" prelude prefix
      output_type_decode a
  | Base String ->
    (* special case *)
    fprintf ppf
      "export function %s_decode(m : MichelsonV1Expression) : %s { return \
       (%s.string_decode(m));}"
      (show_sanitized_name name) (show_sanitized_name name) prefix
  | Base b ->
    fprintf ppf
      "export function %s_decode(m : MichelsonV1Expression) : %s { return \
       (%a_decode(m));}"
      (show_sanitized_name name) (show_sanitized_name name)
      (pp_base_type ~language:Factori_config.Typescript ~abstract:false
         ~prefix:(Some prefix))
      b
  | Pair _ as t ->
    fprintf ppf "export let %s_decode = %a" (show_sanitized_name name)
      output_type_decode t
  | Record r ->
    let name = bind_sanitize_uncapitalize name in
    let is_one_field_record = is_one_field_record m in
    let record_decode =
      Format.asprintf "%a" (fun ppf type_ -> decode_record ~prefix ppf type_) r
    in
    let lst = record_to_list r in
    fprintf ppf
      "export function %s_decode(arg : MichelsonV1Expression) : %s {\n\
       let before_projection = %s(arg);\n\
       %a\n\
       %!}"
      (show_sanitized_name name) (show_sanitized_name name) record_decode
      (record_extract_decoded_values ~is_one_field_record lst)
      "before_projection" (* (opening d) record_content (closing d) *)

and micheline_type_decl ~prefix (m : type_) ppf name =
  let output_type_micheline = output_type_micheline ~prefix in
  let name = bind_sanitize_uncapitalize name in
  let prelude =
    asprintf "export let %s_micheline = " (show_sanitized_name name) in
  match m with
  | Base b ->
    fprintf ppf "%s%s.%a_micheline" prelude prefix
      (pp_base_type ~language:Factori_config.Typescript ~abstract:false
         ~prefix:None)
      b
  | Annot (ep, _) -> fprintf ppf "%a" (micheline_type_decl ~prefix ep) name
  | TVar tn ->
    fprintf ppf "\n%s%s_micheline" prelude (show_sanitized_name (fst tn))
  | Unary (List, a) ->
    fprintf ppf "%s(%s.list_micheline (%a))" prelude prefix
      output_type_micheline a
  | Unary (Set, a) ->
    fprintf ppf "%s(%s.set_micheline (%a))" prelude prefix output_type_micheline
      a
  | Unary (Ticket, t) ->
    fprintf ppf "%s(%s.ticket_micheline (%a))" prelude prefix
      output_type_micheline t
  | Contract c ->
    fprintf ppf "%s(%s.contract_micheline (new Parser().parseJSON(%a)))" prelude
      prefix pp_micheline_ c
  | Binary (BigMap, (k, v)) ->
    fprintf ppf "%s(%s.big_map_micheline (%a,%a))" prelude prefix
      output_type_micheline k output_type_micheline v
  | Binary (Lambda, (x, y)) ->
    fprintf ppf "%s(%s.lambda_micheline (%a,%a))" prelude prefix
      output_type_micheline x output_type_micheline y
  | Binary (Map, (k, v)) ->
    fprintf ppf "%s(%s.map_micheline (%a,%a))" prelude prefix
      output_type_micheline k output_type_micheline v
  | Unary (Option, a) ->
    fprintf ppf "%s(%s.option_micheline (%a))" prelude prefix
      output_type_micheline a
  | Or (_ as t) ->
    fprintf ppf "let %s_micheline = %a" (show_sanitized_name name)
      (output_sumtype_micheline ~prefix)
      t
  | Pair _ as t ->
    fprintf ppf "let %s_micheline = %a" (show_sanitized_name name)
      output_type_micheline t
  | Record r ->
    fprintf ppf "let %s_micheline = %a" (show_sanitized_name name)
      (micheline_record ~prefix) r

let assert_failwith ~prefix ppf name =
  fprintf ppf
    {|export async function assert_failwith_str_%s(tk: TezosToolkit,
  %s_kt1: string,
  param: %s, expected : string, prefix : string, msg : string, amount?: number): Promise<%s.operation_result> {
  let param1 = %s_encode(param);
  //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
  var res = await %s.send(tk, %s_kt1, '%s', param1, amount);
  //console.log(JSONbig.stringify(res))
  if(res.hash == null){
    //console.log("hash is null!")
    res.error.errors.forEach(element => {
      //console.log(`element is ${JSONbig.stringify(element)}`)
      if (element.with != null){
        var actual_fail = element.with.string
        if (actual_fail == expected){
          console.log(`${prefix}[assert_failwith] got ${expected} as expected`.padEnd(80) + "Test Success!")
          return res
        }
        else{
          console.log(`${prefix}[assert_failwith]${msg} got ${JSONbig.stringify(actual_fail)} instead of ${expected}`.padEnd(80) + "Test Failed!")
          return res
        }
      }
      //console.log(`${prefix}element ${element} is null!`)
    });
  }
  return res
     }|}
    name name name prefix name prefix name name

let call_entrypoint ~prefix ppf name =
  (* let name = (String.uncapitalize_ascii name) in *)
  fprintf ppf
    "export async function call_%s(tk: TezosToolkit,\n\
    \  %s_kt1: string,\n\
    \  param: %s, amount?: number): Promise<%s.operation_result> {\n\
    \  let res = %s_encode(param);\n\
    \  //console.log(`res: ${JSONbig.stringify(res,null,2)}`);\n\
    \  return %s.send(tk, %s_kt1, '%s', res, amount);\n\
    \     }" name name name prefix name prefix name name

let rec pp_interface_instruction_typescript ~storage_name ~prefix ~contract_name
    ppf ii =
  match ii with
  | Define_type (name, type_) ->
    output_verbose ~level:1
    @@ sprintf
         "[pp_interface_instruction_typescript] Going through type definition \
          %s"
         (show_sanitized_name name) ;
    (* let name = bind_filter name in *)
    fprintf ppf "%a\n%a\n%a\n%a\n%a"
      (declare_type_ ~prefix ~initial:false type_)
      name
      (encode_type_decl ~prefix type_)
      name
      (decode_type_decl ~prefix type_)
      name
      (micheline_type_decl ~prefix type_)
      name
      (generator_type_decl ~prefix type_)
      name
  | Deploy ->
    (* deploy ppf () *)
    let sanitized_contract_name = show_sanitized_name contract_name in
    let print_deploy_raw ppf () =
      fprintf ppf
        {|
async function deploy_%s_raw(
  tezosKit: TezosToolkit,
  storage : MichelsonV1Expression,
  config : any,
  prefix = "",
  debug = false): Promise<string> {
  //console.log("[deploy_%s_raw] Deploying new %s smart contract");
  try {
        if(debug){console.log(`%s initial storage ${JSON.stringify(storage)}`)}
        const client = new RpcClient(config.node_addr);
        var b = await client.getBlock();
        let origination_op = await tezosKit.contract
        .originate({
            code: %s,
            init: storage
        })
        //console.log(`Waiting for confirmation of origination for ${origination_op.contractAddress}...`);
        var contract = await origination_op.contract();
        //console.log(`Origination completed.`);
        return contract.address

  } catch (error) {
    console.log(`${prefix}ERROR in deploy %s: ${JSON.stringify(error)}`)
    throw error
  }
}
|}
        sanitized_contract_name sanitized_contract_name sanitized_contract_name
        sanitized_contract_name
        (Factori_config.get_typescript_code_basename ~contract_name)
        (show_sanitized_name storage_name) in
    let print_deploy ppf () =
      fprintf ppf
        {|
export async function deploy_%s(
  tezosKit: TezosToolkit,
  storage : %s,
  config: any,
  prefix="",
  debug = false): Promise<string> {
  let kt1_address = await deploy_%s_raw(tezosKit, %s_encode(storage),config, prefix,debug);
  return kt1_address;
}

export type Storage_type = %s
export let Storage_type_encode = %s_encode
export let Storage_type_decode = %s_decode

|}
        sanitized_contract_name
        (show_sanitized_name storage_name)
        sanitized_contract_name
        (show_sanitized_name storage_name)
        (show_sanitized_name storage_name)
        (show_sanitized_name storage_name)
        (show_sanitized_name storage_name) in

    fprintf ppf {|
%a
%a
|} print_deploy_raw () print_deploy ()
  | Seq ii ->
    (pp_interface_typescript ~storage_name ~prefix ~contract_name) ppf ii
  | Entrypoint (name, _) ->
    fprintf ppf "%a\n%a" (call_entrypoint ~prefix) (filter name)
      (assert_failwith ~prefix) (filter name)

and pp_interface_typescript ~storage_name ~prefix ~contract_name
    (ppf : formatter) (i : interface) =
  pp_print_list
    ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
    (pp_interface_instruction_typescript ~storage_name ~prefix ~contract_name)
    ppf i

let prelude prefix (contract_name : sanitized_name) =
  Format.sprintf
    {|
import {
  TezosToolkit,
  BigMapAbstraction,
  MichelsonMap,
  OriginationOperation,
  OpKind,
  createTransferOperation,
  TransferParams,
  RPCOperation,
  createRevealOperation
} from "@taquito/taquito"
import { MichelsonV1Expression } from "@taquito/rpc"
import { RpcClient } from '@taquito/rpc';
import { encodeOpHash } from '@taquito/utils';
import { Parser } from '@taquito/michel-codec';
import { stringify } from "querystring"
import {JSONbig } from "./%s"
import * as %s from "./%s";

let %s = require('./%s')

|}
    prefix prefix prefix
    (Factori_config.get_typescript_code_basename ~contract_name)
    (Factori_config.get_typescript_code_filename ~contract_name)

let print_original_blockchain_storage ~storage_name ~final_storage_type_ ~kt1
    ~network ppf () =
  let open Tzfunc.Rp in
  let open Tzfunc.Proto in
  match
    Lwt_main.run
      (let>? storage =
         Factori_config.get_storage ~network
           ~debug:(!Factori_options.verbosity > 0)
           kt1 in
       try
         let value =
           value_of_typed_micheline
             (* ~debug:(!Factori_options.verbosity > 0) *)
             storage final_storage_type_ in
         fprintf ppf
           "\n\
            const p = new Parser();\n\
            export let initial_blockchain_storage : %s = %a\n\
            /*\n\
           \ %s\n\
            */\n"
           (show_sanitized_name storage_name)
           pp_value_to_typescript value
           (EzEncoding.construct micheline_enc.json storage) ;
         Lwt.return_ok ()
       with e ->
         Factori_errors.handle_exception e ;
         Lwt.return_ok ())
  with
  | Ok () -> ()
  | Error e -> print_error e

let process_entrypoints ~prefix ~contract_name ~storage_name
    ~final_storage_type_ ~network ?(kt1 = None) ppf interface =
  let interface =
    try_overflow ~msg:"extract_interface typescript" (fun () -> interface) in
  let _ = check_interface_for_doubles interface in
  let original_blockchain_storage =
    match kt1 with
    | None -> ""
    | Some kt1 ->
      Format.asprintf "\n%a"
        (print_original_blockchain_storage ~storage_name ~final_storage_type_
           ~kt1 ~network)
        () in
  fprintf ppf "%s\n%a%s"
    (prelude prefix contract_name)
    (pp_interface_typescript ~storage_name ~prefix ~contract_name)
    interface original_blockchain_storage
