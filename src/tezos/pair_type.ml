open Format
open Factori_utils

type 'a paired_type =
  | LeafP of ((sanitized_name * 'a)[@wrap "LeafP"])
  | PairP of ('a paired_type list[@wrap "PairP"])
[@@deriving show, encoding { recursive }]

let pairp l = PairP l

let leafp (n, v) = LeafP (n, v)

let rec eq_paired_type ~eq p1 p2 =
  match (p1, p2) with
  | LeafP (s1, x1), LeafP (s2, x2) -> s1 = s2 && eq x1 x2
  | PairP l1, PairP l2 -> List.for_all2 (eq_paired_type ~eq) l1 l2
  | _, _ -> false

let map_paired_type ~f x =
  let rec aux = function
    | LeafP (s, x) -> f s x
    | PairP l -> PairP (List.map aux l) in
  aux x

let fold_paired_type ~(f : sanitized_name -> 'a -> 'b -> 'b) (init : 'b)
    (x : 'a paired_type) =
  let rec aux res = function
    | LeafP (s, x) -> f s x res
    | PairP l -> List.fold_right (fun x res -> aux res x) l res in
  aux init x

let fold_and_replace_paired_type
    ~(f : sanitized_name -> 'a -> 'b -> 'a paired_type * 'b) ~(init : 'b)
    (x : 'a paired_type) =
  let rec aux res = function
    | LeafP (s, x) -> f s x res
    | PairP (l : 'a paired_type list) ->
      let l, res =
        List.fold_right
          (fun (x : 'a paired_type) (l, (res : 'b)) ->
            let x1, resx = aux res x in
            (x1 :: l, resx))
          l ([], res) in
      (PairP l, res) in
  aux init x

let fold_and_replace_paired_type_combine ~f ~combine x =
  let rec aux = function
    | LeafP (s, x) -> f s x
    | PairP (l : 'a paired_type list) ->
      let l, res =
        List.fold_right
          (fun x (l, res) ->
            let x1, resx = aux x in
            (x1 :: l, combine resx res))
          l ([], []) in
      (PairP l, res) in
  aux x

(* Primarily for encoding records; mich_type says whether what we
   are printing is a type or a value (default is value) *)
let pp_pairedtype_ocaml_build ?(mich_type = false) f ppf x =
  let rec aux ppf = function
    | LeafP (n, l) -> fprintf ppf "%a" (f n) l
    | PairP l ->
      fprintf ppf "Mprim {prim = `%s;\nargs = [%a];\nannots=[]}\n"
        (if mich_type then
          "pair"
        else
          "Pair")
        (pp_print_list ~pp_sep:(tag ";") aux)
        l in
  aux ppf x

let pp_pairedtype_typescript_build f ppf x =
  let rec aux ppf = function
    | LeafP (n, l) -> fprintf ppf "%a" (f n) l
    | PairP l ->
      fprintf ppf "{prim : 'Pair',args: [%a]}"
        (pp_print_list ~pp_sep:(tag ",") aux)
        l in
  aux ppf x

(* For generic printing operations on records (abiding by the on-chain
   structure) *)
let pp_pairedtype_ocaml ~f_leaf ~f_pair ~sep ?(delimiters = Nothing) ppf x =
  let rec aux ppf = function
    | LeafP (n, l) -> fprintf ppf "%a" (f_leaf n) l
    | PairP l ->
      let n = List.length l in
      fprintf ppf "%s%a%a%s" (opening delimiters) f_pair n
        (pp_print_list ~pp_sep:(tag sep) aux)
        l (closing delimiters) in
  aux ppf x

let pp_pairedtype_typescript ~f_leaf ~f_pair ~sep ?(pairparens = false)
    ?(delimiters = Nothing) ppf x =
  let rec aux ppf = function
    | LeafP (n, l) -> fprintf ppf "%a" (f_leaf n) l
    | PairP l ->
      let n = List.length l in
      fprintf ppf "%s%a%s%a%s%s" (opening delimiters) f_pair n
        (if pairparens then
          opening Parens
        else
          "")
        (pp_print_list ~pp_sep:(tag sep) aux)
        l
        (if pairparens then
          closing Parens
        else
          "")
        (closing delimiters) in
  aux ppf x

let rec flattenP res = function
  | [] -> res
  | PairP bl :: xs ->
    let res1 = flattenP res bl in
    flattenP res1 xs
  | LeafP (n, x) :: xs -> flattenP ((n, x) :: res) xs
