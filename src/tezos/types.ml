open Tzfunc.Proto
open Format
open Pair_type
open Or_type
open Factori_utils

type unary =
  | List
  | Set
  | Ticket
  | Option
[@@deriving show, encoding]

let str_of_unary = function
  | List -> "list"
  | Set -> "set"
  | Ticket -> "ticket"
  | Option -> "option"

type binary =
  | Map
  | BigMap
  | Lambda
[@@deriving show, encoding]

let str_of_binary = function
  | Map -> "map"
  | BigMap -> "big_map"
  | Lambda -> "lambda"

(** A reification of Michelson types for our inference purposes *)
type basetype =
  | Never
  | String
  | Nat
  | Int
  | Byte
  | Address
  | Signature
  | Unit
  | Bool
  | Timestamp
  | Keyhash
  | Key
  | Mutez
  | Operation
  | Chain_id
  | Sapling_state
  | Sapling_transaction_deprecated
[@@deriving show, encoding { enum }]

let str_of_base = function
  | Never -> "never"
  | String -> "string"
  | Nat -> "nat"
  | Int -> "int"
  | Byte -> "byte"
  | Address -> "address"
  | Signature -> "signature"
  | Unit -> "unit"
  | Bool -> "bool"
  | Timestamp -> "timestamp"
  | Keyhash -> "key_hash"
  | Key -> "key"
  | Mutez -> "mutez"
  | Operation -> "operation"
  | Chain_id -> "chain_id"
  | Sapling_state -> "sapling_state"
  | Sapling_transaction_deprecated -> "sapling_transaction_deprecated"

(** An abstract type to represent types (only those with content and
   which mirror on-chain types) with their arities so as to generate
   generic functions on those types in various languages. *)
type abs_type_ =
  | ATuple of int
  | AContract
  | ABase of basetype
  | AUnary of unary
  | ABinary of binary

let arity = function
  | ATuple k -> k
  | AContract -> 0
  | ABase _ -> 0
  | AUnary _ -> 1
  | ABinary _ -> 2

type typename = string [@@deriving show, encoding]

type micheline_ = micheline

let micheline__enc = micheline_enc.json

let pp_micheline_ ppf m =
  fprintf ppf "%s" (EzEncoding.construct micheline__enc m)

type type_ =
  | Pair of (type_ list[@wrap "pair"])
  | Record of (type_ paired_type[@wrap "record"])
  | TVar of ((sanitized_name * type_)[@wrap "tvar"])
  | Annot of ((type_ * sanitized_name)[@wrap "annot"])
  | Or of (type_ or_type[@wrap "or"])
  | Contract of (micheline_[@wrap "contract"])
  | Base of (basetype[@wrap "base"])
  | Unary of ((unary * type_)[@wrap "unary"])
  | Binary of ((binary * (type_ * type_))[@wrap "binary"])
[@@deriving show, encoding { recursive }]

let rec is_unit t_ =
  match t_ with
  | Base Unit -> true
  | TVar (_, t_) -> is_unit t_
  | _ -> false
