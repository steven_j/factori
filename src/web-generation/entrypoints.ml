open Factori_file
open Factori_utils
open Types
open Common

let pp_input fmt (acc, i) =
  match i with
  | Never -> File.write fmt "Never say never@."
  | String ->
    File.write fmt "<a-input v-model:value='%a' placeholder='abc...' />@."
      Accessors.pp_ts acc
  | Nat ->
    File.write fmt
      "<a-input type='number' v-model:value='%a' min='0' placeholder='Positive \
       number: %d' />@."
      Accessors.pp_ts acc (Random.int 100000)
  | Int ->
    File.write fmt
      "<a-input type='number' v-model:value='%a' placeholder='Integer number: \
       %d' />@."
      Accessors.pp_ts acc
      (Random.int 100000 - 50000)
  | Byte ->
    File.write fmt
      "<a-input type='text' v-model:value='%a' placeholder='Bytes: \
       f456afe90fb6...' />@."
      Accessors.pp_ts acc
  | Address ->
    File.write fmt
      "<a-input type='text' v-model:value='%a' placeholder='Address: \
       tz1xxx...' />@."
      Accessors.pp_ts acc
  | Signature ->
    File.write fmt
      "<a-input type='text' v-model:value='%a' placeholder='Signature' />@."
      Accessors.pp_ts acc
  | Unit ->
    File.write fmt "<a-input type='text' placeholder='Unit: ()' disabled/>@."
  | Bool ->
    File.write fmt "<a-switch v-model:checked='%a' /> {{ %a }}@."
      Accessors.pp_ts acc Accessors.pp_ts acc
  | Timestamp ->
    File.write fmt "<a-input type='time' v-model:value='%a' step='1' />@."
      Accessors.pp_ts acc
  | Keyhash ->
    File.write fmt
      "<a-input type='text' v-model:value='%a' placeholder='KeyHash: edpk...' \
       />@."
      Accessors.pp_ts acc
  | Key ->
    File.write fmt
      "<a-input type='text' v-model:value='%a' placeholder='Key: edsk...' />@."
      Accessors.pp_ts acc
  | Mutez ->
    File.write fmt
      "<a-input type='number' min='0' v-model:value='%a' placeholder='Amount \
       in &mu;&#42793;'/>@."
      Accessors.pp_ts acc
  | Operation -> Format.printf "Operation is not Passable or Storable@."
  | Chain_id ->
    File.write fmt
      "<a-input type='text' v-model:value='%a' placeholder='Chaind ID: %d'/>@."
      Accessors.pp_ts acc (Random.int 1000000)
  | Sapling_state -> File.write fmt "TODO: INPUT Sapling_state@."
  | Sapling_transaction_deprecated ->
    File.write fmt "TODO: INPUT Sapling_transaction_deprecated@."

let rec pp_paired fmt (sp, acc, p) =
  let open Pair_type in
  match p with
  | LeafP (name, t) ->
    File.write fmt "%a@." pp_type
      (sp, Some name, Accessors.Field (show_sanitized_name name) :: acc, t)
  | PairP l -> List.iter (fun p -> pp_paired fmt (sp, acc, p)) l

and pp_or fmt (sp, acc, ort) =
  let l = get_all_constructor ort in
  File.write fmt "<a-form-item>@." ;
  File.write fmt
    "<a-select v-model:value='%a' placeholder='Select a value from the list \
     below' style='width: 100%%' >@."
    Accessors.pp_ts
    (Accessors.Field "kind" :: acc) ;
  List.iter
    (fun (s, _) ->
      File.write fmt
        "<a-select-option value='%s_constructor'>%s</a-select-option>@." s s)
    l ;
  File.write fmt "</a-select>@." ;
  File.write fmt "</a-form-item>@." ;
  File.write fmt "<template v-if='%a == \"\"'>@." Accessors.pp_ts
    (Accessors.Field "kind" :: acc) ;
  File.write fmt "<a-empty />@." ;
  File.write fmt "</template>@." ;
  List.iter
    (fun (s, t) ->
      File.write fmt "<template v-else-if='%a == \"%s\"'>@." Accessors.pp_ts
        (Accessors.Field "kind" :: acc)
        (s ^ "_constructor") ;
      File.write fmt "%a@." pp_type
        (sp, None, Accessors.Field (s ^ "_element") :: acc, t) ;
      File.write fmt "</template>@.")
    l

and pp_type fmt (sp, name, acc, t) =
  let alert_message t s =
    File.write fmt "<a-alert type='%s' message='%s' closable show-icon />@." t s
  in
  match t with
  | Pair l ->
    List.iteri
      (fun i t -> pp_type fmt (sp + 1, None, Accessors.Index i :: acc, t))
      l
  | Record p ->
    let n =
      match name with
      | None -> ""
      | Some n -> show_sanitized_name n in
    File.write fmt "<a-form-item>@." ;
    File.write fmt "<a-card title='%s object'>@." n ;
    File.write fmt "<a-list>@." ;
    File.write fmt "%a" pp_paired (sp + 4, acc, p) ;
    File.write fmt "</a-list>" ;
    File.write fmt "</a-card>@." ;
    File.write fmt "</a-form-item>@."
  | TVar (_, t) -> File.write fmt "%a" pp_type (sp + 2, name, acc, t)
  | Annot (t, _) -> File.write ~sp fmt "%a@." pp_type (sp, name, acc, t)
  | Or ort -> File.write fmt "%a" pp_or (sp, acc, ort)
  | Base b ->
    File.write fmt "<a-form-item>@." ;
    File.write fmt "<a-row>@." ;
    File.write fmt "<a-col :span='5'>@." ;
    File.write fmt "%a@." print_base b ;
    if name <> None then
      File.write fmt ~sp:(sp + 2) "%a@." print_arg
        (Option.map show_sanitized_name name) ;
    File.write fmt "</a-col>@." ;
    File.write fmt ~sp:(sp + 2) "<a-col :span='19'>%a</a-col>@." pp_input
      (acc, b) ;
    File.write fmt "</a-row>@." ;
    File.write fmt "</a-form-item>@."
  | Unary (List, l) ->
    File.write fmt "<a-form-item>@." ;
    File.write fmt "<a-row>@." ;
    File.write fmt "<a-col :span='5'>@." ;
    File.write fmt
      "<a-typography-text type='warning' code>list</a-typography-text>" ;
    begin
      match name with
      | None -> ()
      | Some n ->
        File.write fmt "<a-typography-text strong>%s</a-typography-text>@."
          (show_sanitized_name n)
    end ;
    File.write fmt "</a-col>@." ;
    File.write fmt "<a-col :span='19'>@." ;
    File.write fmt "%a@." pp_type (sp, None, Accessors.Index 0 :: acc, l) ;
    File.write fmt "</a-col>@." ;
    File.write fmt "</a-row>@." ;
    File.write fmt "</a-form-item>@." ;
    alert_message "warning"
      "You can only send one element (should be fixed soon)."
  | Binary (BigMap, _) -> alert_message "error" "BigMap not supported yet."
  | Binary (Map, _) -> alert_message "error" "Map not supported yet"
  | Unary (Set, _) -> alert_message "error" "Set not supported yet"
  | Unary (Ticket, _) -> alert_message "error" "Ticket not supported yet."
  | Binary (Lambda, _) -> alert_message "error" "Lambda not supported yet."
  | Unary (Option, t) ->
    File.write fmt "%a@." pp_type (sp, None, acc, t) ;
    alert_message "warning" "Option are not fully supported yet."
  | Contract _ -> alert_message "error" "Contract not supported yet."

let pp_form fmt (sp, name, t) =
  File.write fmt "%a@." pp_type (sp, None, [Accessors.Field ("ep." ^ name)], t)

let rec pp_entrypoints file ?(sp = 0) entrypoints =
  match entrypoints with
  | [] -> ()
  | (name, t) :: l' ->
    let previous_name = show_sanitized_name name in
    let n_aux =
      if String.length previous_name > 1 && String.get previous_name 0 = '_'
      then
        String.sub previous_name 1 (String.length previous_name - 1)
      else
        previous_name in
    let n = { name with sanitized = n_aux } in
    let san_n = show_sanitized_name n in
    File.write file "<a-collapse-panel header='Entrypoint: %s'>@." san_n ;
    File.write file "<a-form>@." ;
    File.write file "%a" pp_form (sp + 3, "param_" ^ previous_name, t) ;
    File.write file "<a-form-item>@." ;
    File.write file "<a-row>@." ;
    File.write file "<a-col :span='12'>@." ;
    File.write file ~sp:(sp + 6)
      "<a-input type='number' min='0' v-model:value='senders.amount_%s' \
       placeholder='Default: 0'>@."
      previous_name ;
    File.write file "<template #prefix>@." ;
    File.write file
      "<a-typography-text type='warning'>Amount</a-typography-text>@." ;
    File.write file "</template>@." ;
    File.write file "<template #suffix>@." ;
    File.write file "&#42793;" ;
    File.write file "</template>@." ;
    File.write file "</a-input>@." ;
    File.write file "</a-col>@." ;
    File.write file "<a-col :span='12'>@." ;
    File.write file ~sp:(sp + 6)
      "<a-button type='primary' @click='%sCall' block \
       :disabled='ep.disabled'>Call</a-button>@."
      previous_name ;
    File.write file "</a-col>@." ;
    File.write file "</a-row>@." ;
    File.write file "</a-form-item>" ;
    File.write file "</a-form>@." ;
    File.write file ~sp:(sp + 1) "</a-collapse-panel>@." ;
    pp_entrypoints file ~sp l'

let pp file ?(sp = 0) entrypoints =
  File.write file ~sp "<a-collapse>@." ;
  pp_entrypoints file ~sp:(sp + 1) entrypoints ;
  File.write file ~sp "</a-collapse>@."
