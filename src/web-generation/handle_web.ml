open Factori_file
open Factori_utils
open Factori_config
open Types
open Common

let index_ts dir contracts =
  let file_name = get_typescript_index_ts_path ~dir () in
  let file = File.create_file file_name in
  File.write file "import { createApp } from 'vue'@." ;
  File.write file "import * as VueRouter from 'vue-router'@." ;
  File.write file "import Antd from 'ant-design-vue';@." ;
  File.write file "import 'ant-design-vue/dist/antd.dark.css';@." ;
  File.write file "import App from './App.vue'@." ;
  File.write file "import ContractsList from './components/ContractsList.vue'@." ;
  File.write file "import Home from './components/Home.vue'@." ;
  File.write file "import Settings from './components/Settings.vue'@." ;
  File.write file "import FactoriWallet from './components/FactoriWallet.vue'@." ;
  List.iter
    (fun c ->
      File.write file "import %s from './components/%s.vue'@." c.contract_name
        c.contract_name)
    contracts ;
  File.write file "const router = VueRouter.createRouter({@." ;
  File.write file ~sp:1 "history: VueRouter.createWebHistory(),@." ;
  File.write file ~sp:1 "routes: [@." ;
  File.write file ~sp:2
    "{ path: '/', name: 'SmartContrats', component: ContractsList },@." ;
  File.write file ~sp:2
    "{ path: '/wallet', name: 'FactoriWallet', component: FactoriWallet },@." ;
  List.iter
    (fun c ->
      File.write file ~sp:2 "{ path: '/%s', name: '%s', component: %s },@."
        c.contract_name c.contract_name c.contract_name)
    contracts ;
  File.write file ~sp:1 "]@." ;
  File.write file "})@." ;
  File.write file "createApp(App).use(Antd).use(router).mount('#app')@."

let print_get_storage file network =
  File.write file ~sp:2 "get_storage: async function(){@." ;
  File.write file ~sp:3
    "this.factori.tezos = new TezosToolkit(functolib.%s_config.node_addr)@."
    network ;
  File.write file ~sp:3
    "const contract = await this.factori.tezos.contract.at(this.kt1)@." ;
  File.write file ~sp:3 "return contract.script.storage@." ;
  File.write file ~sp:2 "},@."

let construct_param_base fmt b =
  match b with
  | Never -> File.write fmt "'TODO Never'"
  | String -> File.write fmt "''"
  | Nat -> File.write fmt "0"
  | Int -> File.write fmt "0"
  | Byte -> File.write fmt "''"
  | Address -> File.write fmt "''"
  | Signature -> File.write fmt "''"
  | Unit -> File.write fmt "null"
  | Bool -> File.write fmt "true"
  | Timestamp -> File.write fmt "''"
  | Keyhash -> File.write fmt "''"
  | Key -> File.write fmt "''"
  | Mutez -> File.write fmt "0"
  | Operation -> Format.printf "''"
  | Chain_id -> File.write fmt "''"
  | Sapling_state -> File.write fmt "'TODO Sapling_state'"
  | Sapling_transaction_deprecated ->
    File.write fmt "'TODO Sapling_transaction_deprecated'"

let rec construct_param_record fmt r =
  let open Pair_type in
  match r with
  | LeafP (name, t) ->
    File.write fmt "%s : %a" (show_sanitized_name name) construct_param t
  | PairP l ->
    Format.pp_print_list ~pp_sep:(Factori_utils.tag ",") construct_param_record
      fmt l

and construct_param_or fmt ort =
  let l = get_all_constructor ort in
  File.write fmt "{ kind: '', " ;
  List.iter
    (fun (s, t) -> File.write fmt "%s_element: %a," s construct_param t)
    l ;
  File.write fmt "}"

and construct_param fmt t =
  match t with
  | Base b -> File.write fmt "%a" construct_param_base b
  | Pair l ->
    File.write fmt "[%a]"
      (Format.pp_print_list ~pp_sep:(Factori_utils.tag ",") construct_param)
      l
  | Record r -> File.write fmt "{ %a }" construct_param_record r
  | TVar (_, t) -> File.write fmt "%a" construct_param t
  | Annot (t, _) -> File.write fmt "%a" construct_param t
  | Or ort -> File.write fmt "%a@." construct_param_or ort
  | Unary (List, t) -> File.write fmt "[%a]" construct_param t
  | Binary (BigMap, _) -> File.write fmt "0"
  | Binary (Map, _) -> File.write fmt "[]"
  | Unary (Set, _) -> File.write fmt "[]"
  | Unary (Ticket, _) -> File.write fmt "{ kind: 'ticket', value:0 }"
  | Binary (Lambda, _) -> File.write fmt "{ prim: 'LAMBDA', args: [] }"
  | Unary (Option, t) -> File.write fmt "%a" construct_param t
  | Contract _ -> File.write fmt "{ prim: 'unit' }"

let print_call file name _network entrypoint =
  let ep_name, _ = entrypoint in
  let ep_name = show_sanitized_name ep_name in
  File.write file ~sp:2 "%sCall: async function(){@." ep_name ;
  File.write file ~sp:3 "console.log('Entrypoint : %s called')@." ep_name ;
  File.write file ~sp:3 "this.ep.disabled = true;@." ;
  File.write file ~sp:6 "this.storage_disabled = true;@." ;
  File.write file ~sp:3
    "this.factori.tezos.setWalletProvider(this.factori.wallet)@." ;
  File.write file ~sp:3 "let arg = {@." ;
  File.write file ~sp:4 "amount: this.senders.amount_%s,@." ep_name ;
  File.write file ~sp:4 "to: this.kt1,@." ;
  File.write file ~sp:4
    "parameter: { entrypoint:'%s', value: %s.%s_encode(this.ep.param_%s) }@."
    ep_name name ep_name ep_name ;
  File.write file ~sp:3 "}@." ;
  File.write file ~sp:3 "try{@." ;
  File.write file ~sp:4
    "const op = await this.factori.tezos.wallet.transfer(arg).send()@." ;
  File.write file ~sp:4 "await op.confirmation();@." ;
  File.write file ~sp:4
    "this.store = %s.Storage_type_decode(await this.get_storage())@." name ;
  File.write file ~sp:3 "}@." ;
  File.write file ~sp:3 "catch(error){@." ;
  File.write file ~sp:4 "console.log(error)@." ;
  File.write file ~sp:3 "}@." ;
  File.write file ~sp:4 "this.ep.disabled = false;@." ;
  File.write file ~sp:4 "this.storage_disabled = false;@." ;
  File.write file ~sp:2 "},@."

let print_entrypoints_call file name network entrypoints =
  List.iter (print_call file name network) entrypoints

let print_params_vars fmt entrypoints =
  File.write fmt "ep :{@." ;
  File.write fmt ~sp:4 "disabled: false,@." ;
  Format.pp_print_list ~pp_sep:(Factori_utils.tag ",@.")
    (fun fmt (n, t) ->
      File.write fmt ~sp:4 "param_%s: %a" (show_sanitized_name n)
        construct_param t)
    fmt entrypoints ;
  File.write fmt "@." ;
  File.write fmt ~sp:3 "}"

let print_senders fmt entrypoints =
  File.write fmt "senders :{@." ;
  Format.pp_print_list ~pp_sep:(Factori_utils.tag ",@.")
    (fun fmt (n, _) ->
      let n = show_sanitized_name n in
      File.write fmt ~sp:4 "amount_%s:0" n)
    fmt entrypoints ;
  File.write fmt "@." ;
  File.write fmt ~sp:3 "}"

let rec get_all_ort res = function
  | Base _ -> res
  | Binary (_, (k, v)) -> get_all_ort (get_all_ort res k) v
  | Record p ->
    Pair_type.fold_paired_type ~f:(fun _ t acc -> get_all_ort acc t) res p
  | TVar (_, t) -> get_all_ort res t
  | Annot (t, _) -> get_all_ort res t
  | Pair l -> List.fold_left (fun acc t -> get_all_ort acc t) res l
  | Unary (_, t) -> get_all_ort res t
  | Contract _ -> res
  | Or ort ->
    Or_type.fold_or_type (fun t acc -> get_all_ort acc t) (ort :: res) ort

let beacon_network network =
  match network with
  | "flextesa" -> "custom"
  | _ -> network

let rpc_url network =
  match network with
  | "flextesa" -> "rpcUrl: 'http://localhost:20000'"
  | _ -> ""

let contract_vue dir name kt1 network interface storage_type =
  let entrypoints = get_entrypoints interface in
  let components_dir = get_vue_components_dir ~dir () in
  let file_name = Factori_config.( // ) components_dir (name ^ ".vue") in
  let file = File.create_file file_name in
  File.write file "<template>@." ;
  File.write file "<a-affix :offset-top='50'>" ;
  File.write file "<router-link to='/'>@." ;
  File.write file
    "<a-button type='primary' shape='round'>Back to contracts</a-button>" ;
  File.write file "</router-link>@." ;
  File.write file
    "<a-button @click='connectWallet' type='primary' shape='round' \
     style='position: fixed; right: 70px'>{{ tz1 }}</a-button>@." ;
  File.write file "</a-affix>" ;
  File.write file "<a-divider>Storage</a-divider>@." ;
  Storage.pp file ~sp:7 storage_type ;
  File.write file "<a-divider>Entrypoints</a-divider>@." ;
  Entrypoints.pp file ~sp:5 entrypoints ;
  File.write file "</template>@." ;

  File.write file "<script>@." ;
  File.write file "import { TezosToolkit } from '@taquito/taquito'@." ;
  File.write file "import * as functolib from '../functolib'@." ;
  File.write file "import * as %s from '../%s_interface'@." name name ;
  File.write file "import { BeaconWallet } from '@taquito/beacon-wallet'@." ;
  File.write file "export default {@." ;
  File.write file ~sp:1 "props: ['factori'],@." ;
  File.write file ~sp:1 "name: '%s',@." name ;
  File.write file ~sp:1 "data: function(){@." ;
  File.write file ~sp:2 "return {@." ;
  File.write file ~sp:3 "storage_disabled: false,@." ;
  File.write file ~sp:3 "tz1: 'Connect wallet',@." ;
  File.write file ~sp:3 "network: '%s',@." network ;
  (* TODO: Add other networks *)
  File.write file ~sp:3 "kt1: '%s',@." kt1 ;
  File.write file ~sp:3 "store : null,@." ;
  File.write file ~sp:3 "%a,@." print_params_vars entrypoints ;
  File.write file ~sp:3 "%a@." print_senders entrypoints ;
  File.write file ~sp:2 "}@." ;
  File.write file ~sp:1 "},@." ;
  File.write file ~sp:1 "async created(){@." ;
  File.write file ~sp:2 "this.storage_disabled = true;@." ;
  File.write file ~sp:2
    "this.store = %s.Storage_type_decode(await this.get_storage())@." name ;
  File.write file ~sp:3 "this.storage_disabled = false;@." ;
  File.write file ~sp:3
    "const activeAccount = await \
     this.factori.wallet.client.getActiveAccount();@." ;
  File.write file ~sp:3 "if(activeAccount)@." ;
  File.write file ~sp:4 "this.tz1 = 'Connected: ' + activeAccount.address;@." ;
  File.write file ~sp:3 "else@." ;
  File.write file ~sp:4 "this.tz1 = 'Connect wallet';@." ;
  File.write file ~sp:1 "},@." ;
  File.write file ~sp:1 "methods:{@." ;
  File.write file ~sp:2 "connectWallet: async function(){@." ;
  File.write file ~sp:3 "this.factori.wallet@." ;
  File.write file ~sp:3 ".requestPermissions({@." ;
  File.write file ~sp:4 "network: {@." ;
  File.write file ~sp:5 "type: '%s',@." (beacon_network network) ;
  File.write file ~sp:5 "%s" (rpc_url network) ;
  File.write file ~sp:4 "}@." ;
  File.write file ~sp:3 "})@." ;
  File.write file ~sp:3 ".then((_) => this.factori.wallet.getPKH())@." ;
  File.write file ~sp:3
    ".then((address) => this.tz1 = 'Connected: ' + address)@." ;
  File.write file ~sp:3
    ".then(() => this.factori.tezos.setWalletProvider(this.factori.wallet));@." ;
  File.write file ~sp:2 "},@." ;
  print_get_storage file network ;
  print_entrypoints_call file name network entrypoints ;
  File.write file ~sp:1 "}@." ;
  File.write file "}@." ;
  File.write file "</script>@."

let contract_format_to_string cf =
  match cf with
  | Michelson -> "Michelson"
  | MichelsonJson -> "MichelsonJson"
  | KT1 -> "KT1"

let contracts_vue dir contracts =
  let _truncate_text s =
    let l = String.length s in
    if l > 12 then
      String.sub s 0 5 ^ "..." ^ String.sub s (l - 5) 5
    else
      s in
  let components_dir = get_vue_components_dir ~dir () in
  let path = Factori_config.( // ) components_dir "ContractsList.vue" in
  let file = File.create_file path in
  File.write file "<template>@." ;
  File.write file "<a-list item-layout:='horizontal'>@." ;
  List.iter
    (fun x ->
      let kt1, network = Option.value ~default:("", "") x.original_kt1 in
      File.write file "<a-list-item>@." ;
      File.write file "<a-list-item-meta description='%s'>@." kt1 ;
      File.write file "<template #title>@." ;
      File.write file
        "<a-typography-link strong href='/%s'>%s</a-typography-link>@."
        x.contract_name x.contract_name ;
      File.write file "</template>@." ;
      File.write file "</a-list-item-meta>@." ;
      File.write file "<template #actions>@." ;
      File.write file "<a-tag color='blue'>%s</a-tag>@." network ;
      File.write file "<a-typography-text strong>%s</a-typography-text>@."
        x.import_date ;
      File.write file "<router-link to='/%s'>@." x.contract_name ;
      File.write file "<a-button type='primary' block>Open dApp</a-button>@." ;
      File.write file "</router-link>@." ;
      File.write file "</template>@." ;
      File.write file "" ;
      File.write file "</a-list-item>@.")
    contracts ;
  File.write file "</a-list>@." ;
  File.write file "</template>@."

let app_vue dir =
  let path = Factori_config.get_typescript_app_vue_path ~dir () in
  if not (File.exists path) then
    let file = File.create_file path in
    let content =
      {|
<template>
  <a-back-top />
  <a-layout>
    <a-layout-content :style="{ padding: '0 50px', marginTop: '64px' }">
      <br />
      <br />
      <div :style="{ padding: '24px', minHeight: '600px' }">
        <router-view :factori='factori'></router-view>
      </div>
      </a-layout-content>
    <a-layout-footer :style="{ textAlign: 'center' }">
      Factori by Functori
    </a-layout-footer>
  </a-layout>
</template>

<script>
import { BeaconWallet } from '@taquito/beacon-wallet';
import { TezosToolkit } from '@taquito/taquito';
import { ref } from 'vue';

export default {
  name: 'app',
  data: function(){
    return {
      factori:{
        tezos: new TezosToolkit('https://ithacanet.tezos.marigold.dev'),
        wallet: new BeaconWallet({ name: 'Factori App' })
        }
      }
    }
}
</script>
<style>
  @import url('https://fonts.googleapis.com/css2?family=Ubuntu&display=swap');
* {
  font-family: 'Ubuntu', sans-serif;
}
</style>
|}
    in
    File.write file "%s" content

let wallet_vue dir =
  let component_dir = Factori_config.get_vue_components_dir ~dir () in
  let path = Factori_config.( // ) component_dir "FactoriWallet.vue" in
  if not (File.exists path) then
    let file = File.create_file path in
    let content =
      {|
<template>
    <div class="col-12 col-md-12 col-xxl-6 d-flex order-3 order-xxl-2">
       <div class="card flex-fill w-100">
         <div class="card-header">
           <h2 class="card-title mb-0">Wallet</h2>
         </div>
         <div class="card-body px-4">
<button @click='connectWallet' class='btn btn-success'>Connect Your Wallet</button>
</div></div></div>
</template>

<script>
export default {
  name: 'factoriwallet',
  props: ['factori'],
  methods:{
    connectWallet: async function () {
        console.log('Connect Wallet Clicked')
        this.factori.wallet
        .requestPermissions({
            network: {
                type: "ghostnet"
            }
        })
        .then((_) => this.factori.wallet.getPKH())
        .then((address) => console.log(address))
        .then(() => this.factori.tezos.setWalletProvider(this.factori.wallet));
}
  }
}
</script>
|}
    in
    File.write file "%s" content

let produce_web ~kt1 ~network ~name dir interface contracts ~storage_type =
  contract_vue dir name
    (Option.value ~default:"NO KT1 PROVIDED" kt1)
    network interface storage_type ;
  wallet_vue dir ;
  contracts_vue dir contracts ;
  app_vue dir ;
  index_ts dir contracts
