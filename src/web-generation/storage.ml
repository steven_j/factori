open Factori_file
open Factori_utils
open Types
open Common

let rec pp_paired fmt (sp, acc, p) =
  let open Pair_type in
  match p with
  | LeafP (name, t) ->
    File.write fmt "<a-list-item>@." ;
    File.write fmt "%a" pp_type
      ( sp,
        Some name,
        false,
        Accessors.Field (show_sanitized_name name) :: acc,
        t ) ;
    File.write fmt "</a-list-item>@."
  | PairP l -> List.iter (fun p -> pp_paired fmt (sp, acc, p)) l

and pp_or fmt (sp, acc, ort) =
  let l = get_all_constructor ort in
  File.write fmt "<a-collapse>@." ;
  List.iter
    (fun (n, t) ->
      let kind = Accessors.Field "kind" :: acc in
      File.write fmt ~sp
        "<a-collapse-panel header='%s' v-if='store && %a == \
         \"%s_constructor\"'>@."
        n Accessors.pp_ts kind n ;
      File.write fmt ~sp:(sp + 1)
        "<div v-if='store && %a == \"%s_constructor\"' class='collapse' \
         id='collapseOr%s'>@."
        Accessors.pp_ts kind n n ;
      File.write fmt "%a" pp_type
        (sp + 3, None, false, Accessors.Field (n ^ "_element") :: acc, t) ;
      File.write fmt ~sp:(sp + 1) "</div>@." ;
      File.write fmt "</a-collapse-panel>@.")
    l ;
  File.write fmt "</a-collapse>@."

and pp_params fmt (name, l) =
  let rec parameter_type fmt t =
    match t with
    | Base b -> File.write fmt "%a@." print_base b
    | Binary (Map, (k, v)) ->
      File.write fmt
        "<a-typography-text type='warning'>map</a-typography-text>&lt;%a, \
         %a&gt;@."
        parameter_type k parameter_type v
    | Binary (BigMap, (k, v)) ->
      File.write fmt
        "<a-typography-text type='warning'>big_map</a-typography-text>&lt;%a, \
         %a&gt;@."
        parameter_type k parameter_type v
    | Record _ | TVar (_, Record _) | Annot (Record _, _) ->
      File.write fmt
        "<a-typography-text type='success' code>record</a-typography-text>@."
    | TVar (_, t) -> parameter_type fmt t
    | Pair l ->
      File.write fmt
        "<a-typography-text type='warning' \
         code>pair</a-typography-text>&lt;%a&gt;@."
        (Format.pp_print_list ~pp_sep:(Factori_utils.tag ",") (fun fmt t ->
             parameter_type fmt t))
        l
    | Unary (u, t) ->
      File.write fmt
        "<a-typography-text type='warning'>%s&lt;%a&gt;</a-typography-text>@."
        (str_of_unary u) parameter_type t
    | Binary (Lambda, _) ->
      File.write fmt
        "<a-typography-text type='warning'>lambda</a-typography-text>@."
    | Contract _ ->
      File.write fmt
        "<a-typography-text type='warning'>contract</a-typography-text>@."
    | Or _ ->
      File.write fmt
        "<a-typography-text type='warning'>sum_type</typography-text>@."
    | Annot (_, _) ->
      File.write fmt
        "<a-typography-text type='warning'>annot</typography-text>@." in
  File.write fmt
    "<a-typography-text type='warning' code>%s</a-typography-text>&lt; %a&gt;@."
    name
    (Format.pp_print_list ~pp_sep:(Factori_utils.tag ", ") (fun fmt t ->
         File.write fmt "%a" parameter_type t))
    l

and pp_type fmt (sp, name, opt, acc, t) =
  match t with
  | Pair l ->
    File.write fmt "<a-list>@." ;
    List.iteri
      (fun i t ->
        File.write fmt "<a-list-item>@." ;
        File.write fmt "%a@." pp_type
          (sp + 1, None, false, Accessors.Index i :: acc, t) ;
        File.write fmt "</a-list-item>@.")
      l ;
    File.write fmt "</a-list>@."
  | Record p ->
    let n =
      match name with
      | None -> ""
      | Some n -> show_sanitized_name n ^ ":" in
    File.write fmt "<a-card title='%s object'>@." n ;
    File.write fmt "<a-list>@." ;
    File.write fmt "%a" pp_paired (sp + 4, acc, p) ;
    File.write fmt "</a-list>" ;
    File.write fmt "</a-card>@."
  | TVar (n, Or ort) ->
    File.write fmt ~sp
      "<a-typography-text type='warning' code>%s</a-typography-text> %a@."
      (show_sanitized_name n) print_arg
      (Option.map show_sanitized_name name) ;
    File.write fmt ~sp:(sp + 1) ":@." ;
    File.write fmt "%a" pp_or (sp + 1, acc, ort)
  | TVar (_, Record p) ->
    File.write fmt "%a@." pp_type (sp, name, opt, acc, Record p)
  | TVar (_, t) -> File.write fmt "%a@." pp_type (sp, name, opt, acc, t)
  | Annot _ -> File.write fmt "Annot@."
  | Or ort -> File.write fmt "%a@." pp_or (sp, acc, ort)
  | Base Unit -> File.write fmt ~sp "%a@." print_base Unit
  | Base b ->
    File.write fmt ~sp "<a-space>@." ;
    let qm =
      if opt then
        "<a-typography-text code>?</a-typography-text>"
      else
        "" in
    if name = None then
      File.write fmt ~sp "%s%a@." qm print_base b
    else
      File.write fmt ~sp "%s%a %a@." qm print_base b print_arg
        (Option.map show_sanitized_name name) ;
    if opt then
      File.write fmt ~sp:(sp + 1)
        "<span v-if='store'>: {{ (%a == null) ? 'null' : %a }}</span>@."
        Accessors.pp_ts acc Accessors.pp_ts acc
    else
      File.write fmt ~sp:(sp + 1) "<span v-if='store'>: {{ %a }}</span>@."
        Accessors.pp_ts acc ;
    File.write fmt ~sp "</a-space>@."
  | Unary (List, t) ->
    File.write fmt "<a-space align='start'>@." ;
    begin
      match name with
      | None -> File.write fmt "%a" pp_params ("list", [t])
      | _ -> File.write fmt "%a:@." pp_params ("list", [t])
    end ;
    File.write fmt "<a-collapse>@." ;
    begin
      match name with
      | None -> File.write fmt "<a-collapse-panel header='List'>@."
      | Some n ->
        File.write fmt "<a-collapse-panel header='%s'>@."
          (show_sanitized_name n)
    end ;
    File.write fmt "<a-list>@." ;
    File.write fmt "<a-list-item v-if='store' v-for='(item, index) in %a'>@."
      Accessors.pp_ts acc ;
    File.write fmt "<div class='space-align-block'>@." ;
    File.write fmt "<a-space align-start>@." ;
    File.write fmt "[{{ index }}] %a@." pp_type
      (sp, None, opt, [Accessors.Field "item"], t) ;
    File.write fmt "</a-space>@." ;
    File.write fmt "</div>@." ;
    File.write fmt "</a-list-item>@." ;
    File.write fmt "</a-list>@." ;
    File.write fmt "</a-collapse-panel>@." ;
    File.write fmt "</a-collapse>@." ;
    File.write fmt "</a-space>@."
  | Binary (BigMap, (k, v)) ->
    pp_params fmt ("big_map", [k; v]) ;
    File.write fmt "%a : " print_arg (Option.map show_sanitized_name name) ;
    File.write fmt
      "<a-typography-link v-if='store' :href=\"'https://' + network + \
       '.tzkt.io/' + kt1 + '/storage/' + %a\" target='_blank'>@."
      Accessors.pp_ts
      (Accessors.Field "value" :: acc) ;
    File.write fmt "ID : {{ %a }}@." Accessors.pp_ts
      (Accessors.Field "value" :: acc) ;
    File.write fmt "</a-typography-link>@."
  | Binary (Map, (k, v)) ->
    File.write fmt "<a-space align='start'>@." ;
    begin
      match name with
      | None -> File.write fmt "%a" pp_params ("map", [k; v])
      | _ -> File.write fmt "%a:@." pp_params ("map", [k; v])
    end ;
    File.write fmt "<a-collapse>@." ;
    begin
      match name with
      | None -> File.write fmt "<a-collapse-panel header='Map'>@."
      | Some n ->
        File.write fmt "<a-collapse-panel header='%s'>@."
          (show_sanitized_name n)
    end ;
    File.write fmt "<a-list>@." ;
    File.write fmt "<a-list-item v-if='store' v-for='(item, index) in %a'>@."
      Accessors.pp_ts acc ;
    File.write fmt "<div class='space-align-block'>@." ;
    File.write fmt "<a-space align-start>@." ;
    File.write fmt "%a@." pp_type
      (sp, None, opt, [Accessors.Index 0; Accessors.Field "item"], k) ;
    File.write fmt
      "<span class='material-symbols-outlined'>chevron_right</span>@." ;
    File.write fmt "%a@." pp_type
      (sp, None, opt, [Accessors.Index 1; Accessors.Field "item"], v) ;
    File.write fmt "</a-space>@." ;
    File.write fmt "</div>@." ;
    File.write fmt "</a-list-item>@." ;
    File.write fmt "</a-list>@." ;
    File.write fmt "</a-collapse-panel>@." ;
    File.write fmt "</a-collapse>@." ;
    File.write fmt "</a-space>@."
  | Unary (Set, t) ->
    File.write fmt "<a-space align='start'>@." ;
    begin
      match name with
      | None -> File.write fmt "%a" pp_params ("set", [t])
      | _ -> File.write fmt "%a:@." pp_params ("set", [t])
    end ;
    File.write fmt "<a-collapse>@." ;
    begin
      match name with
      | None -> File.write fmt "<a-collapse-panel header='Set'>@."
      | Some n ->
        File.write fmt "<a-collapse-panel header='%s'>@."
          (show_sanitized_name n)
    end ;
    File.write fmt "<a-list>@." ;
    File.write fmt "<a-list-item v-if='store' v-for='(item, index) in %a'>@."
      Accessors.pp_ts acc ;
    File.write fmt "<div class='space-align-block'>@." ;
    File.write fmt "<a-space align-start>@." ;
    File.write fmt "[{{ index }}]@." ;
    File.write fmt
      "<span class='material-symbols-outlined'>chevron_right</span>@." ;
    File.write fmt "%a@." pp_type (sp, None, opt, [Accessors.Field "item"], t) ;
    File.write fmt "</a-space>@." ;
    File.write fmt "</div>@." ;
    File.write fmt "</a-list-item>@." ;
    File.write fmt "</a-list>@." ;
    File.write fmt "</a-collapse-panel>@." ;
    File.write fmt "</a-collapse>@." ;
    File.write fmt "</a-space>@."
  | Unary (Ticket, _) ->
    File.write fmt
      "<a-typography-text type='warning' code>ticket</a-typography-text>@."
  | Binary (Lambda, _) ->
    File.write fmt
      "<a-typography-text type='warning' code>Lambda</a-typography-text>@."
  | Unary (Option, t) -> pp_type fmt (sp, name, true, acc, t)
  | Contract _ ->
    File.write fmt
      "<a-typography-text type='warning' code>Contract</a-typography-text>@."

let pp_aux ?(sp = 0) file storage_type =
  match storage_type with
  | TVar (_, t) ->
    File.write file "%a" pp_type (sp, None, false, [Accessors.Field "store"], t)
  | Base Unit -> File.write file ~sp "%a@." print_base Unit
  | Base b -> File.write file ~sp "%a = {{ store }}@." print_base b
  | _ ->
    File.write file
      "<a-typography-text type='danger'>This type is not \
       handled.</a-typography-text>"

let pp file ?(sp = 0) storage_type =
  File.write file ~sp "<a-card :loading='storage_disabled'>@." ;
  File.write file "%a" (pp_aux ~sp) storage_type ;
  File.write file ~sp "</a-card>@."
