open Ppxlib
open Ast_builder.Default

let has_id_in_deep_ldot id x =
  match x with
  | Lident id1 -> id1 = id
  | Ldot (_, id1) -> id1 = id
  | Lapply (_, _) -> false

let aux =
  object (_my_object)
    inherit Ast_traverse.map as super

    method! expression expr =
      match expr.pexp_desc with
      | Pexp_construct ({ txt = Lident "[]"; _ }, _) ->
        (* Format.eprintf "[in aux Pexp_construct []]%s@." (Pprintast.string_of_expression expr); *)
        expr
      | Pexp_construct ({ txt; _ }, _) when has_id_in_deep_ldot "Id" txt ->
        (* Format.eprintf "[in aux Pexp_construct Id]%s@." (Pprintast.string_of_expression expr); *)
        expr
      | Pexp_open (open_declaration, expr) ->
        (* Format.eprintf "[in aux Pexp_open]%s@." (Pprintast.string_of_expression expr); *)
        {
          expr with
          pexp_desc =
            Pexp_open
              ( open_declaration,
                let loc = expr.pexp_loc in
                [%expr
                  Factori_abstract_types.Abstract.lift
                    [%e super#expression expr]] );
        }
      | Pexp_construct
          ( ({ txt = Lident "::"; _ } as li),
            Some
              ({ pexp_desc = Pexp_tuple [x; rest]; pexp_loc_stack = []; _ } as
              e) ) ->
        {
          expr with
          pexp_desc =
            Pexp_construct
              ( { li with txt = Lident "::" },
                Some
                  {
                    e with
                    pexp_desc =
                      Pexp_tuple [super#expression x; super#expression rest];
                    pexp_loc_stack = [];
                  } );
        }
      | _ ->
        (* Format.eprintf "[in aux other cases]%s@." (Pprintast.string_of_expression expr); *)
        let loc = expr.pexp_loc in
        [%expr Factori_abstract_types.Abstract.lift [%e super#expression expr]]
  end

let to_ml =
  object (_self)
    inherit Ast_traverse.map as super

    method! value_binding vb =
      match
        ( List.exists
            (fun a -> a.attr_name.txt = "storage" || a.attr_name.txt = "param")
            vb.pvb_attributes,
          vb.pvb_pat.ppat_desc )
      with
      | true, Ppat_var { txt = _id; _ } ->
        let expr = aux#expression vb.pvb_expr in
        (* Format.eprintf "[in value binding]%s@." (Pprintast.string_of_expression expr); *)
        value_binding ~loc:vb.pvb_loc ~pat:vb.pvb_pat ~expr
      | _ ->
        (* Format.eprintf "[in value binding, default case]%s@." (Pprintast.string_of_expression vb.pvb_expr); *)
        super#value_binding vb
  end

let () =
  Ppxlib.Driver.register_transformation "factori_scenario_ppx"
    ~impl:to_ml#structure
