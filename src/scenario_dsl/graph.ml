(* TODO: unify this with infer_entrypoints (maybe use tsort for
   topological sorting too *)
(* The Graph module is used to linearize scenarios with forks, using
   the topological sort *)
exception CycleFound

type 'a graph = ('a * 'a list) list

let dfs graph visited start_node =
  let rec explore path visited node =
    if List.mem node path then
      raise CycleFound
    else if List.mem node visited then
      visited
    else
      let new_path = node :: path in
      let edges =
        match List.assoc_opt node graph with
        | None -> []
        | Some l -> l in
      let visited = List.fold_left (explore new_path) visited edges in
      node :: visited in
  explore [] visited start_node

let toposort graph =
  List.fold_left (fun visited (node, _) -> dfs graph visited node) [] graph
