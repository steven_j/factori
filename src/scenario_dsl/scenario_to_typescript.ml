open Context
open Format
open Tzfunc.Proto
open Context
open Scenario_value

let show_code c = asprintf "(%s)" (EzEncoding.construct script_expr_enc.json c)

let show_micheline m =
  Format.asprintf "new Parser().parseJSON(%s)"
    (EzEncoding.construct micheline_enc m)

let show_generate_identity universe i =
  Format.asprintf "get_identity (\"%d_%a\")" i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ";")
       (fun ppf d -> fprintf ppf "%d" d))
    universe

let show_raw_identity universe i =
  Format.asprintf "%d_%a" i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ";")
       (fun ppf d -> fprintf ppf "%d" d))
    universe

let fresh_native =
  let counter = ref 0 in
  fun () ->
    let res = !counter in
    incr counter ;
    res

let show_kt1_typescript names universe = function
  | StaticKt1 kt1 -> sprintf "%s" kt1
  | IdKt1 id -> asprintf "%a" (print_name ~underscore:true names universe) id

let pp_use_uv_typescript ?(static = false) ?(alter = fun x -> x) names universe
    ppf id =
  fprintf ppf "%a" (print_name ~static ~alter names universe) id

let print_id_typescript names universe ppf idpointer =
  fprintf ppf "%a" (pp_use_uv_typescript names universe) idpointer

let show_uv_typescript names universe = function
  (* | NamedValue vl -> show_named_value vl *)
  | DefaultIdentity ParameterNetwork -> sprintf "(get_default_identity network)"
  | DefaultIdentity (SpecificNetwork n) ->
    sprintf "(functolib.get_node %s)" (Factori_utils.get_default_identity n)
  | Code_uv c -> show_code c
  | Amount_uv amount -> show_amount amount
  | Kt1_uv kt1 -> show_kt1_typescript names universe kt1
  | Network_uv ParameterNetwork -> "network"
  | Network_uv network -> show_network network
  | String_uv s -> sprintf "\"%s\"" s
  | Entrypoint_name_uv s -> sprintf "\"%s\"" s
  | Micheline_uv c -> show_micheline c
  | Address_uv address -> show_address address
  | Generate_key seed -> show_generate_identity universe seed
  | NativeAbstractValue (AValue v) ->
    asprintf "%a" (Pp_abstract_value.pp_value_to_typescript names universe) v
  | Id id_pointer ->
    asprintf "%a" (print_id_typescript names universe) id_pointer
  | FunctionCall (GetAddress, [id]) ->
    asprintf "(%a).pkh" (print_id_typescript names universe) id
  | FunctionCall (GetAddress, _) ->
    failwith "Wrong number of arguments: GetAddress has one argument"
(* | _ -> failwith "TODO" *)

let pp_definition_uv_typescript names universe id ppf uv =
  match uv with
  | uv ->
    fprintf ppf "let %a = %s;@."
      (print_name names universe)
      id
      (show_uv_typescript names universe uv)

let print_network_typescript ppf network =
  match network with
  | SpecificNetwork n -> fprintf ppf "\"%s\"" n
  | ParameterNetwork -> fprintf ppf "network"

let print_deploy_typescript names universe ppf id (d : u_id deploy) =
  fprintf ppf
    "setSigner(tezosKit,%a.sk);\n\
     let config_%a = {node_addr : get_node(%a)};\n\
     let %a = await %s_interface.deploy_%s(tezosKit,%a,config_%a);\n\
     console.log(`[scenario%a]Deployed KT1 ${%a}`)\n"
    (pp_use_uv_typescript names universe)
    d.from
    (pp_use_uv_typescript names universe)
    id
    (pp_use_uv_typescript names universe)
    d.network
    (pp_use_uv_typescript names universe)
    id
    (String.capitalize_ascii d.contract_name)
    (* (pp_use_uv_typescript names universe)
     * d.amount *)
    (* for now it seems amount can't be used in deploy in Typescript? *)
    d.contract_name
    (pp_use_uv_typescript names universe)
    d.storage
    (pp_use_uv_typescript names universe)
    id Universes.print_universe universe
    (pp_use_uv_typescript names universe)
    id

let print_call_typescript names universe id ppf (c : u_id call) =
  let contract_interface =
    sprintf "%s_interface" (String.capitalize_ascii c.contract_name) in
  fprintf ppf
    "setSigner(tezosKit,%a.sk);\n\
     let config_%a = {node_addr : get_node(%a)};\n\
     let %a = await %s.call_%s(tezosKit,%a,%a,%a) ;@."
    (pp_use_uv_typescript names universe)
    c.from
    (pp_use_uv_typescript names universe)
    id
    (pp_use_uv_typescript names universe)
    c.network
    (pp_use_uv_typescript names universe)
    id contract_interface c.entrypoint
    (pp_use_uv_typescript names universe)
    c.contract
    (pp_use_uv_typescript names universe)
    c.param
    (pp_use_uv_typescript names universe)
    c.amount

let print_failed_call_typescript names universe id ppf
    (expected_bc_error : string) (msg : string) (c : u_id call) =
  let contract_interface =
    sprintf "%s_interface" (String.capitalize_ascii c.contract_name) in
  fprintf ppf
    "setSigner(tezosKit,%a.sk);\n\
     let config_%a = {node_addr : get_node(%a)};\n\
     let %a = await \
     %s.assert_failwith_str_%s(tezosKit,%a,%a,\"%s\",\"[scenario%a]\",\"%s\",%a) \
     ;@."
    (pp_use_uv_typescript names universe)
    c.from
    (pp_use_uv_typescript names universe)
    id
    (pp_use_uv_typescript names universe)
    c.network
    (pp_use_uv_typescript names universe)
    id contract_interface c.entrypoint
    (pp_use_uv_typescript names universe)
    c.contract
    (pp_use_uv_typescript names universe)
    c.param expected_bc_error Universes.print_universe universe msg
    (pp_use_uv_typescript names universe)
    c.amount

let pp_seq_instruction_to_typescript names universe ppf
    ((id : u_id), (v : u_id value)) =
  match v with
  | Unary u -> pp_definition_uv_typescript names universe id ppf u
  | Checkpoint -> (* fprintf ppf "(\* Checkpoint before fork *\)@." *) ()
  | Deploy deploy -> print_deploy_typescript names universe ppf id deploy
  | Call call -> print_call_typescript names universe id ppf call
  | FailedCall (Failed (s, msg, call)) ->
    print_failed_call_typescript names universe id ppf s msg call
  | Fork _ | Seq _ -> ()

(* Print a linear sequence of instructions to a Typescript scenario *)
let pp_sequence_to_typescript ppf (s : sequence) =
  List.iter
    (fprintf ppf
       "/* Universe %a */\n\n\
        let scenario%a = async function(network){\n\
        console.log('[scenario%a]Entering scenario%a')\n\
        let node_addr = get_node(network);\n\
        const tezosKit = new TezosToolkit(node_addr)@." Universes.print_universe
       s.seq_universe Universes.print_universe s.seq_universe
       Universes.print_universe s.seq_universe Universes.print_universe
       s.seq_universe ;
     pp_seq_instruction_to_typescript s.seq_names s.seq_universe ppf)
    s.seq ;
  let n = List.length s.seq in
  if n = 0 then
    fprintf ppf
      "return; (* Warning: there was an empty list of instructions *)@.}\n"
  else
    let last_element = List.nth s.seq (n - 1) in
    let last_name =
      Format.asprintf "%a"
        (print_name s.seq_names s.seq_universe)
        (fst last_element) in
    fprintf ppf "return (%s);@.}\n" last_name

let prelude contract_names ppf () =
  fprintf ppf
    {|import {
TezosToolkit,
MichelsonMap,
} from "@taquito/taquito";
import { MichelsonV1Expression } from "@taquito/rpc";
import {get_node,wallet,big_map,setSigner, config, alice_flextesa, bob_flextesa, wait_inclusion,make_abstract_bm, make_literal_bm } from "./functolib";
%a
;|}
    (pp_print_list ~pp_sep:(Factori_utils.tag "\n") (fun ppf cname ->
         fprintf ppf "import * as %s_interface from \"./%s_interface\""
           (String.capitalize_ascii cname)
           cname))
    contract_names

(* Copied from Blockchain *)
let get_identity (seed : string) : identity =
  let open Crypto in
  let better_seed =
    String.init 32 (fun i -> String.get seed (i mod String.length seed)) in
  let sk = Tzfunc.Crypto.Sk.T.mk @@ Raw.mk better_seed in
  let pk = Crypto.Sk.to_public_key sk in
  let curve = `ed25519 in
  let pkh = Pkh.b58enc ~curve @@ Pk.hash pk in
  let sk = Sk.b58enc ~curve sk in
  let pk = Pk.b58enc ~curve pk in
  { pkh; sk; pk }

let pp_funding ppf (ss : sequence list) =
  let show _names universe = function
    | Generate_key seed -> show_raw_identity universe seed
    | _ ->
      failwith
        "[pp_funding][show] This function should only find Generate_key \
         elements" in
  let addr_amount_map = Compilation_utils.get_addr_balances ~show_uv:show ss in
  fprintf ppf
    "\n\n\
     function get_identity(seed : string) : wallet{\n\
     switch(seed){%a\n\
     default:\n\
     throw (\"Unknown seed: \" + seed);\n\
     }\n\
     }@."
    (pp_print_seq ~pp_sep:(Factori_utils.tag "") (fun ppf (k, _) ->
         let ident = get_identity k in
         fprintf ppf
           "\ncase \"%s\":\nreturn {sk : \"%s\",pk: \"%s\",pkh : \"%s\" }" k
           ident.sk ident.pk ident.pkh))
    (AddrBalances.to_seq addr_amount_map) ;
  let addr_amount_map =
    Compilation_utils.get_addr_balances ~show_uv:show_uv_typescript ss in
  fprintf ppf
    "\n\n\
     let fund = async function(network){\n\
     let node_addr = get_node(network);\n\
     const tezosKit = new TezosToolkit(node_addr);\n\
     setSigner(tezosKit,alice_flextesa.sk);\n\
     const batch = await tezosKit.wallet.batch()\n\
     .withTransfer(%a\n\
     const batchOp = await batch.send();\n\
     await batchOp.confirmation();@.\n\
     }\n\
    \              "
    (pp_print_seq ~pp_sep:(Factori_utils.tag "\n.withTransfer(")
       (fun ppf (k, v) ->
         fprintf ppf "{ to: %s.pkh, amount: %s, mutez:true })" k v))
    (AddrBalances.to_seq
       (AddrBalances.filter (fun _k v -> v <> "0") addr_amount_map))

let pp_scenario_from_sequences_to_typescript ?(funding = true) ppf
    (ss : sequence list) =
  let contract_names = extract_contract_names_from_sequence_list ss in
  prelude contract_names ppf () ;
  fprintf ppf "%s"
    (if funding then
      asprintf "%a" pp_funding ss
    else
      "") ;
  List.iter (pp_sequence_to_typescript ppf) ss ;
  fprintf ppf
    "\n\n\
     let main = async function(network : string){\n\
     let node_addr = get_node(network);\n\
     const tezosKit = new TezosToolkit(node_addr)\n\
     console.log('Entering main');\n\
     await fund(network);\n\
     console.log('Finished funding. Launching scenarios');\n\
     await Promise.allSettled([%a]);}\n\n\
     main('flextesa')"
    (pp_print_list ~pp_sep:(Factori_utils.tag ",") (fun ppf s ->
         fprintf ppf "scenario%a(network)" Universes.print_universe
           s.seq_universe))
    ss
