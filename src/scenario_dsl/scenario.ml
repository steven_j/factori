open Context

(* This module facilitates creating such complex actions as
   deployments, calls or forks *)
module Scenario (Ctxt : CONTEXT) = struct
  open Ctxt
  open Scenario_value

  let context = init_context ()

  let make_checkpoint context = set_value context Checkpoint

  let make_deploy ?(name = None) context deploy =
    let id, context = set_value ~name context (Deploy deploy) in
    (* storage has to exist before deployment event *)
    let context = add_dependency context deploy.storage id in
    let context = add_dependency context deploy.from id in
    let context = add_dependency context deploy.amount id in
    let context = add_dependency context deploy.network id in
    (id, context)

  let make_call ?(name = None) context call =
    let id, context = set_value ~name context (Call call) in
    let context = add_dependency context call.from id in
    let context = add_dependency context call.amount id in
    let context = add_dependency context call.contract id in
    let context = add_dependency context call.network id in
    let context = add_dependency context call.param id in
    (id, context)

  let make_failed_call ?(name = None) context (expected : string) (msg : string)
      call =
    let id, context =
      set_value ~name context (FailedCall (Failed (expected, msg, call))) in
    let context = add_dependency context call.from id in
    let context = add_dependency context call.amount id in
    let context = add_dependency context call.contract id in
    let context = add_dependency context call.network id in
    let context = add_dependency context call.param id in
    (id, context)

  (** after is the id after which the fork should happen  *)
  let make_fork ~(after : u_id) (context : context) fork =
    let open Ctxt in
    let id_checkpoint, context = make_checkpoint context in
    let id, context = set_value context (Fork fork) in
    let context = add_dependency context after id_checkpoint in
    let context = add_dependency context id_checkpoint id in
    (id, context)
end
