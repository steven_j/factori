(* Identifiers for objects in the scenario *)
type id = string [@@deriving show, encoding]
