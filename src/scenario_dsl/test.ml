open Scenario_dsl
open Context
module Test_scenario = Scenario.Scenario (Context)
open Scenario_value
open Ast
module AstToContextInstance = Ast_to_context.AstToContext (Context)

(* Build Ast instead of context below *)
let ast = empty_ast

let ast = insert_at_end "agent1" (unary (Generate_key 1)) ast

let ast = insert_at_end "agent2" (unary (Generate_key 2)) ast

let ast = insert_at_end "amount" (unary (Amount_uv Z.one)) ast

let ast =
  insert_at_end "network" (unary @@ Network_uv (SpecificNetwork "ghostnet")) ast

let contract_name = "test_contract"

let ast =
  insert_at_end "storage"
    (unary @@ Micheline_uv (Mprim { prim = `Unit; args = []; annots = [] }))
    ast

let ast =
  insert_at_end "kt1_deploy_from_agent1"
    (Deploy
       {
         from = "agent1";
         amount = "amount";
         network = "network";
         storage = "storage";
         contract_name;
       })
    ast

let ast =
  insert_at_end "param"
    (unary @@ Micheline_uv (Mprim { prim = `Unit; args = []; annots = [] }))
    ast

let entrypoint1 = "mint"

let call1 =
  Call
    {
      from = "agent1";
      contract = "kt1_deploy_from_agent1";
      network = "network";
      param = "param";
      entrypoint = entrypoint1;
      contract_name;
      amount = "amount";
    }

let entrypoint2 = "transfer"

let call2 =
  Call
    {
      from = "agent2";
      contract = "kt1_deploy_from_agent1";
      network = "network";
      param = "param";
      entrypoint = entrypoint2;
      contract_name;
      amount = "amount";
    }

(* let ast = insert_at_end "call2_from_first_fork" call2 ast *)
let fork = Fork [("call1", call1); ("call2", call2)]

let fork2 =
  Fork
    [
      ("fork'", Fork [("toto1", fork); ("toto2", fork)]);
      ( "call2'branch",
        Seq
          [
            ("call2'", call2);
            ("call3'", call2);
            ("string_toto", unary @@ String_uv "toto");
          ] );
    ]

let ast = insert_at_end "fork2" fork2 ast

let context_from_ast = AstToContextInstance.compile ast

let quick_deploy contract_name () =
  (* let interface_name =
   *   Factori_config.get_ocaml_interface_basename ~contract_name in *)
  let ast = empty_ast in
  let ast = insert_at_end "network" (unary (Network_uv ParameterNetwork)) ast in
  let ast =
    insert_at_end "originator"
      (unary @@ DefaultIdentity ParameterNetwork)
      ast (* TODO: make the ast parametrizable by network *) in
  let ast = insert_at_end "amount" (unary @@ Amount_uv Z.zero) ast in
  let ast = insert_at_end "network" (unary @@ Network_uv ParameterNetwork) ast in
  let ast =
    insert_at_end "storage"
      (unary @@ Micheline_uv (Mprim { prim = `Unit; args = []; annots = [] }))
      (* (Named [interface_name; "initial_blockchain_storage"]) *) ast in
  let ast =
    insert_at_end "deploy"
      (Deploy
         {
           from = "originator";
           amount = "amount";
           network = "network";
           storage = "storage";
           contract_name = Factori_utils.(contract_name.sanitized);
         })
      ast in
  ast

(* Use AstInterface to build a scenario hopefully with less hassle *)
open AstInterface

(* module representing our scenario *)

(* module Sc = (val build_new_scenario () : Scenario) *)

let scenario = new_scenario ()

let contract_name = Factori_utils.sanitized_of_str "toto"

let interface_name = Factori_config.get_ocaml_interface_basename ~contract_name

let network = mk_network scenario ParameterNetwork

let originator = gen_agent scenario 0

let amount = mk_amount scenario Z.zero

let storage =
  mk_micheline scenario (Mprim { prim = `Unit; args = []; annots = [] })
(* Named [interface_name; "initial_blockchain_storage"] *)

let fid = open_fork scenario

let branch1 = add_branch fid

let kt1 =
  mk_deploy ~scenario:branch1 ~amount ~from:originator ~network ~storage
    ~contract_name:(Factori_utils.show_sanitized_name contract_name)

let _call =
  mk_call ~scenario:branch1 ~amount ~from:originator ~entrypoint:"play" ~kt1
    ~network ~contract_name:"rps" ~param:storage

let branch2 = add_branch fid

let _ =
  mk_deploy ~scenario:branch2 ~amount ~from:originator ~network ~storage
    ~contract_name:(Factori_utils.show_sanitized_name contract_name)

let forkfork = open_fork branch2

let branch21 = add_branch forkfork

let _ =
  mk_deploy ~scenario:branch21 ~amount ~from:originator ~network ~storage
    ~contract_name:"toto"

let branch22 = add_branch forkfork

let _ =
  mk_deploy ~scenario:branch22 ~amount ~from:originator ~network ~storage
    ~contract_name:"toto"

let _ = close_fork forkfork

let _ = close_fork fid

let _ = finish_scenario scenario

let _ =
  List.iter
    (fun context -> Format.eprintf "%a" Ast.pp_ast_context context)
    [get_ast scenario]

let new_context_from_ast = AstToContextInstance.compile (get_ast scenario)

(* let contexts = List.concat @@ List.map (Test_scenario.duplicate 3) (Test_scenario.duplicate 2 dummy_scenario_context) *)

let _ = Format.eprintf "Context:\n\n\n"

let _ =
  List.iter
    (fun context -> Format.eprintf "%a" Context.print_context context)
    context_from_ast

let _ = Format.eprintf "Linearized sequences:\n\n\n"

(* let _ = Format.eprintf "%a" print_sequences (linearize
 *    dummy_scenario_context) *)

let quick_deploy_ast =
  AstToContextInstance.compile
    (quick_deploy (Factori_utils.sanitized_of_str "test_contract") ())

let _ =
  Format.printf "%a"
    (Scenario_to_ocaml.pp_scenario_from_sequences_to_ocaml ~funding:true)
    (List.concat (List.map Context.context_to_sequences context_from_ast))

let _ =
  Format.printf "%a"
    (Scenario_to_ocaml.pp_scenario_from_sequences_to_ocaml ~funding:true)
    (List.concat (List.map Context.context_to_sequences new_context_from_ast))

(* let _ =
 *   Format.printf "%a"
 *     Scenario_to_typescript.pp_scenario_from_sequences_to_typescript
 *     (List.concat (List.map Context.linearize new_context_from_ast)) *)
