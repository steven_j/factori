open Context
open Format
open Context
open Scenario_value

let get_addr_balances ~show_uv (ss : sequence list) =
  let addr_amount_map =
    let addrmap = AddrBalances.empty in
    List.fold_right
      (fun s addrmap ->
        ValueBalances.fold
          (fun k v newmap ->
            let k =
              asprintf "%a"
                (fun ppf u ->
                  match u with
                  | Unary u ->
                    fprintf ppf "%s" (show_uv s.seq_names s.seq_universe u)
                  | _ -> ())
                k in
            let vnew =
              asprintf "%s"
                (Z.to_string
                   (List.fold_right
                      (fun u total ->
                        match u with
                        | Unary (Amount_uv amount) -> Z.add amount total
                        | _ -> total)
                      v Z.zero)) in

            match AddrBalances.find_opt k newmap with
            | None -> AddrBalances.add k vnew newmap
            | Some v -> AddrBalances.add k (sprintf "%s + %s" v vnew) newmap)
          (Context.collect_paying_addresses s)
          addrmap)
      ss addrmap in
  addr_amount_map
