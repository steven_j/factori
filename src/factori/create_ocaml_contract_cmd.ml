open Factori_command
open Options
open Factori_errors

let ( // ) = Filename.concat

(** This helps the user create and develop a contract using
   https://gitlab.com/functori/dev/mligo. This feature will probably
   be moved to a separate package eventually, as Factori aims to
   manipulate Michelson contracts. *)
let create_ocaml_contract =
  subcommand ~name:"create ocaml contract"
    ~args:[dir_anonymous_option; contract_name_option]
    ~doc:
      "Create a new OCaml contract using \
       https://gitlab.com/functori/dev/mligo. It will be compiled to mligo and \
       then to Michelson." (fun () ->
      try
        match !contract_name with
        | None -> raise NoContractNameProvided
        | Some contract_name ->
          File_structure.create_ml_contract ~dir:!dir ~overwrite:!overwrite
            ~contract_name
      with e -> handle_exception e)
