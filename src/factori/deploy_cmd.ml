open Factori_command
open Factori_options
open Factori_errors
open Factori_config
open Factori_utils
open Format

type storage_type =
  | Random
  | Blockchain

let storage_type_of_str = function
  | "random" | "Random" -> Random
  | "blockchain" | "Blockchain" -> Blockchain
  | s -> raise (UnknownStorageType s)

let check_language_selection () =
  match (!ocaml, !typescript) with
  | true, true -> raise TooManyLanguages
  | false, false -> raise NoLanguageProvided
  | true, false -> OCaml
  | false, true -> Typescript

(* OCaml *)

let blockchain_ocaml_storage = sprintf "initial_blockchain_storage"

(* TODO: get storage_name in case storage isn't called storage *)
let random_ocaml_storage = sprintf "(storage_generator ())"

let storage_ocaml ~storage_type =
  match storage_type with
  | Random -> random_ocaml_storage
  | Blockchain -> blockchain_ocaml_storage

let scenario_ocaml ~contract_name ~network ~from ~storage_type ppf () =
  let get_network_node network = sprintf "Blockchain.%s_node" network in
  let get_from from = sprintf "Blockchain.%s_flextesa" from in
  fprintf ppf
    {|
    open %s_ocaml_interface
    open Tzfunc.Rp
    let _ = Tzfunc.Node.set_silent true
|}
    (String.capitalize_ascii contract_name) ;

  fprintf ppf
    {|let main () =
     let>? kt1,_op_hash =
       deploy
         ~node:%s
         ~name:"%s"
         ~from:%s
         ~amount:100000L
         %s in

  Format.printf "KT1: %%s@." kt1;
  Lwt.return_ok ()

  let _ =
    Lwt_main.run (main ())
  |}
    (get_network_node network) contract_name (get_from from)
    (storage_ocaml ~storage_type)

let dune_file_scenario ~module_name contract_name =
  let contract = get_contract contract_name in
  Artifacts.scenarios_dunefile ~module_name [contract]

let deploy_ocaml ~storage_type ~contract_name ~network ~from () =
  let dir = Factori_config.get_dir () in
  let scenario_file =
    asprintf "%a"
      (scenario_ocaml ~network ~from ~contract_name ~storage_type)
      () in
  let dune_scenario_file = dune_file_scenario (sanitized_of_str contract_name) in
  let ocaml_deploy_dir = get_ocaml_deploy_dir ~dir in
  make_dir ocaml_deploy_dir ;
  (* Write the dune file allowing for the scenario to be seen by the compiler *)
  let deploy_name = sprintf "deploy_scenario_%s" contract_name in
  Ocaml_dune.write_dune_file ~with_readable_backup:true
    ~dune_file:(dune_scenario_file ~module_name:deploy_name)
    ~path:(ocaml_deploy_dir // "dune")
    () ;
  (* Write the actual scenario file *)
  let scenario_path = get_ocaml_deploy_dir ~dir // sprintf "%s.ml" deploy_name in
  write_file scenario_path scenario_file ;
  let cmd =
    sprintf "cd %s && make ocaml && dune exec ./src/%s/%s.exe" dir
      ocaml_deploy_dir_name deploy_name in
  let _ = Sys.command cmd in
  ()
(* Typescript *)

let blockchain_typescript_storage = sprintf "initial_blockchain_storage"

let random_typescript_storage = sprintf "storage_generator()"

let storage_typescript ~storage_type =
  match storage_type with
  | Random -> random_typescript_storage
  | Blockchain -> blockchain_typescript_storage

let scenario_typescript ~contract_name ~network ~from ~storage_type ppf () =
  let get_network_config network = sprintf "functolib.%s_config" network in
  let get_from from = sprintf "functolib.%s_flextesa" from in
  fprintf ppf
    {|
import {
    TezosToolkit,
    MichelsonMap,
  } from "@taquito/taquito";
import { MichelsonV1Expression } from "@taquito/rpc"
import * as %s from "./%s_interface";
import * as functolib from "./functolib";
const config = %s;
const tezosKit = new TezosToolkit(config.node_addr);
|}
    contract_name contract_name
    (get_network_config network) ;

  fprintf ppf
    {|
async function main(tezosKit : TezosToolkit){
    functolib.setSigner(tezosKit,%s.sk);
    let storage = %s.%s;
    let kt1 = await %s.deploy_%s(tezosKit,storage,config)
}

main(tezosKit)
  |}
    (get_from from) contract_name
    (storage_typescript ~storage_type)
    contract_name contract_name

let deploy_typescript ~storage_type ~contract_name ~network ~from () =
  let dir = Factori_config.get_dir () in
  let scenario_file =
    asprintf "%a"
      (scenario_typescript ~contract_name ~network ~from ~storage_type)
      () in
  let deploy_dir = Factori_config.get_typescript_interface_dir_src ~dir () in
  let scenario_name_ts = sprintf "deploy_scenario_%s.ts" contract_name in
  let scenario_name_js = sprintf "deploy_scenario_%s.js" contract_name in
  let full_deploy_path = deploy_dir // scenario_name_ts in
  write_file full_deploy_path scenario_file ;
  let cmd =
    sprintf "cd %s && make ts-deps && make ts && node src/ts-sdk/dist/%s" dir
      scenario_name_js in
  eprintf "[cmd]%s@." cmd ;
  let _ = Sys.command cmd in
  ()

let deploy_command =
  subcommand ~name:"deploy"
    ~args:
      [
        contract_name_anonymous;
        network_option;
        ocaml_language_option;
        typescript_language_option;
        storage_option;
      ] ~doc:"Quickly deploy one of your imported contracts." (fun () ->
      try
        let language = check_language_selection () in
        let network = !network in
        let from = !sender in
        let contract_name =
          match !contract_name with
          | None -> raise NoContractNameProvided
          | Some c -> c in
        let storage_type = storage_type_of_str !storage_type in
        match language with
        | OCaml -> deploy_ocaml ~storage_type ~network ~from ~contract_name ()
        | Typescript ->
          deploy_typescript ~storage_type ~network ~from ~contract_name ()
      with e -> handle_exception e)
