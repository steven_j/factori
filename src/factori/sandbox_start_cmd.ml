open Factori_command

let sandbox_script block_time =
  Format.sprintf
    {|#!/bin/sh

image=oxheadalpha/flextesa:latest
script=jakartabox
docker run --rm --name \
  factori-sandbox \
  --detach \
  -p 20000:20000 \
  -e block_time=%d \
  -e flextesa_node_cors_origin='*' \
  "$image" "$script" \
  start|}
    block_time

let sandbox_start =
  subcommand ~name:"sandbox start" ~args:[Factori_options.block_time_option]
    ~doc:
      "Start a Flextesa sandbox. You will need docker installed as well as \
       proper permissions (see for instance \
       https://docs.docker.com/engine/install/linux-postinstall/)" (fun () ->
      let cmd =
        Factori_utils.command_if_exists
          ~error_msg:"Please install docker first." ~cmd_name:"docker"
          (sandbox_script !Factori_options.block_interval) in
      let _ = Sys.command cmd in
      ())
