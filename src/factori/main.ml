open Ezcmd.V2
open Factori_options

let ( // ) = concat

exception Error of string

module FACTORI = struct
  let command = "factori"

  let about = "factori COMMAND COMMAND-OPTIONS"

  let set_verbosity n = verbosity := n

  let get_verbosity () = !verbosity

  let backtrace_var = Some "FACTORI_BACKTRACE"

  let usage =
    "Manage and interact with Tezos smart contracts in multiple programming \
     languages"

  let version = Version.version ^ "+" ^ Version.commit

  exception Error = Error
end

module MAIN = EZCMD.MAKE (FACTORI)

(* let speclist = [(["-v"], Arg.Set verbose, "Output debug information");] *)

(** Options common to all commands  *)
let common_args = [overwrite_option (* config_dir_option; *)]

let commands =
  [
    Init_cmd.cmd_init;
    (* CreateOCamlContract.create_ocaml_contract; *)
    (*For a future release *)
    Import_kt1_cmd.import_kt1;
    Import_michelson_cmd.import_michelson;
    (* Build_interface_cmd.build_interface; *)
    Rename_variables_cmd.rename_variables;
    Remove_contract_cmd.remove_contract;
    Sandbox_start_cmd.sandbox_start;
    Sandbox_stop_cmd.sandbox_stop;
    Deploy_cmd.deploy_command;
    Deploy_clean_cmd.deploy_clean_command;
  ]

let main () =
  Tzfunc.Node.set_silent true ;
  MAIN.main commands ~common_args ~argv:Sys.argv

let _ = main ()
