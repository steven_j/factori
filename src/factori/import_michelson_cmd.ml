open Factori_command
open Factori_options
open Factori_errors

let import_michelson =
  subcommand ~name:"import michelson"
    ~args:
      [
        dir_anonymous_option;
        contract_name_option;
        ocaml_language_option;
        typescript_language_option;
        field_prefixes_option;
        michelson_file_anonymous;
        library_option;
        crawlori_option;
        crawlori_db_name_option;
        web_option;
        project_name_option;
      ]
    ~doc:
      "Import a contract using its Michelson compiled form. It can be in \
       either a Json or normal Michelson format." (fun () ->
      language_shenanigans () ;
      try
        (* First initialize directory structure if it is not already there
            *)
        if not (File_structure.is_initialized_dir ()) then Init_cmd.init_fun () ;
        let db_name = db_name () in
        let contract_name =
          match (!contract_name, !michelson_file) with
          | None, Some michelson_file ->
            Filename.basename (Filename.remove_extension michelson_file)
          | Some contract_name, Some _michelson_file -> contract_name
          | _, _ -> raise NoMichelsonFileProvided in
        Factori_utils.try_overflow ~msg:"import_from_michelson" (fun () ->
            let _ =
              Import_kt1.import_from_michelson ~library:!library_mode
                ~ctrct_name:contract_name ~crawlori:!crawlori ~db_name () in
            Format.eprintf "Successfully imported KT1.\n%!")
      with e -> handle_exception e)
