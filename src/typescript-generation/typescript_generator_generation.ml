open Format
open Factori_utils
open Types

let auxiliary_functions ppf () =
  fprintf ppf
    "\n\
     function random (mn : number,mx : number) {\n\
    \    let r = Math.random() * (mx - mn) + mn;\n\
    \    let res = (Math.floor(r));\n\
    \    return res\n\
     }\n\n\
     export function chooseFrom<T>(l : T[]){\n\
    \    var list = l;\n\
    \    var n = list.length;\n\
    \    var index = random(0,n);\n\
    \    var chosenFun = list[index];\n\
    \    return chosenFun;\n\
     }\n\n\n\
     function int64_generator() : number {\n\
    \    return random(0,10);\n\
     }\n\n\
     export function make_list(f : any,k : number) {\n\
    \    var res = new Array(k);\n\
    \    for(var i = 0; i < res.length; i++){\n\
     \tres[i] = f ();\n\
    \    }\n\
    \    return function (){\n\
     \treturn res;\n\
    \    }\n\
     }\n\n"

let all_base =
  [
    Never;
    String;
    Nat;
    Int;
    Byte;
    Address;
    Signature;
    Unit;
    Bool;
    Timestamp;
    Keyhash;
    Key;
    Mutez;
    Operation;
    Chain_id;
    Sapling_state;
    Sapling_transaction_deprecated;
  ]

let all_unary = [Set; List; Option; Ticket]

let all_binary = [Map; BigMap; Lambda]

let generate_all_kinds tuple_size_min tuple_size_max =
  [AContract]
  @ List.map (fun b -> ABase b) all_base
  @ List.map (fun b -> AUnary b) all_unary
  @ List.map (fun b -> ABinary b) all_binary
  @ List.map
      (fun i -> ATuple i)
      (list_integers ~from:tuple_size_min tuple_size_max)

let build_generator ppf kind =
  match kind with
  | ABase b -> (
    match b with
    | Bool ->
      fprintf ppf
        "function bool_generator() : bool{\n\
        \        var res = chooseFrom([true,false]);\n\
        \        return res;\n\
        \        }"
    | Int ->
      fprintf ppf
        "function int_generator() : number {\n\
        \        return random(0,10);\n\
        \        }" ;
      fprintf ppf
        "\n\
         export function big_int_generator() : bigint {\n\
        \        return BigInt(random(0,10));\n\
        \        }"
    | Mutez ->
      fprintf ppf
        "function tez_generator() : tez {\n\
        \        return BigInt(int64_generator());\n\
        \        }"
    | Nat ->
      fprintf ppf
        "function nat_generator() : nat {\n\
        \        return BigInt(int64_generator());\n\
        \        }"
    | Timestamp ->
      fprintf ppf
        "function timestamp_generator() : timestamp{\n\
        \        return '2022-12-01T10:01:00+01:00';\n\
        \        }"
    | Never ->
      fprintf ppf
        "function never_generator() : never{\n\
        \        throw \"Never gonna generate a `never`\"\n\
        \        }"
    | String ->
      fprintf ppf
        "function string_generator () : string{return \"Very like a whale\"}"
    | Byte ->
      fprintf ppf
        "function bytes_generator(){\n        return '0xff';\n        }"
    | Address ->
      fprintf ppf
        "function address_generator(){\n\
        \        return 'tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb';\n\
        \        }"
    | Unit ->
      fprintf ppf "function unit_generator(){\n        return null\n        }"
    | Keyhash
    | Chain_id
    | Key
    | Signature
    | Operation
    | Sapling_state
    | Sapling_transaction_deprecated ->
      let typename = str_of_base b in
      fprintf ppf
        "function %s_generator(){\n        return string_generator()\n        }"
        typename)
  | AContract ->
    fprintf ppf "function contract_generator(){\n        return unit_micheline}"
  | AUnary u -> (
    match u with
    | List ->
      fprintf ppf
        "function list_generator(f : any){\n\
        \         var res =\n\
         \t make_list(\n\
         \t f,\n\
         \t int_generator ()) ();\n\
        \         return function (){\n\
         \t return res;\n\
        \    }\n\
         }\n"
    | Set ->
      fprintf ppf
        "function set_generator(f : any){\n\
        \         return list_generator(f)\n\
        \         }"
    | Option ->
      fprintf ppf
        "function option_generator(f : any){\n\
        \            var optres = f();\n\
        \            return function(){\n\
         \t    return chooseFrom([function(){return null},function(){return \
         optres}]) ();\n\
        \            }\n\
        \            }"
    | Ticket ->
      fprintf ppf
        "function ticket_generator<T>(f : () => T){\n\
        \            return function() : ticket<T>{\n\
        \            var res = f();\n\
         \t    return {kind : \"ticket\", value : res};\n\
        \            }\n\
        \            }")
  | ABinary b -> (
    match b with
    | Map ->
      fprintf ppf
        "function map_generator(f:any,g:any){\n\
        \                      return function(){\n\
        \                        return []\n\
        \                      }\n\
        \                      }"
    | BigMap ->
      fprintf ppf
        "function big_map_generator<K,V>(f: () => K,g : () => V){\n\
         return function():big_map<K,V>{\n\
         return {kind : 'literal', value: []}\n\
         }\n\
         }"
    | Lambda ->
      fprintf ppf
        "function lambda_generator(f : any,g : any){\n\
        \                      return function(){\n\
        \                        return ({ from : unit_micheline,\n\
        \                          to_ : unit_micheline,\n\
        \                          body : {prim : 'FAILWITH'}} as lambda_params)\n\
        \                      }\n\
        \                      }")
  | ATuple k ->
    fprintf ppf
      "function tuple%d_generator<%a>(%a){\n\
      \                    return function() : [%a]{\n\
      \                  return([%a])\n\
      \                  }\n\
      \                  }" k
      (pp_print_list ~pp_sep:(tag ",") (fun ppf i -> fprintf ppf "T%d" i))
      (list_integers ~from:1 k)
      (pp_print_list ~pp_sep:(tag ",") (fun ppf i ->
           fprintf ppf "f%d : () => T%d" i i))
      (list_integers ~from:1 k)
      (pp_print_list ~pp_sep:(tag ",") (fun ppf i -> fprintf ppf "T%d" i))
      (list_integers ~from:1 k)
      (pp_print_list ~pp_sep:(tag ",") (fun ppf i -> fprintf ppf "f%d()" i))
      (list_integers ~from:1 k)

(* | _ -> failwith "TODO" *)
