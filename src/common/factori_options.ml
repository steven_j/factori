open Ezcmd.V2
open EZCMD.TYPES

(* let verbose = ref false *)

(* Verbosity will have to coexist with verbose for a while, this is
   for compatibility with EZCMD *)
let verbosity = ref 0

let dir = ref (None : string option)

let project_name = ref (None : string option)

let block_interval = ref 1

let overwrite = ref false

let purge = ref false

let contract_name = ref (None : string option)

(* Determines whether record fields are prefixed by the entrypoint
   name to disambiguate when several entrypoints have the same
   parameter names *)
let field_prefixes = ref false

(* Determines whether we are only generating a bare bones library *)
let library_mode = ref false

let web_mode = ref false

let concat = Filename.concat

let default_config_dir () =
  let home = Unix.getenv "HOME" in
  concat home ".factori/"

let config_file = ref (Some (default_config_dir ()))

let kt1 = ref (None : string option)

let michelson_file = ref (None : string option)

let network = ref "mainnet"

let sender = ref "alice"

let storage_type = ref "blockchain"

(* Interface languages *)
let ocaml = ref false

(* Factori can generate a crawlori plugin (this can only be use
   with ocaml flag) *)
let crawlori = ref false

let dipdup = ref false

let crawlori_db_name = ref None

let typescript = ref false
(* By default, Factori generates a
   typescript interface only *)

let language_shenanigans () =
  if !verbosity > 0 then
    Format.eprintf "ocaml: %b; typescript: %b@.\n" !ocaml !typescript ;
  match (!ocaml, !typescript) with
  | false, false ->
    if !verbosity > 0 then
      Format.eprintf "No language selected, defaulting to Typescript\n%!" ;
    typescript := true
  | _, _ ->
    () ;
    if !crawlori then ocaml := true ;
    if !web_mode then typescript := true

let db_name () =
  Option.value
    ~default:
      (Option.fold ~none:"crawlori_db_generic_name"
         ~some:(fun s ->
           if s = "." || s = ".." || s = "~" then
             "crawlori_db_generic_name"
           else
             Filename.basename s)
         !dir)
    !crawlori_db_name

let overwrite_option =
  ( ["force"; "f"],
    Arg.Bool (fun b -> overwrite := b),
    EZCMD.info ~docv:"FORCE"
      "If set to true, Factori will overwrite the input folder for commands \
       that write to files. Defaults to false." )

(* let verbose_option =
 *   ( ["verbose"],
 *     Arg.Bool (fun b -> verbose := b),
 *     EZCMD.info
 *       ~docv:"VERBOSE"
 *       "If true, Factori will print messages to explicit what it is doing on \
 *        the standard error output. Defaults to false." ) *)

(* let config_dir_option =
 *   [ "config-dir"], Arg.String (fun s -> config_file := Some s),
 *   EZCMD.info ~docv:"CONFIG_FILE"
 *     "If different from ~/.factori, this is where the Factori \
 *      configuration file is located." *)

let dir_anonymous_option =
  ( [],
    Arg.Anon (0, fun s -> dir := Some s),
    EZCMD.info ~docv:"DIRECTORY"
      "Directory where the Factori project is stored." )

let ocaml_language_option =
  ( ["ocaml"],
    Arg.Set ocaml,
    EZCMD.info ~docv:"OCAML"
      "If activated, an OCaml interface for the smart contract(s) will be \
       generated." )

let typescript_language_option =
  ( ["typescript"],
    Arg.Set typescript,
    EZCMD.info ~docv:"TYPESCRIPT"
      "If activated, a Typescript interface for the smart contract(s) will be \
       generated" )

let contract_name_option =
  ( ["name"],
    Arg.String (fun name -> contract_name := Some name),
    EZCMD.info ~docv:"CONTRACT_NAME"
      "Provide the contract name for which you would like to perform the \
       operation." )

let contract_name_anonymous =
  ( [],
    Arg.Anon (0, fun name -> contract_name := Some name),
    EZCMD.info ~docv:"CONTRACT_NAME"
      "Provide a contract name; by default, the file name or KT1 will be used \
       to create a new name." )

let michelson_file_anonymous =
  ( [],
    Arg.Anon (1, fun name -> michelson_file := Some name),
    EZCMD.info ~docv:"MICHELSON_FILE"
      "The Michelson file you would like to use for this operation." )

let kt1_anonymous =
  ( [],
    Arg.Anon (1, fun s -> kt1 := Some s),
    EZCMD.info ~docv:"KT1"
      "Provide an on-chain KT1 you wish to download and import." )

let purge_option =
  ( ["purge"],
    Arg.Bool (fun b -> purge := b),
    EZCMD.info ~docv:"purge"
      "Actually remove the source files related to the contract you wish to \
       remove. Defaults to false." )

let network_option =
  ( ["network"],
    Arg.String (fun s -> network := s),
    EZCMD.info ~docv:"network"
      "Specify on which network (mainnet,ithacanet,etc...) you would like to \
       perform the operation." )

let field_prefixes_option =
  ( ["field_prefixes"],
    Arg.Set_bool field_prefixes,
    EZCMD.info ~docv:"field_prefixes"
      "Determines whether record fields are prefixed by the entrypoint\n\
      \       name to disambiguate when several entrypoints have the same\n\
      \       parameter names" )

let library_option =
  ( ["library"],
    Arg.Bool (fun b -> library_mode := b),
    EZCMD.info ~docv:"library"
      "Determines whether the OCaml code is generated only as a library." )

let crawlori_option =
  ( ["crawlori"],
    Arg.Bool (fun b -> crawlori := b),
    EZCMD.info ~docv:"crawlori"
      "If activated, a crawlori plugin that can crawl the smart contract(s) \
       will be generated (needs ocaml option)." )

let crawlori_db_name_option =
  ( ["db-name"],
    Arg.String (fun s -> crawlori_db_name := Some s),
    EZCMD.info ~docv:"db-name"
      "This is the name for the psql database (default is project directory \
       basename)." )

let web_option =
  ( ["web"],
    Arg.Bool (fun b -> web_mode := b),
    EZCMD.info ~docv:"web"
      "Determines whether the Web page of the smart contract is generated." )

let dipdup_option =
  ( ["dipdup"],
    Arg.Bool (fun b -> dipdup := b),
    EZCMD.info ~docv:"dipdup"
      "If activated, necessary files to use didup will be generated." )

let from_option =
  ( ["from"],
    Arg.String (fun s -> sender := s),
    EZCMD.info ~docv:"from"
      "Agent who will do the current operation. Note that if the network is \
       mainnet, this will most likely fail" )

let storage_option =
  ( ["storage"],
    Arg.String (fun s -> storage_type := s),
    EZCMD.info ~docv:"storage"
      "Storage type used for the `factori deploy` command." )

let project_name_option =
  ( ["project_name"],
    Arg.String (fun name -> project_name := Some name),
    EZCMD.info ~docv:"PROJECT_NAME"
      "Provide a project name. This will be used e.g. for the opam file name" )

let block_time_option =
  ( ["block_interval"],
    Arg.Int (fun i -> block_interval := i),
    EZCMD.info ~docv:"BLOCK_INTERVAL"
      "Provide a block time to start the flextesa sandbox with" )
