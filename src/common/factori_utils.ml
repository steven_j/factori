open Factori_errors
open Format
open Tzfunc.Proto

let list_integers ?(from = 1) n =
  let rec aux res = function
    | n when n = from - 1 -> res
    | n -> aux (n :: res) (n - 1) in
  aux [] n

(** shortcut for the separator in pp_print_list  *)
let tag s ppf _ = fprintf ppf s

(* Utilities for sanitizing names while still keeping around the
   original names of variables, entrypoint names, etc... *)

type accessors = string list [@@deriving show, encoding]

type sanitized_name = {
  original : string;
  sanitized : string;
  accessors : accessors;
}
[@@deriving show, encoding]

let get_original sn = sn.original

let get_sanitized sn = sn.sanitized

let get_accessors sn = sn.accessors

let add_accessors sn l = { sn with accessors = sn.accessors @ l }

let add_accessor sn x = add_accessors sn [x]

let make_sanitized ~original ~sanitized ~accessors =
  { original; sanitized; accessors }

let sanitized_of_str ?(accessors = []) sn =
  { original = sn; sanitized = sn; accessors }

(** This function tells whether the sanitized values are equal, it
   does not concern itself with the original values (though in all
   logic they *should* be the same *)
let eq_sanitized sn1 sn2 =
  get_sanitized sn1 = get_sanitized sn2 && get_accessors sn1 = get_accessors sn2

let sanitize_aux s =
  let open Str in
  let temp = global_replace (regexp_string "-") "_" s in
  let temp = global_replace (regexp_string " ") "" temp in
  String.lowercase_ascii temp

let sanitize_basename contract_name =
  {
    original = contract_name;
    sanitized = sanitize_aux contract_name;
    accessors = [];
  }

let print_sanitized_name ppf sn =
  let open Format in
  fprintf ppf "%s%s" (get_sanitized sn)
    (match sn.accessors with
    | [] -> ""
    | l ->
      asprintf "%a"
        (pp_print_list ~pp_sep:(tag "_") (fun ppf x -> fprintf ppf "%s" x))
        l)

let show_sanitized_name sn = Format.asprintf "%a" print_sanitized_name sn

(* *)

(** remove an element from a list, as many times at it is present *)
let remove ~eq x l = List.filter (fun y -> not (eq y x)) l

let is_dir dir = try Sys.is_directory dir with _ -> false

let try_overflow ~msg f =
  try f ()
  with Stack_overflow ->
    Format.eprintf "Stack overflow in %s\n%!" msg ;
    raise Stack_overflow

let output_if_verbose ?(level = 1) ~verbose s =
  if verbose >= level then
    Format.eprintf "[verbosity %d>= (level) %d] %s\n" verbose level s

let get_fresh_id =
  let counter = ref 0 in
  fun () ->
    let res = !counter in
    counter := !counter + 1 ;
    res

let output_verbose ?(id = -1) ?(level = 1) s =
  let s =
    if id = -1 then
      s
    else
      sprintf "[id:%d]%s" id s in
  let verbose = !Factori_options.verbosity in
  output_if_verbose ~level ~verbose s

let write_file filename str =
  let oc = open_out filename in
  output_string oc str ;
  close_out oc

let write_file filename str =
  let dir = Filename.dirname filename in
  output_verbose @@ Format.sprintf "Writing to directory: %s@.\n" dir ;
  if is_dir dir then
    write_file filename str
  else
    raise (NotADirectory (dir, "write_file"))

let contains s1 s2 =
  let re = Str.regexp_string s2 in
  try
    ignore (Str.search_forward re s1 0) ;
    true
  with Not_found -> false

let rec make_dir dir =
  if Sys.file_exists dir && Sys.is_directory dir then
    ()
  else if Sys.file_exists dir && not (Sys.is_directory dir) then
    raise (NotADirectory (dir, "make_dir"))
  else begin
    let dirname = Filename.dirname dir in
    make_dir dirname ;
    let basename = Filename.basename dir in
    match basename with
    | "." | ".." -> ()
    | _ -> (
      try Unix.mkdir dir 0o755 with Unix.Unix_error (EEXIST, _, _) -> ())
  end

let command_if_exists ?(error_msg = "command does not exist") ~cmd_name
    cmd_content =
  Format.sprintf
    {|
if ! command -v %s \&> /dev/null
then
    echo "%s"
    exit
else
    %s
fi
|}
    cmd_name error_msg cmd_content

let rec read ic buffer bytes =
  let nbytes = input ic bytes 0 1024 in
  if nbytes > 0 then begin
    Buffer.add_subbytes buffer bytes 0 nbytes ;
    read ic buffer bytes
  end

let read_file filename =
  let ic = open_in filename in
  let bytes = Bytes.create 1024 in
  let buf = Buffer.create 1024 in
  read ic buf bytes ;
  close_in ic ;
  Buffer.contents buf

let rec copy ic oc b =
  let read = input ic b 0 1024 in
  if read = 0 then
    ()
  else begin
    output oc b 0 read ;
    copy ic oc b
  end

let copy_file src dst =
  let ic = open_in_bin src in
  let oc = open_out_bin dst in
  try
    let b = Bytes.create 1024 in
    copy ic oc b
  with exn ->
    close_in ic ;
    close_out oc ;
    raise exn

let append_to_file ?(check_already_present = true) ~filepath ~content () =
  if not (Sys.file_exists filepath) then
    write_file filepath content
  else
    let cur_file_content = read_file filepath in
    if check_already_present && contains cur_file_content content then
      ()
    else
      let result = Format.sprintf "%s%s" cur_file_content content in
      write_file filepath result

let remove_from_file ~filepath ~content () =
  if not (Sys.file_exists filepath) then
    Format.eprintf
      "(Soft) Warning: file %s does not exist, it cannot be removed" filepath
  else
    let open Str in
    let new_file_content =
      global_replace (regexp_string content) "" (read_file filepath) in
    write_file filepath new_file_content

(* Input *)

let general_input ~default s =
  match s with
  | "" -> default
  | s -> s

let general_input_opt ~default s =
  match s with
  | "" -> default
  | s -> Some s

let yes_no ~default s =
  match String.uncapitalize_ascii s with
  | "y" | "yes" -> true
  | _ -> default

(* Serialization/Deserialization *)
let serialize_value ?(pretty = true) ~enc v =
  Ezjsonm.value_to_string ~minify:(not pretty) @@ Json_encoding.construct enc v

let serialize_file ~filename ~enc v =
  write_file filename @@ serialize_value ~enc v

let deserialize_value ~enc s =
  try
    let js_value = Ezjsonm.value_from_string s in
    try Json_encoding.destruct enc js_value
    with _e ->
      raise (CouldNotDestruct (sprintf "%s" (Ezjsonm.value_to_string js_value)))
  with _e -> raise (CouldNotDestruct (sprintf "%s" s))

let deserialize_file ~filename ~enc =
  try_overflow
    (fun () -> deserialize_value ~enc @@ read_file filename)
    ~msg:(Format.sprintf "deserialize filename:%s" filename)

type delimiter =
  | Brackets
  | Parens
  | Curly
  | Nothing

let opening = function
  | Brackets -> "["
  | Parens -> "("
  | Curly -> "{"
  | Nothing -> ""

let closing = function
  | Brackets -> "]"
  | Parens -> ")"
  | Curly -> "}"
  | Nothing -> ""

let surround d f ppf x = Format.fprintf ppf "%s%a%s" (opening d) f x (closing d)

let comment_ocaml x = Format.sprintf "(*%s*)" x

(* Some common blockchain stuff imported from import_kt1.ml  *)

let unexpected_michelson msg m =
  Format.eprintf "%s\n%!" msg ;
  raise @@ UnexpectedMichelson (EzEncoding.construct micheline_enc.json m)

let get_code_elt p = function
  | Some (Micheline (Mseq l)) ->
    List.find_map
      (function
        | Mprim { prim; args = [arg]; _ } when prim = p -> Some arg
        | _ -> None)
      l
  | _ -> None

let get_storage_from_script_expr (se : script_expr) : micheline option =
  get_code_elt `storage (Some se)

let get_parameter_from_script_expr (se : script_expr) : micheline option =
  get_code_elt `parameter (Some se)

let get_code_from_script_expr (se : script_expr) : micheline option =
  get_code_elt `code (Some se)

let get_name_annots = function
  | [s] when String.length s > 0 && String.get s 0 = '%' ->
    Some (String.sub s 1 (String.length s - 1))
  | _ -> None

let get_entrypoints script =
  output_verbose ~level:2
    (sprintf "Entering get_entrypoints with %s"
       (EzEncoding.construct script_expr_enc.json script)) ;
  let rec aux acc = function
    | Mprim { prim = `or_; args = l; _ } -> (
      let arg1 = List.nth l 0 in
      let arg2 = List.nth l 1 in
      match aux acc arg1 with
      | Error e -> Error e
      | Ok acc -> aux acc arg2)
    | Mprim { annots; _ } as m -> (
      match get_name_annots annots with
      | None -> Ok acc
      | Some name -> Ok ((sanitized_of_str name, m) :: acc))
    | m -> unexpected_michelson "toto\n\n\n" m in
  match get_parameter_from_script_expr script with
  | Some (Mprim { prim = `or_; _ } as m) -> aux [] m
  | Some m -> Ok [(sanitized_of_str "default", m)]
  | None ->
    Format.eprintf "couldn't get code\n%!" ;
    Result.error (Error `unexpected_michelson_value)

(* Get storage from the blockchain from a KT1. The network is chosen
   from the `kt1` ref value in options.ml, which is affected by the
   --network option *)

let get_raw_network = function
  | "mainnet" -> "https://tz.functori.com"
  | "hangzhounet" | "hangzhou" -> "https://hangzhounet.api.tez.ie"
  | "ithacanet" | "ghostnet" | "ithaca" | "ghost" ->
    "https://ithacanet.tezos.marigold.dev"
  | "sandbox" -> "https://localhost:18731"
  | "jakartanet" | "jakarta" -> "https://jakartanet.tezos.marigold.dev"
  | "flextesa" -> "http://localhost:20000"
  | n -> n

let get_default_identity = function
  | "ithacanet" | "ghostnet" | "flextesa" -> "alice_flextesa"
  | "sandbox" -> "bootstrap1"
  | n -> sprintf "No default identity for %s" n

let get_tzkt_api = function
  | "mainnet" -> "https://api.tzkt.io/"
  | "ithacanet" | "ghostnet" -> "https://api.ghostnet.tzkt.io/"
  | "jakartanet" -> "https://api.jakartanet.tzkt.io/"
  | _ -> "<CUSTOM_URL>"

let get_tzkt_datasource_url network =
  match network with
  | "mainnet" -> ("tzkt_mainnet", get_tzkt_api network)
  | "ithacanet" | "ghostnet" -> ("tzkt_testnet", get_tzkt_api network)
  | _ -> ("tzkt_custom", get_tzkt_api network)

let get_base_of_network x = EzAPI.BASE (get_raw_network x)

let get_storage kt1 =
  let open Proto in
  match
    Lwt_main.run
      (Tzfunc.Node.get_account_info
         ~base:(get_base_of_network !Factori_options.network)
         kt1)
  with
  | Error e ->
    let str_err = Tzfunc.Rp.string_of_error e in
    Format.eprintf "[Error in get_storage]: %s\n" str_err ;
    raise (GenericError ("get_storage", str_err))
  | Ok { ac_script = None; _ } ->
    Format.eprintf "[Error in get_storage]: No script for kt1: %s\n" kt1 ;
    raise (NoScript kt1)
  | Ok { ac_script = Some { code = se; _ }; _ } -> (
    let storage = get_storage_from_script_expr (Micheline se) in
    match storage with
    | None -> raise (GenericError ("get_storage", "No storage type"))
    | Some sto -> sto)

let wrap_get_entrypoints_michelson_file se =
  match get_entrypoints se with
  | Error _ ->
    raise
      (GenericError
         ( "import_from_michelson",
           "Couldn't get entrypoints from Michelson file" ))
  | Ok entrypoints -> entrypoints

let wrap_get_entrypoints_from_kt1 ~network ~kt1 () =
  match
    Lwt_main.run
      (Tzfunc.Node.get_entrypoints ~base:(get_base_of_network network) kt1)
  with
  | Error e ->
    raise
      (GenericError ("Error in get_entrypoints", Tzfunc.Rp.string_of_error e))
  | Ok entrypoints -> entrypoints

let get_backup_file_name ~filename =
  Format.sprintf "%s.json" (Filename.remove_extension filename)

let camel_to_snake s =
  let n = String.length s in
  let b = Bytes.create (2 * n) in
  let rec aux i j =
    if i = n then
      j
    else
      let c = String.get s i in
      let code = Char.code c in
      if code >= 65 && code <= 90 then (
        Bytes.set b j '_' ;
        Bytes.set b (j + 1) (Char.chr (code + 32)) ;
        aux (i + 1) (j + 2)
      ) else (
        Bytes.set b j c ;
        aux (i + 1) (j + 1)
      ) in
  let m = aux 0 0 in
  Bytes.(to_string @@ sub b 0 m)

let snake_to_camel s =
  let n = String.length s in
  let b = Bytes.create n in
  let rec aux i j =
    if i = n then
      j
    else
      let c = String.get s i in
      if c = '_' && i <> n - 1 then (
        let c = String.get s (i + 1) in
        let cc = Char.code c in
        if cc >= 97 && cc <= 122 then
          Bytes.set b j Char.(chr @@ (code c - 32))
        else
          Bytes.set b j Char.(chr @@ code c) ;
        aux (i + 2) (j + 1)
      ) else (
        Bytes.set b j c ;
        aux (i + 1) (j + 1)
      ) in
  let m = aux 0 0 in
  Bytes.(to_string @@ sub b 0 m)

(* Two utility functions *)
let map3 ?(reference = "") f l1 l2 l3 =
  let l = List.length in
  let n1, n2, n3 = (l l1, l l2, l l3) in
  if not (n1 = n2 && n2 = n3) then
    failwith
      (Format.sprintf
         "[map3][ref %s] Error: lists have different lengths %d,%d and %d@."
         reference n1 n2 n3)
  else
    List.map
      (fun ((x, y), z) -> f x y z)
      (List.map2 (fun x y -> (x, y)) (List.map2 (fun x y -> (x, y)) l1 l2) l3)

let split4 l1234 =
  let l12, l34 = List.split l1234 in
  let l1, l2 = List.split l12 in
  let l3, l4 = List.split l34 in
  (l1, l2, l3, l4)

let print_z_int_ocaml ppf z = fprintf ppf "Z.of_string (\"%s\")" (Z.to_string z)

let print_z_int_typescript ppf z = fprintf ppf "BigInt (\"%s\")" (Z.to_string z)

module Z = struct
  include Z

  let enc = Json_encoding.conv to_string of_string Json_encoding.string
end

module Hex = struct
  type hex = Proto.hex

  let hex_enc = hex_enc.json
end

module Ticket = struct
  type 'a ticket = Ticket of string
end

module Lambda = struct
  type lambda_params = {
    from : micheline;
    to_ : micheline;
    body : micheline;
  }

  type ('a, 'b) lambda = Lambda of lambda_params

  type ex_lambda = CLambda : ('a, 'b) lambda -> ex_lambda

  let lambda_enc (_aenc : 'a encoding) (_benc : 'b encoding) =
    let open Json_encoding in
    (conv
       (function
         | Lambda l -> (l.from, l.to_, l.body))
       (fun (from, to_, body) -> Lambda { from; to_; body })
       (obj3
          (req "from" micheline_enc.json)
          (req "to_" micheline_enc.json)
          (req "body" micheline_enc.json))
      : ('a, 'b) lambda encoding)

  (* let lambda_enc = fun (_aenc : 'a encoding) (_benc : 'b encoding) ->
   *   let open Json_encoding in
   *   (conv (function | Lambda l -> (l.from,l.to_,l.body)) (fun (from,to_,body) -> Lambda {from;to_;body}) (obj3 (req "from" micheline_enc.json) (req "to_" micheline_enc.json) (req "body" micheline_enc.json)) : ('a,'b) lambda encoding ) *)
end
