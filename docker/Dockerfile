FROM functori/opam:debian as compilation

COPY --chown=functori . ./factori
WORKDIR ./factori
ARG CI_PIPELINE_SOURCE
ARG CI_MERGE_REQUEST_TARGET_BRANCH_NAME
ARG CI_COMMIT_BRANCH

RUN sudo apt-get update && sudo apt-get install bash
RUN opam switch create --no-install . ocaml-system && \
eval $(opam env) && \
opam update && \
make build-deps && \
make

FROM debian:latest as target

RUN apt-get update && \
apt-get install -y sudo libcurl4-gnutls-dev tree nodejs npm && \
npm install -g typescript-formatter && \
addgroup --system functori && adduser --system --group --shell /bin/sh functori && \
echo 'functori ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

USER functori
WORKDIR /home/functori
COPY --from=compilation --chown=functori /home/functori/factori/docker/entrypoint.sh /home/functori/factori/_bin/factori.asm /home/functori/

ENTRYPOINT [ "./entrypoint.sh" ]
