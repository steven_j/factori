open Typescript
open Factori_config

module Timeout = struct
  exception Timeout

  let action ~enter ~exit ~do_ =
    enter () ;
    let result =
      try do_ ()
      with e ->
        exit () ;
        raise e in
    exit () ;
    result

  let with_timeout ~timeout ?on_timeout ~do_ =
    let old_handler = ref Sys.Signal_default in
    let old_timeout = ref 0 in
    let on_timeout _sigalrm =
      match on_timeout with
      | None -> exit 42
      | Some f ->
        f () ;
        exit 42 in
    action
      ~enter:(fun () ->
        old_handler := Sys.signal Sys.sigalrm (Sys.Signal_handle on_timeout) ;
        old_timeout := Unix.alarm timeout)
      ~exit:(fun () ->
        ignore (Unix.alarm !old_timeout) ;
        Sys.set_signal Sys.sigalrm !old_handler)
      ~do_
end

let command_log cmd log_file =
  Sys.command @@ Format.sprintf "%s 1>> %s 2>> %s" cmd log_file log_file

let command_timeout_log ~timeout cmd log_file =
  command_log
    (Format.sprintf "timeout --preserve-status %ds %s" timeout cmd)
    log_file

let command cmd = Sys.command @@ Format.sprintf "%s" cmd

let command_timeout ~timeout cmd =
  command @@ Format.sprintf "timeout --preserve-status %ds %s" timeout cmd

let ( // ) = Filename.concat

let pwd = Sys.getcwd ()

let bin = pwd // "_bin"

let factori = bin // "factori.asm"

let ts_tests_path = get_typescript_interface_dir ~dir:(pwd // "test") ()

type input = {
  i_hash : string; [@key "hash"]
  i_kt1 : string; [@key "kt1"]
  i_occurence : int; [@key "occurence"]
}
[@@deriving encoding]

type output = {
  o_hash : string; [@key "hash"]
  o_occurence : int; [@key "occurence"]
  o_kt1 : string; [@key "kt1"]
  o_import_exit_code : int; [@key "import_exit_code"]
  o_ocaml_compilation_exit_code : int; [@key "ocaml_compilation_exit_code"]
  o_typescript_compilation_exit_code : int;
      [@key "typescript_compilation_exit_code"]
  o_test_ocaml_exit_code : int; [@key "test_ocaml_exit_code"]
  o_test_ts_exit_code : int; [@key "test_ts_exit_code"]
  o_import_status : string; [@key "import_status"]
  o_ocaml_compilation_status : string; [@key "ocaml_compilation_status"]
  o_typescript_compilation_status : string;
      [@key "typescript_compilation_status"]
  o_test_ocaml_status : string; [@key "test_ocaml_status"]
  o_test_ts_status : string; [@key "test_ts_status"]
  o_network : string; [@key "network"]
  o_name : string; [@key "name"]
}
[@@deriving encoding]

module Result = struct
  module R = Map.Make (String)

  type t = output R.t

  let add table hash el = R.add hash el table
end

let read_kt1s filename =
  let ic = open_in filename in
  let buf = really_input_string ic (in_channel_length ic) in
  close_in ic ;
  EzEncoding.destruct (Json_encoding.list input_enc) buf
  |> List.sort (fun i1 i2 -> Stdlib.compare i2.i_occurence i1.i_occurence)

let to_status exit_code =
  if exit_code <> 0 then
    "Failed"
  else
    "Succeeded"

let result : Result.t ref = ref Result.R.empty

let to_output_json k = EzEncoding.construct (Json_encoding.list output_enc) k

let output_result filename o_hash o_kt1 o_import_exit_code
    o_ocaml_compilation_exit_code o_typescript_compilation_exit_code
    o_test_ocaml_exit_code o_test_ts_exit_code o_name o_network o_occurence =
  result :=
    Result.add !result o_hash
      {
        o_hash;
        o_occurence;
        o_kt1;
        o_import_exit_code;
        o_ocaml_compilation_exit_code;
        o_typescript_compilation_exit_code;
        o_test_ocaml_exit_code;
        o_test_ts_exit_code;
        o_import_status = to_status o_import_exit_code;
        o_ocaml_compilation_status = to_status o_ocaml_compilation_exit_code;
        o_typescript_compilation_status =
          to_status o_typescript_compilation_exit_code;
        o_test_ocaml_status = to_status o_test_ocaml_exit_code;
        o_test_ts_status = to_status o_test_ts_exit_code;
        o_network;
        o_name;
      } ;
  if o_import_exit_code <> 0 then
    Format.eprintf "%S importation failed with code %d@." o_kt1
      o_import_exit_code
  else
    Format.eprintf "%S importation succeed with code %d@." o_kt1
      o_import_exit_code ;
  if o_ocaml_compilation_exit_code <> 0 then
    Format.eprintf "%S OCaml compilation failed with code %d@." o_kt1
      o_ocaml_compilation_exit_code
  else
    Format.eprintf "%S OCaml compilation succeed with code %d@." o_kt1
      o_ocaml_compilation_exit_code ;
  if o_typescript_compilation_exit_code <> 0 then
    Format.eprintf "%S Typescript compilation failed with code %d@." o_kt1
      o_typescript_compilation_exit_code
  else
    Format.eprintf "%S Typescript compilation succeed with code %d@." o_kt1
      o_typescript_compilation_exit_code ;
  let oc = open_out filename in
  let foc = Format.formatter_of_out_channel oc in
  let x = List.map (fun (_, o) -> o) @@ Result.R.bindings !result in
  Format.fprintf foc "%s" @@ to_output_json x ;
  close_out oc

(* xxxx from test.ml *)
let typescript_imports =
  [
    Import_value
      {
        imported =
          [
            "TezosToolkit";
            "BigMapAbstraction";
            "MichelsonMap";
            "OriginationOperation";
            "OpKind";
            "createTransferOperation";
            "TransferParams";
            "RPCOperation";
            "createRevealOperation";
          ];
        source = "\"@taquito/taquito\"";
      };
    Import_value
      { imported = ["MichelsonV1Expression"]; source = "\"@taquito/rpc\"" };
    Import_value { imported = ["encodeOpHash"]; source = "\"@taquito/utils\"" };
  ]

let ts_preamble =
  {|

const tezosKit_mainnet = new TezosToolkit('https://tz.functori.com/');

|}

let ts_main_function =
  {
    name = "main";
    async = true;
    export = true;
    body = "";
    (* will be filled out progressively *)
    args = [];
  }

let typescript_test_file =
  {
    imports = typescript_imports;
    preamble = ts_preamble;
    functions = [ts_main_function];
    body =
      "let res = main ()\n\
       console.log(\"Battery of Typescript tests successful.\")\n";
  }

let init_tests_folder () =
  Format.eprintf "Creating tests folder...@.\n" ;
  let tests_path = pwd // "test" // "src" // "tests" in
  let ts_tests_path = pwd // "test" // "src" // "ts" in
  Factori_utils.make_dir tests_path ;
  Factori_utils.make_dir ts_tests_path ;
  Ocaml_dune.add_to_dune_file ~file_must_exist:false ~stanza_must_not_exist:true
    ~path:(tests_path // "dune")
    [
      Executable
        {
          name = "tests";
          modules = ["tests"];
          libraries =
            ["factori_types"; "blockchain"; "utils"; "ez_api.icurl_lwt"];
        };
    ] ;
  Format.eprintf "Writing file \"tests.ml\"...@.\n" ;
  Factori_utils.write_file (tests_path // "tests.ml") ""

let mk_log kt1 log_directory log_file =
  log_directory // Format.sprintf "%s_%s.html" kt1 log_file

(* xxxx *)
let import_and_compile_all_kt1s filename result_filename log_directory =
  let kt1s = read_kt1s filename in
  let tests_path = pwd // "test" // "src" // "tests" in
  Factori_utils.append_to_file ~check_already_present:true
    ~filepath:(tests_path // "tests.ml")
    ~content:
      "let _ = Factori_types.set_debug 0\n\
       let _ = Tzfunc.Node.set_silent true\n\n"
    () ;
  (* Main loop *)
  let rec aux ~networks index = function
    | [] -> add_to_body ~fun_name:"main" "return;" typescript_test_file
    | k :: xs ->
      let kt1 = k.i_kt1 in
      let hash = k.i_hash in
      let name = kt1 in
      let occurence = k.i_occurence in
      let network = "mainnet" in
      let cmd_drop = "dropdb test" in
      let cmd =
        Format.sprintf
          "%s import kt1 test %s --network %s --name %s_%d --crawlori --ocaml \
           --typescript --force"
          factori kt1 network name index in
      let _ =
        command_timeout_log ~timeout:150 cmd_drop
          (mk_log kt1 log_directory "drop") in
      let import_exit_code =
        command_timeout_log ~timeout:150 cmd (mk_log kt1 log_directory "import")
      in
      init_tests_folder () ;
      let ocaml_compilation_exit_code =
        if import_exit_code = 0 then
          command_timeout_log ~timeout:150 "make -C test ocaml"
            (mk_log kt1 log_directory "ocaml_compilation")
        else
          -1 in
      let typescript_compilation_exit_code =
        if import_exit_code = 0 then
          command_timeout_log ~timeout:150
            "tsc -p test/src/ts-sdk/tsconfig.json"
            (mk_log kt1 log_directory "typescript_compilation")
        else
          -1 in
      Format.eprintf "End of compilation\n@." ;
      (* OCaml *)
      if ocaml_compilation_exit_code = 0 then begin
        Ocaml_dune.add_library_to_stanza_in_dune_file ~file_must_exist:true
          ~stanza_must_exist:true ~path:(tests_path // "dune")
          ~stanza_name:"tests"
          (Format.sprintf "%s_%d_ocaml_interface" name index) ;
        let storage_download_chunk_ml =
          Format.sprintf
            "let _ = %s_%d_ocaml_interface.test_storage_download ~kt1:\"%s\" \
             ~base:(EzAPI.BASE (\"%s\")) ()\n\
             %!"
            (String.capitalize_ascii name)
            index kt1
            (Factori_utils.get_raw_network network) in
        Factori_utils.append_to_file ~check_already_present:true
          ~filepath:(tests_path // "tests.ml")
          ~content:storage_download_chunk_ml ()
      end ;
      (* Typescript *)
      if typescript_compilation_exit_code = 0 then begin
        let network_url = Factori_utils.get_raw_network network in
        let new_tzkit =
          if not (List.mem network networks) then
            Format.sprintf "const tezosKit_%s = new TezosToolkit('%s');" network
              network_url
          else
            "" in
        let storage_download_chunk_ts =
          Format.sprintf
            {|
  %s
  const contract_%d = await tezosKit_%s.contract.at("%s")
  let storage_%d = await contract_%d.storage() as %s_%d_interface.Storage_type
  //console.log(storage_%d)
  /*let storage%d_reencoded = %s_%d_interface.Storage_type_encode(storage_%d)
  //console.log(storage%d_reencoded)
  let storage%d_redecoded = %s_%d_interface.Storage_type_decode(storage%d_reencoded)*/
 |}
            new_tzkit index network kt1 index index name index index index name
            index index index index name index index in
        let ts_file =
          add_to_body ~fun_name:"main"
            (Format.sprintf ";\n%s" storage_download_chunk_ts)
            typescript_test_file in
        let ts_file =
          add_import
            (Import_as
               {
                 alias = Format.sprintf "%s_%d_interface" name index;
                 source = Format.sprintf "\"./%s_%d_interface\"" name index;
               })
            ts_file in
        Factori_utils.append_to_file ~check_already_present:true
          ~filepath:(tests_path // "tests.ml")
          ~content:
            "let _ = Factori_types.output_debug ~level:0 \"Battery of OCaml \
             tests successful.\n\
             \"\n\n"
          () ;
        let ts_file_content = Format.asprintf "%a" (print_ts_file ~ts_file) () in
        Factori_utils.write_file (ts_tests_path // "test.ts") ts_file_content
      end ;
      let o_test_ts_exit_code =
        if typescript_compilation_exit_code = 0 then
          let res1 =
            command_timeout_log ~timeout:10
              "tsc -p test/src/ts-sdk/tsconfig.json"
              (mk_log kt1 log_directory "typescript_test") in
          if res1 = 0 then
            command_timeout_log ~timeout:10 "node test/src/ts-sdk/dist/test.js"
              (mk_log kt1 log_directory "typescript_test")
          else
            -1
        else
          -1 in
      let o_test_ocaml_exit_code =
        if ocaml_compilation_exit_code = 0 then
          command_timeout_log ~timeout:20 "dune exec test/src/tests/tests.exe"
            (mk_log kt1 log_directory "ocaml_test")
        else
          -1 in
      let _ =
        command_log
          "rm -rf test/src/ocaml_scenarios/dune* test/src/ts-sdk/test.ts \
           test/src/ocaml_sdk/dune* test/src/tests/dune* \
           test/src/ocaml_sdk/*.ml* test/src/ts-sdk/*code*.json \
           test/src/ts-sdk/*interface*.ts"
          Filename.null in
      let _ =
        command_log
          (Format.sprintf
             "cp -r test.bkp/contracts.json test.bkp/src/tests \
              test.bkp/src/ts-sdk/tsconfig.json \
              test.bkp/src/ts-sdk/package.json test/")
          Filename.null in
      (* let _ = command @@ Format.sprintf "mv test/project.opam.bkp test/project.opam" in *)
      output_result result_filename hash kt1 import_exit_code
        ocaml_compilation_exit_code typescript_compilation_exit_code
        o_test_ocaml_exit_code o_test_ts_exit_code name network occurence ;

      (* TODO: use config instead of ad-hoc name *)
      aux ~networks:["mainnet"] (index + 1) xs in
  let res = aux ~networks:["mainnet"] 0 kt1s in

  res

let prelude () =
  let _ =
    Format.eprintf
      "Prelude: creating and saving default compilation environment@." in
  let _ = command "rm -rf test" in
  Factori_utils.make_dir "test" ;
  let cmd =
    Format.sprintf
      "make test-clean && mkdir -p test.bkp && rm -rf test.bkp/* && %s empty \
       project test --ocaml --typescript --crawlori --force"
      factori in
  let _ = command cmd in
  let _ = init_tests_folder () in
  let _res2 = command (Format.sprintf "make -C test ts-deps") in
  let _res4 = command (Format.sprintf "make -C test ocaml") in
  let _res4 = command (Format.sprintf "cp -r test/* test.bkp/") in
  let _res5 =
    command
    @@ Format.sprintf "mv test.bkp/project.opam test.bkp/project.opam.bkp" in
  Format.eprintf "End of prelude\n@."

let () =
  prelude () ;
  let mainnet_kt1_json = Sys.argv.(1) in
  let result_json = Sys.argv.(2) in
  let log_directory = Sys.argv.(3) in
  let _ =
    import_and_compile_all_kt1s mainnet_kt1_json result_json log_directory in
  ()
