ROOT=/home/factori/json-table-viewer
BIN='/home/factori/factori/_build/default/scripts/long_test.exe'
DATE=`date +"%d-%m-%Y"`
FACTORI_PATH=/home/factori/factori

export PATH=/usr/local/bin:/usr/bin:/bin:/usr/games:/home/factori/factori/_opam/bin:/home/factori/factori/_build/
eval $(/usr/local/bin/opam env)
cp -r $ROOT/template  $ROOT/$DATE
$FACTORI_PATH/_bin/factori.asm --version | jq -R '{"version" : .}' > $ROOT/$DATE/version.json
ls $ROOT | grep 2022 | jq -R '.' | jq -s . > $ROOT/index.json
cd $FACTORI_PATH
eval $(/usr/local/bin/opam env)
$BIN $ROOT/$DATE/mainnet-kt1.json $ROOT/$DATE/result.json

