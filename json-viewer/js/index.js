function mkCircle(v , id, color, sym, maxValue) {
    var symbol ;
    var maxV;
    if (maxValue == undefined) { maxV = 100 } else { maxV = maxValue }
    if (sym == undefined) { symbol = "%" } else { symbol = sym };
    var myCircle = Circles.create({
        id:                  id,
        radius:              40,
        value:               v,
        maxValue:            maxV,
        width:               10,
        text:                function(value){ return value + "" + symbol;},
        colors:              ['#D3B6C6', color],
        duration:            400,
        wrpClass:            'circles-wrp',
        textClass:           'factori-stats-circle-text-success',
        valueStrokeClass:    'circles-valueStroke',
        maxValueStrokeClass: 'circles-maxValueStroke',
        styleWrapper:        true,
        styleText:           true
    });
}

function nbSuccess(l, field) {
}

function nbFailure(l, field) {
    var cpt = 0;
    l.forEach(function(e) {
        if (field != 0) cpt++
    });
    return cpt;
}

function nbImportSuccess(l) {
    var cpt = 0;
    l.forEach(function(e) {
        if (e.import_exit_code == 0) cpt++
    });
    return cpt;
}

function nbImportFailure(l) {
    var cpt = 0;
    l.forEach(function(e) {
        if (e.import_exit_code != 0) cpt++
    });
    return cpt;
}

function nbOCamlCompilationSuccess(l) {
    var cpt = 0;
    l.forEach(function(e) {
        if (e.ocaml_compilation_exit_code == 0) cpt++
    });
    return cpt;
}

function nbOCamlCompilationFailure(l) {
    var cpt = 0;
    l.forEach(function(e) {
        if (e.ocaml_compilation_exit_code != 0) cpt++
    });
    return cpt;
}

function nbOCamlTestsSuccess(l) {
    var cpt = 0;
    l.forEach(function(e) {
        if (e.test_ocaml_exit_code == 0) cpt++
    });
    return cpt;
}

function nbOCamlTestsFailure(l) {
    var cpt = 0;
    l.forEach(function(e) {
        if (e.test_ocaml_exit_code != 0) cpt++
    });
    return cpt;
}

function nbTsCompilationSuccess(l) {
    var cpt = 0;
    l.forEach(function(e) {
        if (e.typescript_compilation_exit_code == 0) cpt++
    });
    return cpt;
}

function nbTsCompilationFailure(l) {
    var cpt = 0;
    l.forEach(function(e) {
        if (e.typescript_compilation_exit_code != 0) cpt++
    });
    return cpt;
}

function nbTsTestsSuccess(l) {
    var cpt = 0;
    l.forEach(function(e) {
        if (e.test_ts_exit_code == 0) cpt++
    });
    return cpt;
}

function nbTsTestsFailure(l) {
    var cpt = 0;
    l.forEach(function(e) {
        if (e.test_ts_exit_code != 0) cpt++
    });
    return cpt;
}

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function mkCircleEl(cell, v, color, symb, maxV) {
    let id = getRandomInt(100000);
    cell.setAttribute('id', id);
    mkCircle(v, id, color, symb,maxV);
}

function mkRow(index_json) {
    var table = document.getElementById("index-table") ;
    fetch(index_json)
        .then(res => res.json()) // the .json() method parses the JSON response into a JS object literal
        .then(function (index_res) {
            index_res.forEach(function(e) {
                var row = table.insertRow();
                var cell = row.insertCell();
                fetch(e+'/mainnet-kt1.json')
                    .then(res => res.json())
                    .then(function(n) {
                        fetch(e+'/result.json')
                            .then(res => res.json())
                            .then(function(result) {
                                fetch(e+'/version.json')
                                    .then(res => res.json()) // the .json() method parses the JSON response into a JS object literal
                                    .then(function (version) {
                                        cell.innerHTML = '<a href="'+e+'/index.html">'+e+'</a>';
                                        cell = row.insertCell();

                                        cell.innerHTML = version.version.substring(0, 15);
                                        cell = row.insertCell();

                                        mkCircleEl(cell, n.length, "black", "", n.length)
                                        cell = row.insertCell();

                                        mkCircleEl(cell, result.length, "black", "", result.length);
                                        cell = row.insertCell();

                                        mkCircleEl(cell, nbImportSuccess(result) / result.length * 100, "black")
                                        cell = row.insertCell();

                                        mkCircleEl(cell, nbOCamlCompilationSuccess(result) / result.length * 100, "black")
                                        cell = row.insertCell();

                                        mkCircleEl(cell, nbOCamlTestsSuccess(result) / result.length * 100, "black")
                                        cell = row.insertCell();

                                        mkCircleEl(cell, nbTsCompilationSuccess(result) / result.length * 100, "black")
                                        cell = row.insertCell();

                                        mkCircleEl(cell, nbTsTestsSuccess(result) / result.length * 100, "black")
                                        cell = row.insertCell();
                                    })
                            })
                    })
            })
        })
}


function main() {
    mkRow('index.json')
}

main ();
